VERSION 5.00
Object = "{4A4AA691-3E6F-11D2-822F-00104B9E07A1}#3.0#0"; "ssdw3bo.ocx"
Object = "{18448B59-8FF7-4DA5-AAE9-A7627AD4C998}#13.0#0"; "shlob012.ocx"
Object = "{047C08B1-0DC4-4F6D-86E9-56B179DB50E8}#13.0#0"; "shcmb012.ocx"
Object = "{0A354FBB-9D11-4E4C-A84A-435FAC06E59A}#13.0#0"; "fldrv012.ocx"
Object = "{DCFE21FC-18E2-480F-82BF-057A7A5D6422}#13.0#0"; "filev012.ocx"
Begin VB.Form frmCetaFileManager 
   Caption         =   "CETA File Manager"
   ClientHeight    =   12555
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   21720
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCETAFileManager.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   12555
   ScaleWidth      =   21720
   Begin VB.CommandButton cmdParentFolder 
      Caption         =   "Up"
      Height          =   325
      Index           =   1
      Left            =   13800
      TabIndex        =   111
      Top             =   4860
      Width           =   375
   End
   Begin VB.CommandButton cmdParentFolder 
      Caption         =   "Up"
      Height          =   325
      Index           =   0
      Left            =   13800
      TabIndex        =   110
      Top             =   60
      Width           =   375
   End
   Begin VB.CommandButton cmdGoTo 
      Height          =   360
      Index           =   47
      Left            =   1920
      Style           =   1  'Graphical
      TabIndex        =   108
      Top             =   10560
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Height          =   360
      Index           =   46
      Left            =   60
      Style           =   1  'Graphical
      TabIndex        =   107
      Top             =   10560
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Height          =   360
      Index           =   45
      Left            =   1920
      Style           =   1  'Graphical
      TabIndex        =   106
      Top             =   10140
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Height          =   360
      Index           =   44
      Left            =   60
      Style           =   1  'Graphical
      TabIndex        =   105
      Top             =   10140
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Height          =   360
      Index           =   23
      Left            =   1920
      Style           =   1  'Graphical
      TabIndex        =   104
      Top             =   4680
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Height          =   360
      Index           =   22
      Left            =   60
      Style           =   1  'Graphical
      TabIndex        =   103
      Top             =   4680
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Height          =   360
      Index           =   21
      Left            =   1920
      Style           =   1  'Graphical
      TabIndex        =   102
      Top             =   4260
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Height          =   360
      Index           =   20
      Left            =   60
      Style           =   1  'Graphical
      TabIndex        =   101
      Top             =   4260
      Width           =   1770
   End
   Begin VB.CheckBox chkStopCatalog 
      Caption         =   "Stop Catalog"
      Height          =   315
      Left            =   17520
      TabIndex        =   94
      Top             =   9780
      Visible         =   0   'False
      Width           =   1455
   End
   Begin VB.CommandButton cmdStopCatalog 
      Caption         =   "Stop Catalog Operation"
      Height          =   615
      Left            =   60
      TabIndex        =   93
      Top             =   11100
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Caption         =   "Desktop"
      Height          =   360
      Index           =   42
      Left            =   60
      Style           =   1  'Graphical
      TabIndex        =   87
      Top             =   9720
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Caption         =   "Desktop"
      Height          =   360
      Index           =   18
      Left            =   60
      Style           =   1  'Graphical
      TabIndex        =   86
      Top             =   3840
      Width           =   1770
   End
   Begin FileViewControl.FileView shFiles 
      Height          =   4275
      Index           =   0
      Left            =   7680
      TabIndex        =   81
      Top             =   420
      Width           =   6435
      _Version        =   851968
      _ExtentX        =   11351
      _ExtentY        =   7541
      _StockProps     =   64
      ViewStyle       =   1
      AllowFileExecute=   0   'False
      ShowSpecialFolders=   0   'False
      ShowHiddenItems =   0   'False
      AutoUpdate      =   0   'False
      DefaultKeyHandling=   0   'False
      ShowContextMenus=   0   'False
      ShowUIDialogs   =   0   'False
      CurrentFolder   =   "frmCETAFileManager.frx":08CA
      ShowInfotips    =   0   'False
      AllowDragDrop   =   0   'False
      AllowZipFolders =   0   'False
      HideSelection   =   0   'False
      SetTextBackColor=   -1
   End
   Begin VB.CommandButton cmdGoTo 
      Caption         =   "BFS-EDIT"
      Height          =   360
      Index           =   26
      Left            =   60
      Style           =   1  'Graphical
      TabIndex        =   79
      Top             =   6360
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Caption         =   "QFS-01"
      Height          =   360
      Index           =   28
      Left            =   60
      Style           =   1  'Graphical
      TabIndex        =   78
      Top             =   6780
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Caption         =   "DMZ-ASPERA"
      Height          =   360
      Index           =   30
      Left            =   60
      Style           =   1  'Graphical
      TabIndex        =   77
      Top             =   7200
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Caption         =   "DMZ-HOTFOLDERS"
      Height          =   360
      Index           =   32
      Left            =   60
      Style           =   1  'Graphical
      TabIndex        =   76
      Top             =   7620
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Caption         =   "DMZ-LANDINGPAD"
      Height          =   360
      Index           =   34
      Left            =   60
      Style           =   1  'Graphical
      TabIndex        =   75
      Top             =   8040
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Caption         =   "VIESTORE"
      Height          =   360
      Index           =   36
      Left            =   60
      Style           =   1  'Graphical
      TabIndex        =   74
      Top             =   8460
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Caption         =   "IT-RAID"
      Height          =   360
      Index           =   38
      Left            =   60
      Style           =   1  'Graphical
      TabIndex        =   73
      Top             =   8880
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Caption         =   "BBC-HD01"
      Height          =   360
      Index           =   40
      Left            =   60
      Style           =   1  'Graphical
      TabIndex        =   72
      Top             =   9300
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Height          =   360
      Index           =   43
      Left            =   1920
      Style           =   1  'Graphical
      TabIndex        =   71
      Top             =   9720
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Height          =   360
      Index           =   19
      Left            =   1920
      Style           =   1  'Graphical
      TabIndex        =   70
      Top             =   3840
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Caption         =   "BBC-HD01"
      Height          =   360
      Index           =   16
      Left            =   60
      Style           =   1  'Graphical
      TabIndex        =   69
      Top             =   3420
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Caption         =   "IT-RAID"
      Height          =   360
      Index           =   14
      Left            =   60
      Style           =   1  'Graphical
      TabIndex        =   68
      Top             =   3000
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Caption         =   "VIESTORE"
      Height          =   360
      Index           =   12
      Left            =   60
      Style           =   1  'Graphical
      TabIndex        =   67
      Top             =   2580
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Caption         =   "DMZ-LANDINGPAD"
      Height          =   360
      Index           =   10
      Left            =   60
      Style           =   1  'Graphical
      TabIndex        =   66
      Top             =   2160
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Caption         =   "DMZ-HOTFOLDERS"
      Height          =   360
      Index           =   8
      Left            =   60
      Style           =   1  'Graphical
      TabIndex        =   65
      Top             =   1740
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Caption         =   "DMZ-ASPERA"
      Height          =   360
      Index           =   6
      Left            =   60
      Style           =   1  'Graphical
      TabIndex        =   64
      Top             =   1320
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Caption         =   "QFS-01"
      Height          =   360
      Index           =   4
      Left            =   60
      Style           =   1  'Graphical
      TabIndex        =   63
      Top             =   900
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Caption         =   "BFS-EDIT"
      Height          =   360
      Index           =   2
      Left            =   60
      Style           =   1  'Graphical
      TabIndex        =   62
      Top             =   480
      Width           =   1770
   End
   Begin VB.Timer tmrStateMonitor 
      Interval        =   1
      Left            =   17760
      Top             =   7920
   End
   Begin VB.Timer tmrPeriod 
      Interval        =   1
      Left            =   17160
      Top             =   7920
   End
   Begin FolderViewControl.FolderView ShFolders 
      Height          =   4275
      Index           =   0
      Left            =   3780
      TabIndex        =   58
      Top             =   420
      Width           =   3855
      _Version        =   851968
      _ExtentX        =   6800
      _ExtentY        =   7541
      _StockProps     =   64
      root            =   "frmCETAFileManager.frx":08E2
      rrr             =   "test"
      ShowContextMenus=   0   'False
      ShowSpecailFolders=   0   'False
      ShowHiddenObjects=   0   'False
      AutoUpdate      =   0   'False
      AllowNodeRenaming=   0   'False
      DefaultKeyHandling=   0   'False
      AutoCheckChildren=   -1  'True
      ShowInfoTips    =   -1  'True
      AllowDragDrop   =   0   'False
      ShowUIDialogs   =   0   'False
      AllowZipFolders =   0   'False
      HideSelection   =   0   'False
      QueryHasSubFolders=   0   'False
      FullRowSelect   =   0   'False
      LineColor       =   -16777216
      NodeHeight      =   -1
   End
   Begin ShComboBox.ShComboBox ShDrives 
      Height          =   330
      Index           =   0
      Left            =   3780
      TabIndex        =   56
      Top             =   60
      Width           =   9975
      _Version        =   851968
      _ExtentX        =   17595
      _ExtentY        =   582
      _StockProps     =   4
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ShowFullPath    =   -1  'True
   End
   Begin ShellObjects.ShellFileOperation ShellFileOperation 
      Left            =   15720
      Top             =   9120
      _Version        =   851968
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
   Begin ShellObjects.ShellProgressDialog ShellProgressDialog 
      Left            =   18720
      Top             =   7980
      _Version        =   851968
      _ExtentX        =   847
      _ExtentY        =   847
      _StockProps     =   0
   End
   Begin VB.CommandButton cmdEnable 
      Caption         =   "Enable Options"
      Height          =   615
      Left            =   1920
      TabIndex        =   55
      Top             =   11100
      Width           =   1770
   End
   Begin VB.Frame fraButtons 
      BorderStyle     =   0  'None
      Height          =   7695
      Left            =   17040
      TabIndex        =   28
      Top             =   0
      Width           =   4065
      Begin VB.CheckBox chkNoExtenders 
         Caption         =   "Files Have No Extenders"
         Height          =   255
         Left            =   1620
         TabIndex        =   100
         Top             =   2520
         Width           =   2115
      End
      Begin VB.CommandButton cmdFileOp 
         Caption         =   "Folder Delete"
         Height          =   375
         Index           =   5
         Left            =   0
         TabIndex        =   99
         ToolTipText     =   "Enter Source only."
         Top             =   2100
         Width           =   1515
      End
      Begin VB.CommandButton cmdFileOp 
         Caption         =   "Folder Move"
         Height          =   375
         Index           =   4
         Left            =   0
         TabIndex        =   98
         ToolTipText     =   "Enter Source only."
         Top             =   1680
         Width           =   1515
      End
      Begin VB.CommandButton cmdFileOp 
         Caption         =   "Folder Copy"
         Height          =   375
         Index           =   3
         Left            =   0
         TabIndex        =   97
         ToolTipText     =   "Enter Source only."
         Top             =   1260
         Width           =   1515
      End
      Begin VB.CommandButton cmdOptionsNormal 
         Caption         =   "Clear Options Back to Normal"
         Height          =   375
         Left            =   1020
         TabIndex        =   96
         ToolTipText     =   "Enter Source only."
         Top             =   7020
         Width           =   2775
      End
      Begin VB.CommandButton cmdOptionsForIndependents 
         Caption         =   "Set Options for Independents"
         Height          =   375
         Left            =   1020
         TabIndex        =   95
         ToolTipText     =   "Enter Source only."
         Top             =   6600
         Width           =   2775
      End
      Begin VB.CheckBox chkSkipVerify 
         Caption         =   "Don't Verify Existing Files"
         Height          =   255
         Left            =   1620
         TabIndex        =   92
         Top             =   2220
         Width           =   2115
      End
      Begin VB.CheckBox chkRebuildReferences 
         Caption         =   "Rebuild Ref on Rename"
         Height          =   255
         Left            =   1620
         TabIndex        =   90
         Top             =   1920
         Value           =   1  'Checked
         Width           =   2115
      End
      Begin VB.CommandButton cmdFileList 
         Caption         =   "List Folder to Clipboard"
         Height          =   375
         Left            =   1020
         TabIndex        =   89
         ToolTipText     =   "Enter Source only."
         Top             =   6180
         Width           =   2775
      End
      Begin VB.CheckBox chkMD5 
         Caption         =   "Do MD5 (on new files)"
         Height          =   375
         Left            =   1620
         TabIndex        =   88
         Top             =   300
         Width           =   2235
      End
      Begin VB.ComboBox cmbPurpose 
         Height          =   315
         Left            =   1020
         TabIndex        =   84
         ToolTipText     =   "The Clip Codec"
         Top             =   3900
         Width           =   2775
      End
      Begin VB.TextBox txtSvenskProjectNumber 
         Height          =   315
         Left            =   1020
         TabIndex        =   60
         Top             =   3180
         Width           =   2775
      End
      Begin VB.CommandButton cmdClearTitle 
         Caption         =   "Clear Title / Ep Info"
         Height          =   315
         Left            =   1020
         TabIndex        =   54
         Top             =   5340
         Width           =   2775
      End
      Begin VB.TextBox txtSeriesID 
         Height          =   315
         Left            =   2760
         TabIndex        =   51
         ToolTipText     =   "The source Clip ID"
         Top             =   4620
         Width           =   1035
      End
      Begin VB.ComboBox cmbSeries 
         Height          =   315
         Left            =   1020
         TabIndex        =   44
         ToolTipText     =   "The Series for the Tape"
         Top             =   4620
         Width           =   915
      End
      Begin VB.TextBox txtSubTitle 
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   1020
         TabIndex        =   43
         ToolTipText     =   "The sub title (if known)"
         Top             =   4980
         Width           =   2775
      End
      Begin VB.OptionButton optSetAsMasterfile 
         Caption         =   "Catalogue as Nothing"
         Height          =   255
         Index           =   2
         Left            =   1620
         TabIndex        =   40
         Top             =   1320
         Value           =   -1  'True
         Width           =   1935
      End
      Begin VB.OptionButton optSetAsMasterfile 
         Caption         =   "Catalogue as Transcode"
         Height          =   255
         Index           =   1
         Left            =   1620
         TabIndex        =   39
         Top             =   1020
         Width           =   2115
      End
      Begin VB.OptionButton optSetAsMasterfile 
         Caption         =   "Catalogue as Masterfile"
         Height          =   255
         Index           =   0
         Left            =   1620
         TabIndex        =   38
         Top             =   720
         Width           =   1995
      End
      Begin VB.TextBox txtJobID 
         Height          =   315
         Left            =   1020
         TabIndex        =   35
         Top             =   2820
         Width           =   2775
      End
      Begin VB.CheckBox chkMediaInfo 
         Caption         =   "Do MediaInfo (on new files)"
         Height          =   375
         Left            =   1620
         TabIndex        =   34
         Top             =   0
         Width           =   2415
      End
      Begin VB.CommandButton cmdCatalog 
         Caption         =   "Catalogue Folder"
         Height          =   315
         Left            =   1020
         TabIndex        =   33
         Top             =   5760
         Width           =   2775
      End
      Begin VB.CheckBox chkExcludeSubfolders 
         Caption         =   "Exclude Subfolders"
         Height          =   375
         Left            =   1620
         TabIndex        =   32
         Top             =   1560
         Value           =   1  'Checked
         Width           =   1755
      End
      Begin VB.CommandButton cmdFileOp 
         Caption         =   "&Delete"
         Height          =   375
         Index           =   2
         Left            =   0
         TabIndex        =   31
         ToolTipText     =   "Enter Source only."
         Top             =   840
         Width           =   1515
      End
      Begin VB.CommandButton cmdFileOp 
         Caption         =   "&Move"
         Height          =   375
         Index           =   1
         Left            =   0
         TabIndex        =   30
         ToolTipText     =   "Enter Source and Destination."
         Top             =   420
         Width           =   1515
      End
      Begin VB.CommandButton cmdFileOp 
         Caption         =   "&Copy"
         Height          =   375
         Index           =   0
         Left            =   0
         TabIndex        =   29
         ToolTipText     =   "Enter Source and Destination."
         Top             =   0
         Width           =   1515
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo cmbCompany 
         Height          =   315
         Left            =   1020
         TabIndex        =   45
         ToolTipText     =   "The company this tape belongs to"
         Top             =   3540
         Width           =   2775
         DataFieldList   =   "name"
         _Version        =   196617
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BackColorOdd    =   16761087
         RowHeight       =   423
         Columns.Count   =   2
         Columns(0).Width=   5318
         Columns(0).Caption=   "name"
         Columns(0).Name =   "name"
         Columns(0).DataField=   "name"
         Columns(0).FieldLen=   256
         Columns(1).Width=   3200
         Columns(1).Visible=   0   'False
         Columns(1).Caption=   "companyID"
         Columns(1).Name =   "companyID"
         Columns(1).DataField=   "companyID"
         Columns(1).FieldLen=   256
         _ExtentX        =   4895
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   12632319
         DataFieldToDisplay=   "name"
      End
      Begin SSDataWidgets_B_OLEDB.SSOleDBCombo txtTitle 
         Height          =   315
         Left            =   1020
         TabIndex        =   46
         ToolTipText     =   "The company this tape belongs to"
         Top             =   4260
         Width           =   2775
         DataFieldList   =   "title"
         _Version        =   196617
         BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BackColorOdd    =   16761087
         RowHeight       =   423
         Columns.Count   =   4
         Columns(0).Width=   3200
         Columns(0).Visible=   0   'False
         Columns(0).Caption=   "masterseriestitleID"
         Columns(0).Name =   "masterseriestitleID"
         Columns(0).DataField=   "masterseriestitleID"
         Columns(0).FieldLen=   256
         Columns(1).Width=   1773
         Columns(1).Caption=   "seriesID"
         Columns(1).Name =   "seriesID"
         Columns(1).DataField=   "seriesID"
         Columns(1).FieldLen=   256
         Columns(2).Width=   8149
         Columns(2).Caption=   "title"
         Columns(2).Name =   "title"
         Columns(2).DataField=   "title"
         Columns(2).FieldLen=   256
         Columns(3).Width=   3200
         Columns(3).Visible=   0   'False
         Columns(3).Caption=   "companyID"
         Columns(3).Name =   "companyID"
         Columns(3).DataField=   "companyID"
         Columns(3).FieldLen=   256
         _ExtentX        =   4895
         _ExtentY        =   556
         _StockProps     =   93
         BackColor       =   16777215
         DataFieldToDisplay=   "title"
      End
      Begin VB.Label lblCaption 
         Caption         =   "Clip Purpose"
         Height          =   255
         Index           =   1
         Left            =   60
         TabIndex        =   83
         Top             =   3960
         Width           =   915
      End
      Begin VB.Label lblCaption 
         Caption         =   "Sv. Proj #"
         Height          =   255
         Index           =   0
         Left            =   60
         TabIndex        =   61
         Top             =   3240
         Width           =   915
      End
      Begin VB.Label lblCompanyID 
         Appearance      =   0  'Flat
         BackColor       =   &H0080FFFF&
         ForeColor       =   &H80000008&
         Height          =   315
         Left            =   0
         TabIndex        =   53
         Tag             =   "CLEARFIELDS"
         Top             =   5340
         Visible         =   0   'False
         Width           =   855
      End
      Begin VB.Label lblCaption 
         Caption         =   "Series ID"
         Height          =   255
         Index           =   66
         Left            =   2040
         TabIndex        =   52
         Top             =   4680
         Width           =   735
      End
      Begin VB.Label lblCaption 
         Caption         =   "Company"
         Height          =   255
         Index           =   3
         Left            =   60
         TabIndex        =   50
         Top             =   3600
         Width           =   915
      End
      Begin VB.Label lblCaption 
         Caption         =   "Series"
         Height          =   255
         Index           =   74
         Left            =   60
         TabIndex        =   49
         Top             =   4680
         Width           =   915
      End
      Begin VB.Label lblCaption 
         Caption         =   "Title"
         Height          =   255
         Index           =   8
         Left            =   60
         TabIndex        =   48
         Top             =   4320
         Width           =   915
      End
      Begin VB.Label lblCaption 
         Caption         =   "Sub Title"
         Height          =   255
         Index           =   9
         Left            =   60
         TabIndex        =   47
         Top             =   5040
         Width           =   915
      End
      Begin VB.Label Label1 
         Caption         =   "Job No."
         Height          =   195
         Left            =   60
         TabIndex        =   36
         Top             =   2880
         Width           =   555
      End
   End
   Begin VB.Timer timShutDown 
      Interval        =   60000
      Left            =   19920
      Top             =   7980
   End
   Begin VB.CommandButton cmdGoTo 
      Caption         =   "DMZ-IMPEX"
      Height          =   360
      Index           =   33
      Left            =   1920
      Style           =   1  'Graphical
      TabIndex        =   27
      Top             =   7620
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Caption         =   "DMZ-FLASH"
      Height          =   360
      Index           =   31
      Left            =   1920
      Style           =   1  'Graphical
      TabIndex        =   26
      Top             =   7200
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Caption         =   "Warp"
      Height          =   360
      Index           =   29
      Left            =   1920
      Style           =   1  'Graphical
      TabIndex        =   25
      Top             =   6780
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Caption         =   "BFS-PLATFORMS"
      Height          =   360
      Index           =   27
      Left            =   1920
      Style           =   1  'Graphical
      TabIndex        =   24
      Top             =   6360
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Caption         =   "IT-STORE"
      Height          =   360
      Index           =   39
      Left            =   1920
      Style           =   1  'Graphical
      TabIndex        =   23
      Top             =   8880
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Caption         =   "RRSAT-Master"
      Height          =   360
      Index           =   37
      Left            =   1920
      Style           =   1  'Graphical
      TabIndex        =   22
      Top             =   8460
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Caption         =   "DMZ-IMPEX"
      Height          =   360
      Index           =   9
      Left            =   1920
      Style           =   1  'Graphical
      TabIndex        =   21
      Top             =   1740
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Caption         =   "DMZ-FLASH"
      Height          =   360
      Index           =   7
      Left            =   1920
      Style           =   1  'Graphical
      TabIndex        =   20
      Top             =   1320
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Caption         =   "WARP"
      Height          =   360
      Index           =   5
      Left            =   1920
      Style           =   1  'Graphical
      TabIndex        =   19
      Top             =   900
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Caption         =   "BFS-PLATFORMS"
      Height          =   360
      Index           =   3
      Left            =   1920
      Style           =   1  'Graphical
      TabIndex        =   18
      Top             =   480
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Caption         =   "IT-STORE"
      Height          =   360
      Index           =   15
      Left            =   1920
      Style           =   1  'Graphical
      TabIndex        =   17
      Top             =   3000
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Caption         =   "RRSAT-Master"
      Height          =   360
      Index           =   13
      Left            =   1920
      Style           =   1  'Graphical
      TabIndex        =   16
      Top             =   2580
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Caption         =   "DMZ-THUMBS"
      Height          =   360
      Index           =   35
      Left            =   1920
      Style           =   1  'Graphical
      TabIndex        =   15
      Top             =   8040
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Caption         =   "BFS-CETA"
      Height          =   360
      Index           =   24
      Left            =   60
      Style           =   1  'Graphical
      TabIndex        =   14
      Top             =   5940
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Caption         =   "DMZ-THUMBS"
      Height          =   360
      Index           =   11
      Left            =   1920
      Style           =   1  'Graphical
      TabIndex        =   13
      Top             =   2160
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Caption         =   "BFS-CETA"
      Height          =   360
      Index           =   0
      Left            =   60
      Style           =   1  'Graphical
      TabIndex        =   12
      Top             =   60
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Height          =   360
      Index           =   41
      Left            =   1920
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   9300
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Height          =   360
      Index           =   17
      Left            =   1920
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   3420
      Width           =   1770
   End
   Begin VB.CommandButton cmdRefresh 
      Caption         =   "Refresh"
      Height          =   315
      Index           =   1
      Left            =   14340
      TabIndex        =   7
      Top             =   4860
      Width           =   915
   End
   Begin VB.CommandButton cmdRefresh 
      Caption         =   "Refresh"
      Height          =   315
      Index           =   0
      Left            =   14340
      TabIndex        =   6
      Top             =   60
      Width           =   915
   End
   Begin VB.Frame fraOptions 
      Caption         =   "Options"
      Enabled         =   0   'False
      Height          =   1035
      Left            =   3780
      TabIndex        =   3
      Top             =   11040
      Width           =   17295
      Begin VB.CheckBox chkMoveThenForget 
         Caption         =   "Move then Forget"
         Height          =   615
         Left            =   15480
         TabIndex        =   112
         Top             =   240
         Width           =   1575
      End
      Begin VB.CheckBox chkLogAndXML 
         Caption         =   "Include .txt .fcp .csv, .log etc."
         Height          =   375
         Left            =   4260
         TabIndex        =   91
         Top             =   360
         Width           =   1575
      End
      Begin VB.CheckBox chkAllowNoPurpose 
         Caption         =   "Allow Catalogue with no Clip Purpose"
         Height          =   615
         Left            =   7260
         TabIndex        =   85
         Top             =   240
         Width           =   1515
      End
      Begin VB.CheckBox chkSupressFilenameValidationWarnings 
         Caption         =   "Supress Filename Validation Warnings"
         Height          =   615
         Left            =   13620
         TabIndex        =   80
         Top             =   240
         Width           =   1575
      End
      Begin VB.CheckBox chkMediaInfoTimecodes 
         Caption         =   "MediaInfo Overwrite Timecode"
         Height          =   615
         Left            =   12240
         TabIndex        =   42
         Top             =   240
         Width           =   1155
      End
      Begin VB.CheckBox chkDontBlankMasterfile 
         Caption         =   "Don't blank 'Masterfile' status on copies"
         Height          =   615
         Left            =   10440
         TabIndex        =   41
         Top             =   240
         Width           =   1515
      End
      Begin VB.CheckBox chkAllowNoJobID 
         Caption         =   "Allow Catalogue with no JobID"
         Height          =   615
         Left            =   8880
         TabIndex        =   37
         Top             =   240
         Width           =   1395
      End
      Begin VB.CheckBox chkImages 
         Caption         =   "Include Jpgs, Tifs, DPX files"
         Height          =   375
         Left            =   5880
         TabIndex        =   11
         Top             =   360
         Width           =   1455
      End
      Begin VB.CheckBox chkRename 
         Caption         =   $"frmCETAFileManager.frx":08FA
         Height          =   855
         Left            =   1140
         TabIndex        =   5
         Top             =   120
         Width           =   3135
      End
      Begin VB.CheckBox chkYesToAll 
         Caption         =   "'Yes to all'"
         Height          =   555
         Left            =   180
         TabIndex        =   4
         Top             =   300
         Width           =   675
      End
   End
   Begin VB.CommandButton cmdSwapPaths 
      Caption         =   "<<  Swap Paths >>"
      Height          =   480
      Left            =   180
      TabIndex        =   2
      Top             =   5280
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Caption         =   "BFS-OPS"
      Height          =   360
      Index           =   25
      Left            =   1920
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   5940
      Width           =   1770
   End
   Begin VB.CommandButton cmdGoTo 
      Caption         =   "BFS-OPS"
      Height          =   360
      Index           =   1
      Left            =   1920
      TabIndex        =   0
      Top             =   60
      Width           =   1770
   End
   Begin ShComboBox.ShComboBox ShDrives 
      Height          =   330
      Index           =   1
      Left            =   3780
      TabIndex        =   57
      Top             =   4860
      Width           =   9975
      _Version        =   851968
      _ExtentX        =   17595
      _ExtentY        =   582
      _StockProps     =   4
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ShowFullPath    =   -1  'True
   End
   Begin FolderViewControl.FolderView ShFolders 
      Height          =   4935
      Index           =   1
      Left            =   3780
      TabIndex        =   59
      Top             =   5940
      Width           =   3855
      _Version        =   851968
      _ExtentX        =   6800
      _ExtentY        =   8705
      _StockProps     =   64
      root            =   "frmCETAFileManager.frx":0993
      rrr             =   "test"
      ShowContextMenus=   0   'False
      ShowSpecailFolders=   0   'False
      ShowHiddenObjects=   0   'False
      AutoUpdate      =   0   'False
      AllowNodeRenaming=   0   'False
      DefaultKeyHandling=   0   'False
      AutoCheckChildren=   -1  'True
      AllowDragDrop   =   0   'False
      ShowUIDialogs   =   0   'False
      AllowZipFolders =   0   'False
      HideSelection   =   0   'False
      QueryHasSubFolders=   0   'False
      FullRowSelect   =   0   'False
      LineColor       =   -16777216
      NodeHeight      =   -1
   End
   Begin FileViewControl.FileView shFiles 
      Height          =   4995
      Index           =   1
      Left            =   7740
      TabIndex        =   82
      Top             =   5940
      Width           =   6435
      _Version        =   851968
      _ExtentX        =   11351
      _ExtentY        =   8811
      _StockProps     =   64
      ViewStyle       =   1
      AllowFileExecute=   0   'False
      ShowSpecialFolders=   0   'False
      ShowHiddenItems =   0   'False
      AutoUpdate      =   0   'False
      DefaultKeyHandling=   0   'False
      ShowContextMenus=   0   'False
      ShowUIDialogs   =   0   'False
      CurrentFolder   =   "frmCETAFileManager.frx":09AB
      ShowInfotips    =   0   'False
      AllowDragDrop   =   0   'False
      AllowZipFolders =   0   'False
      HideSelection   =   0   'False
      SetTextBackColor=   -1
   End
   Begin FileViewControl.FileView shFiles 
      Height          =   1215
      Index           =   2
      Left            =   16980
      TabIndex        =   109
      Top             =   8400
      Visible         =   0   'False
      Width           =   3855
      _Version        =   851968
      _ExtentX        =   6800
      _ExtentY        =   2143
      _StockProps     =   64
      ViewStyle       =   1
      AllowFileExecute=   0   'False
      ShowSpecialFolders=   0   'False
      ShowHiddenItems =   0   'False
      AutoUpdate      =   0   'False
      DefaultKeyHandling=   0   'False
      ShowContextMenus=   0   'False
      ShowUIDialogs   =   0   'False
      CurrentFolder   =   "frmCETAFileManager.frx":09C3
      ShowInfotips    =   0   'False
      AllowDragDrop   =   0   'False
      AllowZipFolders =   0   'False
      HideSelection   =   0   'False
      SetTextBackColor=   -1
   End
   Begin VB.Label lblCatalogprogress 
      Height          =   195
      Left            =   180
      TabIndex        =   10
      Top             =   12180
      Width           =   18195
   End
End
Attribute VB_Name = "frmCetaFileManager"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Type POINTAPI
    x As Long
    y As Long
End Type
Dim IsIdle As Boolean 'True when idling or while in idle-state
Dim startOfIdle As Long
Private Const INTERVAL As Long = 90
Private Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long
    'API function to check the state of the mouse buttons
    'as well as the keyboard.

Private Declare Function GetDiskFreeSpaceEx Lib "kernel32.dll" Alias "GetDiskFreeSpaceExA" (ByVal lpDirectoryName As String, lpFreeBytesAvailableToCaller As Currency, lpTotalNumberOfBytes As Currency, lpTotalNumberofFreeBytes As Currency) As Long

Private Declare Function GetAsyncKeyState Lib "user32" (ByVal vKey As Long) As Integer
    'set the length of time the computer must idle, before
    'the so-called "idle-state" is reached.
    '     unit: seconds
    'You'd probably want to change this value!!!
Dim MousePos As POINTAPI 'holds mouse position
    'holds time (in seconds) when the idle started
    'used to calculate if the computer has been idle for INTERVAL

Private Sub cmbCompany_Click()

lblCompanyID.Caption = cmbCompany.Columns("companyID").Text

'Try and load up the Custom Field Labels and dropdowns, and display the custom fields.
'Custom field labels are stored as part of the company table, for the owner of the clip.

Dim l_strTempLabel As String, l_lngCompanyID As Long
Dim l_strTemp As String
Dim l_strSQL As String

l_lngCompanyID = Val(lblCompanyID.Caption)

'If InStr(GetData("company", "cetaclientcode", "companyID", l_lngCompanyID), "/sha1checksum") > 0 Then
'    cmdMD5Checksum.Caption = "Make SHA1"
'Else
'    cmdMD5Checksum.Caption = "Make"
'End If
'
If l_lngCompanyID <> 0 Then
    
    lblCompanyID.Caption = cmbCompany.Columns("companyID").Text
    
    Dim l_conSearch As ADODB.Connection
    Dim l_rstSearch2 As ADODB.Recordset
    
    Set l_conSearch = New ADODB.Connection
    Set l_rstSearch2 = New ADODB.Recordset
    
    l_conSearch.ConnectionString = g_strConnection
    l_conSearch.Open
    
    l_strSQL = "SELECT * FROM masterseriestitle WHERE companyID = " & Val(lblCompanyID.Caption) & " ORDER BY title;"
    
    With l_rstSearch2
        .CursorLocation = adUseClient
        .LockType = adLockBatchOptimistic
        .CursorType = adOpenDynamic
        .Open l_strSQL, l_conSearch, adOpenDynamic
    End With
    
    l_rstSearch2.ActiveConnection = Nothing
    
    Set txtTitle.DataSourceList = l_rstSearch2
    
    l_conSearch.Close
    Set l_conSearch = Nothing

End If

End Sub

Private Sub cmbCompany_KeyPress(KeyAscii As Integer)

Dim l_strTempLabel As String, l_lngCompanyID As Long
Dim l_strTemp As String
Dim l_strSQL As String

If KeyAscii = 13 Then

    lblCompanyID.Caption = cmbCompany.Columns("companyID").Text

    'Try and load up the Custom Field Labels and dropdowns, and display the custom fields.
    'Custom field labels are stored as part of the company table, for the owner of the clip.
    
    l_lngCompanyID = Val(lblCompanyID.Caption)
    
    'If InStr(GetData("company", "cetaclientcode", "companyID", l_lngCompanyID), "/sha1checksum") > 0 Then
    '    cmdMD5Checksum.Caption = "Make SHA1"
    'Else
    '    cmdMD5Checksum.Caption = "Make"
    'End If
    '
    If l_lngCompanyID <> 0 Then
        
        Dim l_conSearch As ADODB.Connection
        Dim l_rstSearch2 As ADODB.Recordset
        
        Set l_conSearch = New ADODB.Connection
        Set l_rstSearch2 = New ADODB.Recordset
        
        l_conSearch.ConnectionString = g_strConnection
        l_conSearch.Open
        
        l_strSQL = "SELECT * FROM masterseriestitle WHERE companyID = " & Val(lblCompanyID.Caption) & " ORDER BY title;"
        
        With l_rstSearch2
            .CursorLocation = adUseClient
            .LockType = adLockBatchOptimistic
            .CursorType = adOpenDynamic
            .Open l_strSQL, l_conSearch, adOpenDynamic
        End With
        
        l_rstSearch2.ActiveConnection = Nothing
        
        Set txtTitle.DataSourceList = l_rstSearch2
        
        l_conSearch.Close
        Set l_conSearch = Nothing
        
    End If
    
    KeyAscii = 0

End If
    
End Sub

Private Sub cmbPurpose_GotFocus()
PopulateCombo "clippurpose", cmbPurpose
End Sub

Private Sub cmbSeries_GotFocus()
PopulateCombo "series", cmbSeries
HighLite cmbSeries
End Sub

Private Sub cmdCatalog_Click()

Dim l_strFileName As String, l_strDrivePath As String, l_strPath As String, l_lngEventID As Long, Item As ListItem, Count As Long, Count2 As Long, l_lngOnline As Long, NewCount As Long
Dim cnn As ADODB.Connection, l_strSQL As String, l_lngIntReference As Long, l_strReference As String, l_lngFileHandle As Long, l_curFileSize As Currency, l_strEpisode As String
Dim l_strFolderPath As String, UNCPath As String, temp As String, i As Long, l_strLastFolder As String, MediaData As MediaInfoData, l_lngJobID As Long, l_blnSupressMessage As Boolean
Dim l_ImageDimensions As ImgDimType, FSO As FileSystemObject, Catalog_File As File, FileCreatedDate As Date, l_blnFailed As Boolean, l_lngCommaCount As Long, l_rst As ADODB.Recordset
Dim sID As String, l_strFileToOpen As String

Set FSO = New Scripting.FileSystemObject

If IsCETALocked = True Then
    MsgBox "Ceta is Locked - cannot Catalogue at this time.", vbCritical, "CETA Locked"
    Exit Sub
End If

'Check the JobID is valid
l_lngJobID = Val(txtJobID.Text)
If Val(lblCompanyID.Caption) <> 0 Then
    If GetData("company", "source", "companyID", Val(lblCompanyID.Caption)) = "SVENSK" Then
        If chkAllowNoJobID.Value = 0 And l_lngJobID = 0 And txtSvenskProjectNumber.Text = "" Then
            MsgBox "At least one of JobID or Sv. Pr # has not been set - cannot Catalogue at this time.", vbCritical, "No JobID"
            Exit Sub
        End If
    Else
        If chkAllowNoJobID.Value = 0 And l_lngJobID = 0 Then
            MsgBox "JobID has not been set - cannot Catalogue at this time.", vbCritical, "No JobID"
            Exit Sub
        End If
    End If
Else
    If chkAllowNoJobID.Value = 0 And l_lngJobID = 0 Then
        MsgBox "JobID has not been set - cannot Catalogue at this time.", vbCritical, "No JobID"
        Exit Sub
    End If
End If

If g_LoginCancelled = True Then
    MsgBox "You must provide CETA login credentials to undertake this file operation.", vbCritical, "Error"
    Exit Sub
End If

'Check CompanyID and Diva Category (Clip Purpose)
'If cmbPurpose.Text = "" And chkAllowNoPurpose.Value = 0 Then
'    MsgBox "Please provide the Clip Purpose for these files.", vbCritical, "Error"
'    Exit Sub
'End If
If Val(lblCompanyID.Caption) = 0 Then
    MsgBox "Please provide the Owner of this material for these files.", vbCritical, "Error"
    Exit Sub
End If

If optSetAsMasterfile(0).Value = 0 Then
    If MsgBox("Are you checking in Masterfiles?", vbYesNo, "Please Confirm") = vbYes Then optSetAsMasterfile(0).Value = 1
End If

If cmbCompany.Text <> "" Or lblCompanyID.Caption <> "" Or txtTitle.Text <> "" Or cmbSeries.Text <> "" Or txtSubTitle.Text <> "" Or txtSeriesID.Text <> "" Then
    If MsgBox("Company / Title / Episode information has been edited. Are you sure you wish to catalogue all previously uncatalogued files with this information?", vbYesNo, "Please Confirm") = vbNo Then Exit Sub
    If MsgBox("Are you really sure?", vbYesNo, "Please Confirm") = vbNo Then Exit Sub
End If

g_FileOpInProgress = True

Set cnn = New ADODB.Connection
cnn.Open g_strConnection

If shFiles(0).ItemCount > 0 Then

    chkStopCatalog.Value = 0
'    NewCount = Val(InputBox("Start Item", "Start part way through folder", 1))
    If NewCount < 1 Then NewCount = 1
    For Count = NewCount To shFiles(0).ItemCount
        
        If chkStopCatalog.Value <> 0 Then
            MsgBox "Catalog Cancelled"
            lblCatalogprogress.Caption = ""
            Exit Sub
        End If
        Set Item = shFiles(0).ListItem(Count)
        
        If Not Item.IsFolder And Item.DisplayName <> "libraryID.sys" And Item.DisplayName <> ".libraryID.sys" And (InStr(Item.DisplayName, "@") = 0) And Left(Item.DisplayName, 2) <> "._" And Not ((Item.Attributes(Link) And Link) <> 0) Then
        
            If Not Item.IsFolder And Item.DisplayName <> ".DS_Store" And Item.DisplayName <> "Thumbs.db" And ValidateFilename(Item.DisplayName) = False And chkSupressFilenameValidationWarnings.Value = 0 Then
                MsgBox "File " & Item.DisplayName & " has illegal characters in the filename.", vbInformation
            End If
        
            'get the full file name and path
            l_strPath = shFiles(0).CurrentFolder
        
            If Left(l_strPath, 2) <> "\\" Then
                l_strFolderPath = Right(l_strPath, Len(l_strPath) - 3)
                l_strDrivePath = Left(l_strPath, 2)
            Else
                shFiles(2).CurrentFolder = l_strPath
                l_strFileToOpen = l_strPath
                
                Do While Not FSO.FileExists(l_strFileToOpen & "\libraryID.sys") And Not FSO.FileExists(l_strFileToOpen & "\.libraryID.sys")
                    l_strFileToOpen = Left(shFiles(2).CurrentFolder, Len(shFiles(2).CurrentFolder) - Len(shFiles(2).CurrentFolderDisplayName) - 1)
                    shFiles(2).CurrentFolder = l_strFileToOpen
                Loop
                
                l_strDrivePath = l_strFileToOpen
                l_strFolderPath = Mid(l_strPath, Len(l_strFileToOpen) + 2)
            End If
                
            If ValidateFoldername(l_strFolderPath) = False And l_blnSupressMessage = False And chkSupressFilenameValidationWarnings.Value = 0 Then
                If MsgBox("This folder: " & l_strFolderPath & " has illegal characters in the foldername. Supress further messages within this folder?", vbYesNo) = vbYes Then
                    l_blnSupressMessage = True
                End If
            End If
                
            If ((chkImages.Value <> vbChecked And _
            (LCase(Right(Item.DisplayName, 4)) <> ".jpg" And LCase(Right(Item.DisplayName, 4)) <> ".gif" And LCase(Right(Item.DisplayName, 4)) <> ".bmp" _
            And LCase(Right(Item.DisplayName, 4)) <> ".png" And LCase(Right(Item.DisplayName, 4)) <> ".tif" And LCase(Right(Item.DisplayName, 4)) <> ".dpx")) _
            Or chkImages.Value = vbChecked) _
            And ((chkLogAndXML.Value <> vbChecked And _
            (LCase(Right(Item.DisplayName, 4)) <> ".xml" And LCase(Right(Item.DisplayName, 4)) <> ".xmp" And LCase(Right(Item.DisplayName, 4)) <> ".log" _
            And LCase(Right(Item.DisplayName, 4)) <> ".fcp" And LCase(Right(Item.DisplayName, 4)) <> ".txt" And LCase(Right(Item.DisplayName, 17)) <> ".pipelineschedule" _
            And LCase(Right(Item.DisplayName, 17)) <> ".pipelineprint" And LCase(Right(Item.DisplayName, 4)) <> ".csv" And LCase(Right(Item.DisplayName, 5)) <> ".xlsx" _
            And LCase(Right(Item.DisplayName, 5)) <> ".docx" And LCase(Right(Item.DisplayName, 4)) <> ".doc" And LCase(Right(Item.DisplayName, 4)) <> ".xls" _
            And LCase(Right(Item.DisplayName, 5)) <> ".vamc")) _
            Or chkLogAndXML.Value = vbChecked) _
            Then
                'get the Event ID if it exists
                l_lngEventID = LookupEventID(0, Item)
                
                If l_lngEventID = 0 Then
                
                    'pick up the actual file name
                    l_strFileName = Item.DisplayName
                
                    lblCatalogprogress.Caption = Count & " of " & shFiles(0).ItemCount & ": " & l_strFileName
                    DoEvents
                
                    
                    If Left(l_strDrivePath, 1) <> "\" Then
                        l_strFileToOpen = Left$(shFiles(0).CurrentFolder, 2)
                    Else
                        l_strFileToOpen = l_strDrivePath
                    End If
                    
                    If FSO.FileExists(l_strFileToOpen & "\libraryID.sys") Then
                        Open l_strFileToOpen & "\libraryID.sys" For Input As 1
                        Line Input #1, sID
                        Close 1
                    ElseIf FSO.FileExists(l_strFileToOpen & "\.libraryID.sys") Then
                        Open l_strFileToOpen & "\.libraryID.sys" For Input As 1
                        Line Input #1, sID
                        Close 1
                    End If
                    
                    'Drive LibraryID = sID (0 if no libraryID.sys on root of drive)
                    'Path = l_strPath
                    'Filename = l_strFilename
                    If sID <> "" Then
                        If GetData("library", "format", "libraryID", sID) = "DISCSTORE" Then
                            l_lngOnline = 1
                        Else
                            l_lngOnline = 0
                        End If
                        l_lngIntReference = GetNextSequence("internalreference")
                        l_curFileSize = 0
                        l_blnFailed = False
                        FileCreatedDate = 0
                        On Error GoTo FILE_OPEN_ERROR1
                        If FSO.FileExists(Replace(shFiles(0).CurrentFolder, "\\", "\\?\UNC\") & "\" & Item.DisplayName) Then
                            API_OpenFile Replace(shFiles(0).CurrentFolder, "\\", "\\?\UNC\") & "\" & Item.DisplayName, l_lngFileHandle, l_curFileSize
                            API_CloseFile l_lngFileHandle
                            Set Catalog_File = FSO.GetFile(Replace(shFiles(0).CurrentFolder, "\\", "\\?\UNC\") & "\" & Item.DisplayName)
                            FileCreatedDate = Catalog_File.DateLastModified
                            Set Catalog_File = Nothing
                        End If
CONTINUE:
                        On Error GoTo 0
                        For Count2 = 1 To Len(l_strFileName)
                            If Mid(l_strFileName, Count2, 1) = "." Then
                                l_lngCommaCount = Count2
                            End If
                        Next
                            
                        If InStr(l_strFileName, ".") > 0 And chkNoExtenders.Value = 0 Then
                            l_strReference = Mid(l_strFileName, 1, l_lngCommaCount - 1)
                        Else
                            l_strReference = l_strFileName
                        End If
                        
                        l_strSQL = "INSERT INTO EVENTS (cuser, FileModifiedDate, libraryID, altlocation, clipfilename, online, eventmediatype, internalreference, clipreference, soundlay, eventtitle, eventsubtitle, seriesID, " _
                            & "eventseries, companyID, companyname, eventtype, jobID, svenskprojectnumber, bigfilesize) VALUES ('"
                            
                        l_strSQL = l_strSQL & g_strUserInitials & "', '" & FormatSQLDate(FileCreatedDate) & "', " _
                            & sID & ", '" & QuoteSanitise(l_strFolderPath) & "', '" & QuoteSanitise(l_strFileName) & "', '" & l_lngOnline & "', 'FILE', '" & l_lngIntReference & "', '" _
                            & QuoteSanitise(l_strReference) & "', '" & FormatSQLDate(Now) & "', '" & QuoteSanitise(txtTitle.Text) & "', "
                            
                        If txtSubTitle.Text <> "" Then
                            l_strSQL = l_strSQL & "'" & QuoteSanitise(txtSubTitle.Text) & "', "
                        Else
                            l_strSQL = l_strSQL & "NULL, "
                        End If
                        
                        If Val(txtSeriesID.Text) <> 0 Then
                            l_strSQL = l_strSQL & Val(txtSeriesID.Text) & ", "
                        Else
                            l_strSQL = l_strSQL & "NULL, "
                        End If
                    
                        l_strSQL = l_strSQL & "'" & QuoteSanitise(cmbSeries.Text) & "', "
                        If Val(lblCompanyID.Caption) <> 0 Then
                            l_strSQL = l_strSQL & lblCompanyID.Caption & ", '" & QuoteSanitise(cmbCompany.Text) & "', "
                        Else
                            l_strSQL = l_strSQL & "1, 'Unassigned Company', "
                        End If
                        l_strSQL = l_strSQL & "'" & cmbPurpose.Text & "', "
                        l_strSQL = l_strSQL & l_lngJobID & ", "
                        
                        If txtSvenskProjectNumber.Text <> "" Then
                            l_strSQL = l_strSQL & "'" & txtSvenskProjectNumber.Text & "', "
                        Else
                            l_strSQL = l_strSQL & "NULL, "
                        End If
                        
                        If l_blnFailed = True Then
                            l_strSQL = l_strSQL & "NULL);"
                        Else
                            l_strSQL = l_strSQL & l_curFileSize & ");"
                        End If
                        
                        Debug.Print l_strSQL
                        cnn.Execute l_strSQL
                        l_lngEventID = LookupEventID(0, Item)
                        
                        If optSetAsMasterfile(0).Value <> 0 Then
                            SetData "events", "fileversion", "eventID", l_lngEventID, "Original Masterfile"
                        ElseIf optSetAsMasterfile(1).Value <> 0 Then
                            SetData "events", "fileversion", "eventID", l_lngEventID, "Transcode"
                        End If
                        
'                        'Update the DIVA Category
'                        Set l_rst = cnn.Execute("exec cet_Get_DIVA_Category @EventID=" & l_lngEventID)
'                        SetData "events", "clipsoundformat", "eventID", l_lngEventID, l_rst(0)
'                        l_rst.Close
                        
                        If chkMediaInfo.Value <> 0 Then
                            MediaData = GetMediaInfoOnFile(Replace(shFiles(0).CurrentFolder, "\\", "\\?\UNC\") & "\" & Item.DisplayName)
                            
                            If MediaData.txtFormat = "TTML" Then
                                SetData "events", "clipformat", "eventID", l_lngEventID, "ITT Subtitles"
    '                        ElseIf MediaData.txtFormat = "JPEG Image" Or MediaData.txtFormat = "PNG Image" Or MediaData.txtFormat = "BMP Image" Or MediaData.txtFormat = "GIF Image" Then
    '                            SetData "events", "clipformat", "eventID", l_lngEventID, MediaData.txtFormat
    '                            If getImgDim(ShFiles(0).CurrentFolder & "\" & Item.DisplayName, l_ImageDimensions, temp) = True Then
    '                                SetData "events", "clipverticalpixels", "eventID", l_lngEventID, l_ImageDimensions.height
    '                                SetData "events", "cliphorizontalpixels", "eventID", l_lngEventID, l_ImageDimensions.width
    '                            End If
                            ElseIf MediaData.txtFormat <> "" Then
                                SetData "events", "clipformat", "eventID", l_lngEventID, MediaData.txtFormat
                                SetData "events", "clipaudiocodec", "eventID", l_lngEventID, MediaData.txtAudioCodec
                                SetData "events", "clipframerate", "eventID", l_lngEventID, MediaData.txtFrameRate
                                SetData "events", "cbrvbr", "eventID", l_lngEventID, MediaData.txtCBRVBR
                                SetData "events", "cliphorizontalpixels", "eventID", l_lngEventID, MediaData.txtWidth
                                SetData "events", "clipverticalpixels", "eventID", l_lngEventID, MediaData.txtHeight
                                SetData "events", "videobitrate", "eventID", l_lngEventID, MediaData.txtVideoBitrate
                                SetData "events", "interlace", "eventID", l_lngEventID, MediaData.txtInterlace
                                If chkMediaInfoTimecodes.Value <> 0 Then
                                    SetData "events", "timecodestart", "eventID", l_lngEventID, MediaData.txtTimecodestart
                                    SetData "events", "timecodestop", "eventID", l_lngEventID, MediaData.txtTimecodestop
                                    SetData "events", "fd_length", "eventID", l_lngEventID, MediaData.txtDuration
                                End If
                                SetData "events", "audiobitrate", "eventID", l_lngEventID, MediaData.txtAudioBitrate
                                SetData "events", "trackcount", "eventID", l_lngEventID, MediaData.txtTrackCount
                                SetData "events", "channelcount", "eventID", l_lngEventID, MediaData.txtChannelCount
                                If Val(MediaData.txtAudioBitrate) + Val(MediaData.txtVideoBitrate) <> 0 Then
                                    SetData "events", "clipbitrate", "eventID", l_lngEventID, Val(MediaData.txtAudioBitrate) + Val(MediaData.txtVideoBitrate)
                                Else
                                    SetData "events", "clipbitrate", "eventID", l_lngEventID, Val(MediaData.txtOverallBitrate)
                                End If
                                SetData "events", "aspectratio", "eventID", l_lngEventID, MediaData.txtAspect
                                If MediaData.txtAspect = "4:3" Then
                                    SetData "events", "fourbythreeflag", "eventID", l_lngEventID, 1
                                Else
                                    SetData "events", "fourbythreeflag", "eventID", l_lngEventID, 0
                                End If
                                SetData "events", "geometriclinearity", "eventID", l_lngEventID, MediaData.txtGeometry
                                SetData "events", "clipcodec", "eventID", l_lngEventID, MediaData.txtVideoCodec
                                SetData "events", "colorspace", "eventID", l_lngEventID, MediaData.txtColorSpace
                                SetData "events", "chromasubsampling", "eventID", l_lngEventID, MediaData.txtChromaSubsmapling
'                                If Trim(" " & GetData("events", "eventtype", "eventID", l_lngEventID)) = "" Then SetData "events", "eventtype", "eventID", l_lngEventID, GetData("xref", "format", "description", MediaData.txtVideoCodec)
                                SetData "events", "mediastreamtype", "eventID", l_lngEventID, MediaData.txtStreamType
                                If MediaData.txtSeriesTitle <> "" Then
                                    SetData "events", "eventtitle", "eventID", l_lngEventID, MediaData.txtSeriesTitle
                                    SetData "events", "eventseries", "eventID", l_lngEventID, MediaData.txtSeriesNumber
                                    SetData "events", "eventsubtitle", "eventID", l_lngEventID, MediaData.txtProgrammeTitle
                                    SetData "events", "eventepisode", "eventID", l_lngEventID, MediaData.txtEpisodeNumber
                                    SetData "events", "notes", "eventID", l_lngEventID, MediaData.txtSynopsis
                                End If
                                SetData "events", "lastmediainfoquery", "eventID", l_lngEventID, FormatSQLDate(Now)
                            Else
                                l_strFileName = GetData("events", "clipfilename", "eventID", l_lngEventID)
                                Select Case UCase(Right(l_strFileName, 3))
                                    Case "ISO"
                                        SetData "events", "clipformat", "eventID", l_lngEventID, "DVD ISO Image"
                                    Case "PDF"
                                        SetData "events", "clipformat", "eventID", l_lngEventID, "PDF File"
                                    Case "PSD"
                                        SetData "events", "clipformat", "eventID", l_lngEventID, "PSD Still Image"
                                    Case "SCC"
                                        SetData "events", "clipformat", "eventID", l_lngEventID, "SCC File (EIA 608)"
                                    Case "STL"
                                        SetData "events", "clipformat", "eventID", l_lngEventID, "STL Subtitles"
                                    Case "TIF"
                                        SetData "events", "clipformat", "eventID", l_lngEventID, "TIF Still Image"
                                    Case "DOC", "DOCX"
                                        SetData "events", "clipformat", "eventID", l_lngEventID, "Word File"
                                    Case "XLS", "XLSX"
                                        SetData "events", "clipformat", "eventID", l_lngEventID, "XLS File"
                                    Case "XML"
                                        SetData "events", "clipformat", "eventID", l_lngEventID, "XML File"
                                    Case "RAR"
                                        SetData "events", "clipformat", "eventID", l_lngEventID, "RAR Archive"
                                    Case "ZIP"
                                        SetData "events", "clipformat", "eventID", l_lngEventID, "ZIP Archive"
                                    Case "TAR"
                                        SetData "events", "clipformat", "eventID", l_lngEventID, "TAR Archive"
                                    Case Else
                                        SetData "events", "clipformat", "eventID", l_lngEventID, "Other"
                                End Select
                            End If
                        End If
                        
                        If chkMD5.Value <> 0 And GetData("library", "format", "libraryID", GetData("events", "libraryID", "eventID", l_lngEventID)) = "DISCSTORE" Then
                            If Trim(" " & GetData("events", "md5checksum", "eventID", l_lngEventID) <> "") Then
                                l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, RequestName, RequesterEmail) VALUES ("
                                l_strSQL = l_strSQL & l_lngEventID & ", "
                                l_strSQL = l_strSQL & GetData("events", "libraryID", "eventID", l_lngEventID) & ", "
                                l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "MD5 Checksum") & ", "
                                l_strSQL = l_strSQL & "'" & g_strUserInitials & "', '" & g_strUserEmailAddress & "');"
                                Debug.Print l_strSQL
                                cnn.Execute l_strSQL
                            End If
                        End If
                        
                        'record a usage entry for the new clip
                        l_strSQL = "INSERT INTO eventusage ("
                        l_strSQL = l_strSQL & "eventID, dateused) VALUES ("
                        l_strSQL = l_strSQL & "'" & l_lngEventID & "', "
                        l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "'"
                        l_strSQL = l_strSQL & ");"
                        Debug.Print l_strSQL
                        cnn.Execute l_strSQL
        
                        'Record a History event
                        l_strSQL = "INSERT INTO eventhistory ("
                        l_strSQL = l_strSQL & "eventID, datesaved, username, description) VALUES ("
                        l_strSQL = l_strSQL & "'" & l_lngEventID & "', "
                        l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
                        l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
                        l_strSQL = l_strSQL & "'Created' "
                        l_strSQL = l_strSQL & ");"
                        
                        cnn.Execute l_strSQL
        
                    Else
                        MsgBox "Disc not initialised for CETA logging.", vbCritical, "Cannot Catalogue..."
                        GoTo EXITSUB
                    End If
                Else
                    'pick up the actual file name
                    l_strFileName = Item.DisplayName
                    lblCatalogprogress.Caption = Count & " of " & shFiles(0).ItemCount & ": Already Logged - " & l_strFileName
                    DoEvents
                    If chkSkipVerify.Value = 0 Then
                        API_OpenFile Replace(shFiles(0).CurrentFolder, "\\", "\\?\UNC\") & "\" & Item.DisplayName, l_lngFileHandle, l_curFileSize
                        API_CloseFile l_lngFileHandle
                        l_strSQL = "UPDATE events SET "
                        l_strSQL = l_strSQL & "soundlay = getdate(), bigfilesize = " & l_curFileSize & ", clipfilename = '" & QuoteSanitise(Item.DisplayName) & "' WHERE eventID = " & l_lngEventID & ";"
                        Debug.Print l_strSQL
                        cnn.Execute l_strSQL
                    End If
                End If
            ElseIf (chkImages.Value <> vbChecked And _
            (LCase(Right(Item.DisplayName, 4)) = ".jpg" Or LCase(Right(Item.DisplayName, 4)) = ".gif" Or LCase(Right(Item.DisplayName, 4)) = ".bmp" _
            Or LCase(Right(Item.DisplayName, 4)) = ".png" Or LCase(Right(Item.DisplayName, 4)) = ".tif" Or LCase(Right(Item.DisplayName, 4)) = ".dpx")) _
            Or (chkLogAndXML.Value <> vbChecked And _
            (LCase(Right(Item.DisplayName, 4)) = ".xml" Or LCase(Right(Item.DisplayName, 4)) = ".xmp" Or LCase(Right(Item.DisplayName, 4)) = ".log" _
            Or LCase(Right(Item.DisplayName, 4)) = ".txt" Or LCase(Right(Item.DisplayName, 17)) = ".pipelineschedule" Or LCase(Right(Item.DisplayName, 17)) = ".pipelineprint" _
            Or LCase(Right(Item.DisplayName, 4)) = ".fcp" Or LCase(Right(Item.DisplayName, 4)) = ".csv" Or LCase(Right(Item.DisplayName, 5)) = ".xlsx" _
            Or LCase(Right(Item.DisplayName, 5)) = ".docx" Or LCase(Right(Item.DisplayName, 4)) = ".doc" Or LCase(Right(Item.DisplayName, 4)) = ".xls")) _
            Then
                l_strFileName = Item.DisplayName
                lblCatalogprogress.Caption = Count & " of " & shFiles(0).ItemCount & ": Skipping " & l_strFileName
                DoEvents
            End If
        
        ElseIf Item.IsFolder And Item.DisplayName <> ".TemporaryItems" And Item.DisplayName <> ".ctdb" And Item.DisplayName <> ".Trashes" And (InStr(Item.DisplayName, "@") = 0) Then
            
            If chkExcludeSubfolders.Value = 0 Then
                
                l_strLastFolder = shFiles(0).CurrentFolder
                If Right(shFiles(0).CurrentFolder, 1) = "\" Then
                    If FSO.FolderExists(Replace(shFiles(0).CurrentFolder, "\\", "\\?\UNC\") & Item.DisplayName) Then
                        Catalog_Folder shFiles(0).CurrentFolder & Item.DisplayName, l_lngJobID
                    End If
                Else
                    If FSO.FolderExists(Replace(shFiles(0).CurrentFolder, "\\", "\\?\UNC\") & "\" & Item.DisplayName) Then
                        Catalog_Folder shFiles(0).CurrentFolder & "\" & Item.DisplayName, l_lngJobID
                    End If
                End If
                shFiles(0).CurrentFolder = l_strLastFolder
            End If
            
        End If
        
    Next

End If

EXITSUB:

cnn.Close
Set cnn = Nothing
lblCatalogprogress.Caption = ""
txtJobID.Text = ""
g_FileOpInProgress = False
MsgBox "Done"

Exit Sub

FILE_OPEN_ERROR1:
l_blnFailed = True
Resume CONTINUE

End Sub

Private Sub cmdClearTitle_Click()

If MsgBox("Are you Sure", vbYesNo, "Clearing Types Title / Ep Information") = vbNo Then Exit Sub

txtTitle.Text = ""
cmbSeries.Text = ""
txtSubTitle.Text = ""
txtSeriesID.Text = ""
cmbCompany.Text = ""
lblCompanyID.Caption = ""

End Sub

Private Sub cmdEnable_Click()

If cmdEnable.Caption = "Enable Options" Then
    fraOptions.Enabled = True
    cmdEnable.Caption = "Disable Options"
Else
    fraOptions.Enabled = False
    cmdEnable.Caption = "Enable Options"
End If

End Sub

Private Sub cmdFileList_Click()

If MsgBox("List source folder to Clipboard", vbYesNo + vbDefaultButton2) = vbNo Then Exit Sub

Dim Item As ListItem, MessageString As String, Count As Long

MessageString = ""

If shFiles(0).ItemCount > 0 Then

    For Count = 1 To shFiles(0).ItemCount
        
        Set Item = shFiles(0).ListItem(Count)
        If Not Item.IsFolder And Not Item.IsLibrary Then
            MessageString = MessageString & Item.DisplayName & vbCrLf
        End If
    
    Next

End If

If MessageString <> "" Then
    Clipboard.Clear
    Clipboard.SetText MessageString
End If

End Sub

Private Sub cmdFileOp_Click(Index As Integer)
        '<EhHeader>
        On Error GoTo cmdFileOp_Click_Err
        '</EhHeader>


        'Check whether CETA is locked and do not allow any file operations if it is.
       If IsCETALocked = True Then
            MsgBox "CETA is locked - With logging not possible " & vbCrLf & "the operation cannot be performed at this time", vbCritical, "CETA Locked"
            Exit Sub
        End If

        g_FileOpInProgress = True
        ShFolders(0).Enabled = False
        ShDrives(0).Enabled = False
        shFiles(0).Enabled = False
        ShFolders(1).Enabled = False
        ShDrives(1).Enabled = False
        shFiles(1).Enabled = False
        
        'CSBmk <Declarations>
        Dim lFileOp      As Long

        Dim lResult      As Long

        Dim i            As Integer
        
        Dim l_lngEventID As Long, l_lngSourceLibraryID As Long, l_lngDestinationLibraryID As Long, l_lngNewEventID As Long, l_lngOnline As Long, l_lngOldEventID As Long

        Dim c            As ADODB.Connection, cnn2 As ADODB.Connection

        Dim l_strSQL     As String, SQL As String, MessageString As String

        Dim li As ListItem, l_strDestFile As String, l_strSourceFile As String, l_strDestinationUNCDrive As String, l_strSourceUNCDrive As String

        Dim sID As String
        Dim l_strFileToOpen As String

        Dim rs As ADODB.Recordset, OldName As String, l_strSrcLocation As String, l_strDestLocation As String
        Dim EditText As String, temp As String, NewLocation As String
        
        Dim l_blnFlag As Boolean, l_blnForceUnhide As Boolean
        
        Dim FreeBytesAvailableToCaller As Currency, TotalNumberOfBytes As Currency, TotalNumberofFreeBytes As Currency, l_curSuccess As Currency
        
        Dim l_strNewFolder As String
        
        Dim l_blnLiveTransfersThisTime As Boolean
        
        Dim FSO As Scripting.FileSystemObject

        Set FSO = New Scripting.FileSystemObject
        
        Dim l_blnMakeFileRequest As Boolean, l_blnSkipRequest As Boolean
        
        Select Case Index

            Case 0
                lFileOp = FO_COPY

            Case 1
                lFileOp = FO_MOVE

            Case 2
                lFileOp = FO_DELETE
            
            Case 3
                lFileOp = FO_FOLDERCOPY
            
            Case 4
                lFileOp = FO_FOLDERMOVE
            
            Case 5
                lFileOp = FO_FOLDERDELETE
            
                        
        End Select

        l_strDestFile = shFiles(1).CurrentFolder
        l_strSourceFile = shFiles(0).CurrentFolder
        
'        'work out the UNC path from the destination folder
'        If Left(l_strDestFile, 2) <> "\\" Then
'            l_strDestinationUNCDrive = LetterToUNC(Left(l_strDestFile, 2))
'        Else
'            temp = Right(l_strDestFile, Len(l_strDestFile) - 2)
'            l_strDestinationUNCDrive = "\\"
'            i = InStr(temp, "\")
'            If i <> 0 Then l_strDestinationUNCDrive = l_strDestinationUNCDrive & Left(temp, i)
'            temp = Mid(temp, i + 1)
'            i = InStr(temp, "\")
'            If i <> 0 Then
'                l_strDestinationUNCDrive = l_strDestinationUNCDrive & Left(temp, i - 1)
'            Else
'                l_strDestinationUNCDrive = l_strDestinationUNCDrive & temp
'            End If
'        End If
'
'        'work out the UNC path from the source folder
'        If Left(l_strSourceFile, 2) <> "\\" Then
'            l_strSourceUNCDrive = LetterToUNC(Left(l_strSourceFile, 2))
'        Else
'            temp = Right(l_strSourceFile, Len(l_strSourceFile) - 2)
'            l_strSourceUNCDrive = "\\"
'            i = InStr(temp, "\")
'            If i <> 0 Then l_strSourceUNCDrive = l_strSourceUNCDrive & Left(temp, i)
'            temp = Mid(temp, i + 1)
'            i = InStr(temp, "\")
'            If i <> 0 Then
'                l_strSourceUNCDrive = l_strSourceUNCDrive & Left(temp, i - 1)
'            Else
'                l_strSourceUNCDrive = l_strSourceUNCDrive & temp
'            End If
'        End If

        l_strFileToOpen = ""
        sID = ""
        
        'Check the Source Library ID (necessary for folder moves) etc.
            
        'check for the libraryID.sys file on the root of this drive
         
        On Error GoTo PROC_NoRootFile
                        
        shFiles(2).CurrentFolder = shFiles(0).CurrentFolder
        If Left(l_strSourceFile, 1) <> "\" Then
            l_strFileToOpen = Left$(l_strSourceFile, 2)
        Else
            l_strFileToOpen = l_strSourceFile
        End If
        
        Do While Not FSO.FileExists(l_strFileToOpen & "\libraryID.sys") And Not FSO.FileExists(l_strFileToOpen & "\.libraryID.sys")
            l_strFileToOpen = Left(shFiles(2).CurrentFolder, Len(shFiles(2).CurrentFolder) - Len(shFiles(2).CurrentFolderDisplayName) - 1)
            shFiles(2).CurrentFolder = l_strFileToOpen
        Loop
        
        If FSO.FileExists(l_strFileToOpen & "\libraryID.sys") Then
            Open l_strFileToOpen & "\libraryID.sys" For Input As 1
            Line Input #1, sID
            Close 1
        ElseIf FSO.FileExists(l_strFileToOpen & "\.libraryID.sys") Then
            Open l_strFileToOpen & "\.libraryID.sys" For Input As 1
            Line Input #1, sID
            Close 1
        End If
                 
PROC_NoRootFile:
         
        On Error GoTo cmdFileOp_Click_Err
         
        If sID <> "" Then
            l_lngSourceLibraryID = Val(sID)
        Else
             
            'ask if they want to scan a barcode
            i = MsgBox("The Source you have selected has not been configured for use with CETA. Do you want to scan a barcode for the source disk now?", vbQuestion + vbYesNo)
            
            'if they do want to scan a barcode
            If i = vbYes Then
            
                'pop up an input box to enter the barcode
                l_strSourceUNCDrive = InputBox("Please scan the barcode on the disk", "Barcode Entry")
                
                'work out the library id from the barcode
                l_lngSourceLibraryID = GetData("library", "libraryID", "barcode", l_strSourceUNCDrive)
                
                'still no valid record so quit
                If l_lngSourceLibraryID = 0 Then
                    g_FileOpInProgress = False
                    ShFolders(0).Enabled = True
                    ShDrives(0).Enabled = True
                    shFiles(0).Enabled = True
                    ShFolders(1).Enabled = True
                    ShDrives(1).Enabled = True
                    shFiles(1).Enabled = True
                    Exit Sub
                End If
            
            Else
        
                'they dont want to scan a barcode so quit
                g_FileOpInProgress = False
                ShFolders(0).Enabled = True
                ShDrives(0).Enabled = True
                shFiles(0).Enabled = True
                ShFolders(1).Enabled = True
                ShDrives(1).Enabled = True
                shFiles(1).Enabled = True
                Exit Sub
            
            End If
        End If
        
        l_strFileToOpen = ""
        sID = ""
        'are we moving or copying - ie. do we need to even check the destination?
        If (lFileOp = FO_COPY Or lFileOp = FO_MOVE Or lFileOp = FO_FOLDERCOPY Or lFileOp = FO_FOLDERMOVE) Then
            
            'check for the libraryID.sys file on the root of this drive
            
            On Error GoTo PROC_NoRootFile1
                                            
            If Left(l_strDestFile, 1) <> "\" Then
                l_strFileToOpen = Left$(l_strDestFile, 2)
            Else
                l_strFileToOpen = l_strDestinationUNCDrive
            End If
            
            l_strFileToOpen = shFiles(1).CurrentFolder
            shFiles(2).CurrentFolder = l_strFileToOpen
            Do While Not FSO.FileExists(l_strFileToOpen & "\libraryID.sys") And Not FSO.FileExists(l_strFileToOpen & "\.libraryID.sys")
                l_strFileToOpen = Left(shFiles(2).CurrentFolder, Len(shFiles(2).CurrentFolder) - Len(shFiles(2).CurrentFolderDisplayName) - 1)
                shFiles(2).CurrentFolder = l_strFileToOpen
            Loop
            
            If FSO.FileExists(l_strFileToOpen & "\libraryID.sys") Then
                Open l_strFileToOpen & "\libraryID.sys" For Input As 1
                Line Input #1, sID
                Close 1
            ElseIf FSO.FileExists(l_strFileToOpen & "\.libraryID.sys") Then
                Open l_strFileToOpen & "\.libraryID.sys" For Input As 1
                Line Input #1, sID
                Close 1
            End If
            
PROC_NoRootFile1:

            On Error GoTo cmdFileOp_Click_Err
            
            If sID <> "" Then
                l_lngDestinationLibraryID = Val(sID)
                l_strDestinationUNCDrive = GetData("library", "subtitle", "libraryID", l_lngDestinationLibraryID)
            Else
                
                'ask if they want to scan a barcode
                i = MsgBox("The destination you have selected has not been configured for use with CETA. Do you want to scan a barcode for the destination disk now?", vbQuestion + vbYesNo)
                
                'if they do want to scan a barcode
                If i = vbYes Then
                
                    'pop up an input box to enter the barcode
                    l_strDestinationUNCDrive = InputBox("Please scan the barcode on the disk", "Barcode Entry")
                    
                    'work out the library id from the barcode
                    l_lngDestinationLibraryID = GetData("library", "libraryID", "barcode", l_strDestinationUNCDrive)
                    l_strDestinationUNCDrive = GetData("library", "subtitle", "libraryID", l_lngDestinationLibraryID)
                    
                    'still no valid record so quit
                    If l_lngDestinationLibraryID = 0 Then
                        g_FileOpInProgress = False
                        ShFolders(0).Enabled = True
                        ShDrives(0).Enabled = True
                        shFiles(0).Enabled = True
                        ShFolders(1).Enabled = True
                        ShDrives(1).Enabled = True
                        shFiles(1).Enabled = True
                        Exit Sub
                    End If
                
                Else

                    'they dont want to scan a barcode so quit
                    g_FileOpInProgress = False
                    ShFolders(0).Enabled = True
                    ShDrives(0).Enabled = True
                    shFiles(0).Enabled = True
                    ShFolders(1).Enabled = True
                    ShDrives(1).Enabled = True
                    shFiles(1).Enabled = True
                    Exit Sub
                
                End If
           End If
        End If
            
        'Check whether we are copying to LTO5, and if we are, set the 'Yes to All' flag, to deal with the permissions issues on LTO5 LTFS
        If GetData("library", "format", "libraryID", l_lngDestinationLibraryID) = "LTO5" Or GetData("library", "format", "libraryID", l_lngDestinationLibraryID) = "LTO6" _
        Or GetData("library", "format", "libraryID", l_lngDestinationLibraryID) = "LTO7" Then
            chkYesToAll.Value = vbChecked
            chkDontBlankMasterfile.Value = vbChecked
        End If
        
        'Set the FileOP parameters according to the flags
        If chkYesToAll.Value = vbChecked Then
            ShellFileOperation.Flags = ShellFileOperation.Flags Or SFONoConfirmation
        End If
        If chkRename.Value = vbChecked Then
            ShellFileOperation.Flags = ShellFileOperation.Flags Or SFORenameOnCollision
        End If
        
        ShellFileOperation.AutoClearFilesAfterOperation = True
        
        'Reset the normal error routines
        On Error GoTo cmdFileOp_Click_Err
        
        If l_lngSourceLibraryID = 0 Or (l_lngDestinationLibraryID = 0 And (lFileOp = FO_COPY Or lFileOp = FO_MOVE)) Then
            MsgBox "There has been a problem determining either the source or destination Library ID." & vbCrLf & "This file operation cannot continue", vbCritical, "Problem..."
            g_FileOpInProgress = False
            ShFolders(0).Enabled = True
            ShDrives(0).Enabled = True
            shFiles(0).Enabled = True
            ShFolders(1).Enabled = True
            ShDrives(1).Enabled = True
            shFiles(1).Enabled = True
            Exit Sub
        End If
        
        If lFileOp = FO_DELETE Then
            'CSBmk <Loop through files>
            Set li = shFiles(0).FirstSelectedItem
            MessageString = ""
            Do While li Is Nothing = False
                If li.IsFolder Then
                    MessageString = MessageString & "Folder: " & li.DisplayName & vbCrLf
                Else
                    MessageString = MessageString & "File: " & li.DisplayName & vbCrLf
                End If
                Set li = shFiles(0).NextSelectedItem
            Loop
            If MessageString <> "" Then
                If MsgBox("Are you sure you wish to delete:" & vbCrLf & MessageString, vbYesNo + vbQuestion, "File Deletion") = vbNo Then
                    g_FileOpInProgress = False
                    ShFolders(0).Enabled = True
                    ShDrives(0).Enabled = True
                    shFiles(0).Enabled = True
                    ShFolders(1).Enabled = True
                    ShDrives(1).Enabled = True
                    shFiles(1).Enabled = True
                    Exit Sub
                End If
                ShellFileOperation.Flags = ShellFileOperation.Flags Or SFONoConfirmation
            End If
        
        End If
        
        'Do a run through all the selected items and check whether they have CETA records, and if not, ask whether a live transaction should be done (if allowed).
        'only if we are doing offline requests and this user dows not normally do online requests
        If GetCETASetting("DoOfflineCFMRequests") <> 0 _
        And (CheckAccess("/DoOnlineCFMRequests", True) = False _
        Or (GetData("library", "format", "libraryID", l_lngDestinationLibraryID) <> "LTO5" _
        And GetData("library", "format", "libraryID", l_lngDestinationLibraryID) <> "LTO6" _
        And GetData("library", "format", "libraryID", l_lngDestinationLibraryID) <> "LTO7" _
        And GetData("library", "format", "libraryID", l_lngSourceLibraryID) <> "LTO5" _
        And GetData("library", "format", "libraryID", l_lngSourceLibraryID) <> "LTO6" _
        And GetData("library", "format", "libraryID", l_lngSourceLibraryID) <> "LTO7" _
        And GetData("library", "format", "libraryID", l_lngDestinationLibraryID) <> "MOBILEDISC" _
        And GetData("library", "format", "libraryID", l_lngSourceLibraryID) <> "MOBILEDISC")) _
        Then
            Set li = shFiles(0).FirstSelectedItem
            Do While li Is Nothing = False
                'Lookup the item
                l_lngEventID = LookupEventID(0, li, l_lngSourceLibraryID)
                
                If l_lngEventID = 0 And l_blnLiveTransfersThisTime = False Then
                    If CheckAccess("/DoOnlineCFMRequestsOnTheFly", True) And (lFileOp = FO_COPY Or lFileOp = FO_MOVE Or lFileOp = FO_DELETE) Then
                        If MsgBox("There are selected items that do not have individual CETA file records." & vbCrLf & _
                        "This means that CFM cannot submit Requests to the Request queue." & vbCrLf & _
                        "Do you wish to go ahead and do the file action live from this machine", vbYesNo + vbDefaultButton2, "Items found with no CETA records") = vbYes Then
                            l_blnLiveTransfersThisTime = True
                            Exit Do
                        Else
                            g_FileOpInProgress = False
                            ShFolders(0).Enabled = True
                            ShDrives(0).Enabled = True
                            shFiles(0).Enabled = True
                            ShFolders(1).Enabled = True
                            ShDrives(1).Enabled = True
                            shFiles(1).Enabled = True
                            Exit Sub
                        End If
                    ElseIf (lFileOp = FO_COPY Or lFileOp = FO_MOVE Or lFileOp = FO_DELETE) Then
                        MsgBox "There are selected items that do not have CETA file records." & vbCrLf & _
                        "Either make sure all the items have CETA file records, " & vbCrLf & _
                        "or have someone do the file actions who has permission" & vbCrLf & _
                        "to do Live Requests on the fly." & vbCrLf & _
                        "or, if it is a folder, use the folder action buttons." & vbCrLf & _
                        "This file action has not taken place", vbCritical, "Error - CETA Permission failure."
                        g_FileOpInProgress = False
                        ShFolders(0).Enabled = True
                        ShDrives(0).Enabled = True
                        shFiles(0).Enabled = True
                        ShFolders(1).Enabled = True
                        ShDrives(1).Enabled = True
                        shFiles(1).Enabled = True
                        Exit Sub
                    End If
                End If
                Set li = shFiles(0).NextSelectedItem
            Loop
        End If
        
        'CSBmk <Loop through files>
        Set li = shFiles(0).FirstSelectedItem
        
        'Flag for whether we are on the first item of a multiple selection
        l_blnFlag = True
        
        'Then carry on and loop through doing the file operations.
        Do While li Is Nothing = False
            
            'If the Ceta Setting DoOfflineCFMRequests is non-zero or if the user setting /DoOnlineCFMRequests is present then do the request live in CFM, otherwise do an offline file request
            If GetCETASetting("DoOfflineCFMRequests") = 0 _
            And (lFileOp = FO_COPY Or lFileOp = FO_MOVE Or lFileOp = FO_DELETE) _
            Or CheckAccess("/DoOnlineCFMRequests", True) _
            Or l_blnLiveTransfersThisTime = True _
            Or GetData("library", "format", "libraryID", l_lngDestinationLibraryID) = "LTO5" _
            Or GetData("library", "format", "libraryID", l_lngDestinationLibraryID) = "LTO6" _
            Or GetData("library", "format", "libraryID", l_lngDestinationLibraryID) = "LTO7" _
            Or GetData("library", "format", "libraryID", l_lngSourceLibraryID) = "LTO5" _
            Or GetData("library", "format", "libraryID", l_lngSourceLibraryID) = "LTO6" _
            Or GetData("library", "format", "libraryID", l_lngSourceLibraryID) = "LTO7" _
            Or GetData("library", "format", "libraryID", l_lngDestinationLibraryID) = "MOBILEDISC" _
            Or GetData("library", "format", "libraryID", l_lngSourceLibraryID) = "MOBILEDISC" _
            Then
                l_blnSkipRequest = False
                l_strSourceFile = li.Path
                
    '            Select Case lFileOp
    '
    '                Case FO_DELETE
    '                    If l_blnFlag = True Then
    '                        If MsgBox("Are you sure you wish to delete" & vbCrLf & l_strSourceFile & " ?", vbYesNo + vbQuestion, "File Deletion") = vbNo Then
    '                            g_FileOpInProgress = False
    '                            Exit Sub
    '                        End If
    '                        l_blnFlag = False
    '                        ShellFileOperation.Flags = ShellFileOperation.Flags Or SFONoConfirmation
    '                    End If
    '                Case Else
    '                    'Nothing
    '            End Select
    
                ShellFileOperation.SourceFiles.Clear
                ShellFileOperation.SourceFiles.Add l_strSourceFile
                ShellFileOperation.DestinationFiles.Clear
                ShellFileOperation.DestinationFiles.Add l_strDestFile
                ShellFileOperation.Flags = ShellFileOperation.Flags Or SFOSimpleProgressDialog
                
                lblCatalogprogress.Caption = l_strSourceFile
                'lblCatalogprogress.Refresh
                DoEvents
    
                Select Case lFileOp
                    
                    Case FO_COPY
                        If Trim(" " & GetData("library", "format", "libraryID", l_lngDestinationLibraryID)) = "DISCSTORE" Then
                            l_curSuccess = GetDiskFreeSpaceEx(GetData("library", "subtitle", "libraryID", l_lngDestinationLibraryID), FreeBytesAvailableToCaller, TotalNumberOfBytes, TotalNumberofFreeBytes)
                            If Int(Int(TotalNumberofFreeBytes) / Int(TotalNumberOfBytes) * 100) < Val(Trim(" " & GetData("library", "MinimumFreePercentage", "libraryID", l_lngDestinationLibraryID))) Then
                                'Not enough Min space on disc
                                MsgBox "You are trying to copy to a drive that has less than its min free space percentage available." & vbCrLf & "Operation aborted", vbCritical
                                g_FileOpInProgress = False
                                ShFolders(0).Enabled = True
                                ShDrives(0).Enabled = True
                                shFiles(0).Enabled = True
                                ShFolders(1).Enabled = True
                                ShDrives(1).Enabled = True
                                shFiles(1).Enabled = True
                                Exit Sub
                            End If
                        End If
                        If ShellFileOperation.DoCopy = False Or ShellFileOperation.AreAnyOperationsAborted = True Then
                            g_FileOpInProgress = False
                            lblCatalogprogress.Caption = ""
                            ShFolders(0).Enabled = True
                            ShDrives(0).Enabled = True
                            shFiles(0).Enabled = True
                            ShFolders(1).Enabled = True
                            ShDrives(1).Enabled = True
                            shFiles(1).Enabled = True
                            Exit Sub
                        End If
                    
                    Case FO_MOVE
                        l_lngEventID = LookupEventID(0, li, l_lngSourceLibraryID)
                        If l_lngEventID <> 0 Then
                            If GetData("events", "sound_stereo_russian", "eventID", l_lngEventID) = 1 Then
                                MsgBox "You are trying to move a locked file." & vbCrLf & "Operation aborted", vbCritical
                                g_FileOpInProgress = False
                                ShFolders(0).Enabled = True
                                ShDrives(0).Enabled = True
                                shFiles(0).Enabled = True
                                ShFolders(1).Enabled = True
                                ShDrives(1).Enabled = True
                                shFiles(1).Enabled = True
                                Exit Sub
                            End If
                        End If
                        If l_lngSourceLibraryID <> l_lngDestinationLibraryID Then
                            If Trim(" " & GetData("library", "format", "libraryID", l_lngDestinationLibraryID)) = "DISCSTORE" Then
                                l_curSuccess = GetDiskFreeSpaceEx(GetData("library", "subtitle", "libraryID", l_lngDestinationLibraryID), FreeBytesAvailableToCaller, TotalNumberOfBytes, TotalNumberofFreeBytes)
                                If Int(Int(TotalNumberofFreeBytes) / Int(TotalNumberOfBytes) * 100) < Val(Trim(" " & GetData("library", "MinimumFreePercentage", "libraryID", l_lngDestinationLibraryID))) Then
                                    'Not enough Min space on disc
                                    MsgBox "You are trying to move to a drive that has less than its min free space percentage available." & vbCrLf & "Operation aborted", vbCritical
                                    g_FileOpInProgress = False
                                    ShFolders(0).Enabled = True
                                    ShDrives(0).Enabled = True
                                    shFiles(0).Enabled = True
                                    ShFolders(1).Enabled = True
                                    ShDrives(1).Enabled = True
                                    shFiles(1).Enabled = True
                                    Exit Sub
                                End If
                            End If
                        End If
                        If ShellFileOperation.DoMove = False Or ShellFileOperation.AreAnyOperationsAborted = True Then
                            g_FileOpInProgress = False
                            lblCatalogprogress.Caption = ""
                            ShFolders(0).Enabled = True
                            ShDrives(0).Enabled = True
                            shFiles(0).Enabled = True
                            ShFolders(1).Enabled = True
                            ShDrives(1).Enabled = True
                            shFiles(1).Enabled = True
                            Exit Sub
                        End If
                    
                    Case FO_DELETE
                        l_lngEventID = LookupEventID(0, li, l_lngSourceLibraryID)
                        If l_lngEventID <> 0 Then
                            If GetData("events", "sound_stereo_russian", "eventID", l_lngEventID) = 1 Then
                                MsgBox "You are trying to delete a locked file." & vbCrLf & "Operation aborted", vbCritical
                                g_FileOpInProgress = False
                                ShFolders(0).Enabled = True
                                ShDrives(0).Enabled = True
                                shFiles(0).Enabled = True
                                ShFolders(1).Enabled = True
                                ShDrives(1).Enabled = True
                                shFiles(1).Enabled = True
                                Exit Sub
                            End If
                        End If
                        If ShellFileOperation.DoDelete = False Or ShellFileOperation.AreAnyOperationsAborted = True Then
                            g_FileOpInProgress = False
                            lblCatalogprogress.Caption = ""
                            ShFolders(0).Enabled = True
                            ShDrives(0).Enabled = True
                            shFiles(0).Enabled = True
                            ShFolders(1).Enabled = True
                            ShDrives(1).Enabled = True
                            shFiles(1).Enabled = True
                            Exit Sub
                        End If
                    
                    Case Else
                        MsgBox "Live Requests canot be performed with these buttons. No action has been taken"
                        g_FileOpInProgress = False
                        lblCatalogprogress.Caption = ""
                        ShFolders(0).Enabled = True
                        ShDrives(0).Enabled = True
                        shFiles(0).Enabled = True
                        ShFolders(1).Enabled = True
                        ShDrives(1).Enabled = True
                        shFiles(1).Enabled = True
                        Exit Sub
                        
                End Select
                
                'Check whether the source item was a folder
                
                'open the database connection
                
                On Error GoTo Database_Unavailable
                Set c = New ADODB.Connection
                c.Open g_strConnection
                On Error GoTo cmdFileOp_Click_Err
                
                If li.IsFolder Then
                    'Routines for dealing with a folder copy or rename
               
                    l_lngEventID = LookupEventID(0, li, l_lngSourceLibraryID)
                    
                    Select Case Index
                        Case 0 'Folder Copy
    
                            'If the item is a folder then some hairy algorithms are needed to update all files which fall on that folder tree
                            Set rs = New ADODB.Recordset
                            
                            If Left(l_strSourceFile, 2) <> "\\" Then
                                l_strSrcLocation = Right(l_strSourceFile, Len(l_strSourceFile) - 3)
                            Else
                                l_strSrcLocation = Mid(l_strSourceFile, Len(GetData("library", "subtitle", "libraryID", l_lngSourceLibraryID)) + 2)
                            End If
                            
                            If Left(l_strDestFile, 2) <> "\\" Then
                                l_strDestLocation = Right(l_strDestFile, Len(l_strDestFile) - 3)
                            Else
                                l_strDestLocation = Mid(l_strDestFile, Len(GetData("library", "subtitle", "libraryID", l_lngDestinationLibraryID)) + 2)
                            End If
                            
                            OldName = l_strSrcLocation
                            i = InStr(l_strSrcLocation, "\")
                            Do While i <> 0
                                OldName = Right(OldName, Len(OldName) - i)
                                i = InStr(OldName, "\")
                            Loop
                                                        
                            'libraryID = " & sID & " AND
                            l_strSQL = "SELECT eventID, altlocation, clipfilename FROM events WHERE (altlocation = '" & QuoteSanitise(l_strSrcLocation) & "' OR altlocation LIKE '" & QuoteSanitise(l_strSrcLocation) & "\%') and libraryID = " & l_lngSourceLibraryID & " AND system_deleted = 0;"
                            rs.Open l_strSQL, c, adOpenDynamic, adLockOptimistic
                            If Not rs.EOF Then
                                rs.MoveFirst
                                Do While Not rs.EOF
                                    If GetData("library", "format", "libraryID", l_lngDestinationLibraryID) = "DISCSTORE" Then
                                        l_lngOnline = 1
                                    Else
                                        l_lngOnline = 0
                                    End If
                                    temp = Trim(" " & rs("altlocation"))
                                    If Len(temp) > Len(l_strSrcLocation) Then
                                        temp = Right(temp, Len(temp) - Len(l_strSrcLocation) - 1)
                                    Else
                                        temp = ""
                                    End If
                                    If l_strDestLocation <> "" Then
                                        NewLocation = l_strDestLocation & "\" & OldName
                                    Else
                                        NewLocation = OldName
                                    End If
                                    If temp <> "" Then
                                        NewLocation = NewLocation & "\" & temp
                                    End If
                                    l_lngOldEventID = GetEventIDForFileUsingLibraryID(l_lngDestinationLibraryID, NewLocation, rs("clipfilename"))
                                    l_lngNewEventID = CopyEventToLibraryID(rs("eventID"), l_lngDestinationLibraryID, chkDontBlankMasterfile.Value)
                                    If l_lngOldEventID <> 0 Then
                                        l_strSQL = "DELETE FROM portalpermission WHERE eventID = " & l_lngOldEventID & ";"
                                        c.Execute l_strSQL
                                        l_strSQL = "DELETE FROM distributionpermission WHERE eventID = " & l_lngOldEventID & ";"
                                        c.Execute l_strSQL
    '                                    l_strSQL = "DELETE FROM eventsegment WHERE eventID = " & l_lngOldEventID & ";"
    '                                    c.Execute l_strSQL
    '                                    l_strSQL = "DELETE FROM iTunes_Clip_Advisory WHERE eventID = " & l_lngOldEventID & ";"
    '                                    c.Execute l_strSQL
    '                                    l_strSQL = "DELETE FROM iTunes_Product WHERE eventID = " & l_lngOldEventID & ";"
    '                                    c.Execute l_strSQL
                                        l_strSQL = "UPDATE events SET system_deleted = 1 WHERE eventID = " & l_lngOldEventID & ";"
                                        c.Execute l_strSQL
                                        'Record a History event
                                        l_strSQL = "INSERT INTO eventhistory ("
                                        l_strSQL = l_strSQL & "eventID, datesaved, username, description) VALUES ("
                                        l_strSQL = l_strSQL & "'" & l_lngOldEventID & "', "
                                        l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
                                        l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
                                        l_strSQL = l_strSQL & "'Clip Deleted' "
                                        l_strSQL = l_strSQL & ");"
                                        c.Execute l_strSQL
                                    End If
                                    SetData "events", "altlocation", "eventID", l_lngNewEventID, QuoteSanitise(NewLocation)
                                    SetData "events", "libraryID", "eventID", l_lngNewEventID, l_lngDestinationLibraryID
                                    SetData "events", "online", "eventID", l_lngNewEventID, l_lngOnline
                                    If UCase(Left(NewLocation, 10)) = "AUTODELETE" Then
                                        SetData "events", "AutoDeleteDate", "eventID", l_lngNewEventID, Format(Now, "YYYY-MM-DD HH:NN:SS")
                                    End If
                                    
                                    l_strSQL = "INSERT INTO eventusage (eventID, dateused) VALUES (" & rs("eventID") & ", getdate());"
                                    c.Execute l_strSQL
                                    
                                    l_strSQL = "INSERT INTO eventusage (eventID, dateused) VALUES (" & l_lngNewEventID & ", getdate());"
                                    c.Execute l_strSQL
                                    
                                    'Record a History event
                                    l_strSQL = "INSERT INTO eventhistory ("
                                    l_strSQL = l_strSQL & "eventID, datesaved, username, description) VALUES ("
                                    l_strSQL = l_strSQL & "'" & l_lngNewEventID & "', "
                                    l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
                                    l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
                                    l_strSQL = l_strSQL & "'Created' "
                                    l_strSQL = l_strSQL & ");"
                                    c.Execute l_strSQL
                                    rs.MoveNext
                                Loop
                            End If
                            rs.Close
                            Set rs = Nothing
                        
                            'Then check and copy any individual records for that folder
                            
                            If l_lngEventID <> 0 Then
                                l_lngOldEventID = LookupEventID(1, li, l_lngDestinationLibraryID)
                                l_lngNewEventID = CopyEventToLibraryID(l_lngEventID, l_lngDestinationLibraryID, chkDontBlankMasterfile.Value)
                                If l_lngOldEventID <> 0 Then
                                    l_strSQL = "DELETE FROM portalpermission WHERE eventID = " & l_lngOldEventID & ";"
                                    c.Execute l_strSQL
                                    l_strSQL = "DELETE FROM distributionpermission WHERE eventID = " & l_lngOldEventID & ";"
        '                            c.Execute l_strSQL
        '                            l_strSQL = "DELETE FROM eventsegment WHERE eventID = " & l_lngOldEventID & ";"
        '                            c.Execute l_strSQL
        '                            l_strSQL = "DELETE FROM iTunes_Clip_Advisory WHERE eventID = " & l_lngOldEventID & ";"
        '                            c.Execute l_strSQL
        '                            l_strSQL = "DELETE FROM iTunes_product WHERE eventID = " & l_lngOldEventID & ";"
        '                            c.Execute l_strSQL
                                    l_strSQL = "UPDATE events SET system_deleted = 1 WHERE eventID = " & l_lngOldEventID & ";"
                                    c.Execute l_strSQL
                                    'Record a History event
                                    l_strSQL = "INSERT INTO eventhistory ("
                                    l_strSQL = l_strSQL & "eventID, datesaved, username, description) VALUES ("
                                    l_strSQL = l_strSQL & "'" & l_lngOldEventID & "', "
                                    l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
                                    l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
                                    l_strSQL = l_strSQL & "'Clip Deleted' "
                                    l_strSQL = l_strSQL & ");"
                                    c.Execute l_strSQL
                                End If
                            Else
                                l_lngOldEventID = 0
                                l_lngNewEventID = 0
                            End If
                            'Then perform folder actions if there actually is a record
                            If l_lngNewEventID <> 0 Then
                            
                                'check to see if the same drive is being used
                                If UCase(GetData("library", "subtitle", "libraryID", l_lngSourceLibraryID)) <> UCase(GetData("library", "subtitle", "libraryID", l_lngDestinationLibraryID)) Then
                                    
                                    'fire off the SQL
                                    If GetData("library", "format", "libraryID", l_lngDestinationLibraryID) = "DISCSTORE" Then
                                        l_lngOnline = 1
                                    Else
                                        l_lngOnline = 0
                                    End If
                                    l_strSQL = "UPDATE events SET libraryID = '" & l_lngDestinationLibraryID & "', online = '" & l_lngOnline & _
                                            "' WHERE eventID = '" & l_lngNewEventID & "';"
                                    c.Execute l_strSQL
                                
                                End If
                            
                                'update the event to use the new path
                                If Left(l_strDestFile, 2) <> "\\" Then
                                    l_strSQL = "UPDATE events SET altlocation = '" & QuoteSanitise(Right(l_strDestFile, Len(l_strDestFile) - 3)) & "' WHERE eventID = '" & l_lngNewEventID & "';"
                                Else
                                    l_strSQL = "UPDATE events SET altlocation = '" & QuoteSanitise(Mid(l_strDestFile, Len(GetData("library", "subtitle", "libraryID", l_lngDestinationLibraryID)) + 2)) & "' WHERE eventID = '" & l_lngNewEventID & "';"
                                End If
                                c.Execute l_strSQL
                            End If
                            If l_lngEventID <> 0 Then
                                l_strSQL = "INSERT INTO eventusage (eventID, dateused) VALUES (" & l_lngEventID & ", getdate());"
                                c.Execute l_strSQL
                            End If
                            If l_lngNewEventID <> 0 Then
                                l_strSQL = "INSERT INTO eventusage (eventID, dateused) VALUES (" & l_lngNewEventID & ", getdate());"
                                c.Execute l_strSQL
                                'Record a History event
                                l_strSQL = "INSERT INTO eventhistory ("
                                l_strSQL = l_strSQL & "eventID, datesaved, username, description) VALUES ("
                                l_strSQL = l_strSQL & "'" & l_lngNewEventID & "', "
                                l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
                                l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
                                l_strSQL = l_strSQL & "'Created' "
                                l_strSQL = l_strSQL & ");"
                                c.Execute l_strSQL
                            End If
                        
                        Case 1 'Folder Move
                        
                            'If the item is a folder then some hairy algorithms are needed to update all files which fall on that folder tree
                            Set rs = New ADODB.Recordset
                            
                            If Left(l_strSourceFile, 2) <> "\\" Then
                                l_strSrcLocation = Right(l_strSourceFile, Len(l_strSourceFile) - 3)
                            Else
                                l_strSrcLocation = Mid(l_strSourceFile, Len(GetData("library", "subtitle", "libraryID", l_lngSourceLibraryID)) + 2)
                            End If
                            
                            If Left(l_strDestFile, 2) <> "\\" Then
                                l_strDestLocation = Right(l_strDestFile, Len(l_strDestFile) - 3)
                            Else
                                l_strDestLocation = Mid(l_strDestFile, Len(GetData("library", "subtitle", "libraryID", l_lngDestinationLibraryID)) + 2)
                            End If
                            
                            OldName = l_strSrcLocation
                            i = InStr(l_strSrcLocation, "\")
                            Do While i <> 0
                                OldName = Right(OldName, Len(OldName) - i)
                                i = InStr(OldName, "\")
                            Loop
                                                        
                            'libraryID = " & sID & " AND
                            l_strSQL = "SELECT eventID, altlocation, libraryID, online, clipfilename, hidefromweb, MediaWindowDeleteDate, AutoDeleteDate, system_deleted FROM events WHERE (altlocation = '" & QuoteSanitise(l_strSrcLocation) & "' OR altlocation LIKE '" & QuoteSanitise(l_strSrcLocation) & "\%') and libraryID = " & l_lngSourceLibraryID & " AND system_deleted = 0;"
                            rs.Open l_strSQL, c, adOpenDynamic, adLockOptimistic
                            If Not rs.EOF Then
                                rs.MoveFirst
                                Do While Not rs.EOF
                                    If GetData("library", "format", "libraryID", l_lngDestinationLibraryID) = "DISCSTORE" Then
                                        l_lngOnline = 1
                                    Else
                                        l_lngOnline = 0
                                    End If
                                    temp = Trim(" " & rs("altlocation"))
                                    If Len(temp) > Len(l_strSrcLocation) Then
                                        temp = Right(temp, Len(temp) - Len(l_strSrcLocation) - 1)
                                    Else
                                        temp = ""
                                    End If
                                    If l_strDestLocation <> "" Then
                                        NewLocation = l_strDestLocation & "\" & OldName
                                    Else
                                        NewLocation = OldName
                                    End If
                                    If temp <> "" Then
                                        NewLocation = NewLocation & "\" & temp
                                    End If
                                    l_lngOldEventID = GetEventIDForFileUsingLibraryID(l_lngDestinationLibraryID, NewLocation, rs("clipfilename"))
                                    If l_lngOldEventID <> 0 Then
                                        l_strSQL = "DELETE FROM portalpermission WHERE eventID = " & l_lngOldEventID & ";"
                                        c.Execute l_strSQL
                                        l_strSQL = "DELETE FROM distributionpermission WHERE eventID = " & l_lngOldEventID & ";"
                                        c.Execute l_strSQL
    '                                    l_strSQL = "DELETE FROM eventsegment WHERE eventID = " & l_lngOldEventID & ";"
    '                                    c.Execute l_strSQL
    '                                    l_strSQL = "DELETE FROM iTunes_Clip_Advisory WHERE eventID = " & l_lngOldEventID & ";"
    '                                    c.Execute l_strSQL
    '                                    l_strSQL = "DELETE FROM iTunes_product WHERE eventID = " & l_lngOldEventID & ";"
    '                                    c.Execute l_strSQL
                                        l_strSQL = "UPDATE events SET system_deleted = 1 WHERE eventID = " & l_lngOldEventID & ";"
                                        c.Execute l_strSQL
                                        'Record a History event
                                        l_strSQL = "INSERT INTO eventhistory ("
                                        l_strSQL = l_strSQL & "eventID, datesaved, username, description) VALUES ("
                                        l_strSQL = l_strSQL & "'" & l_lngOldEventID & "', "
                                        l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
                                        l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
                                        l_strSQL = l_strSQL & "'Clip Deleted' "
                                        l_strSQL = l_strSQL & ");"
                                        c.Execute l_strSQL
                                    End If
                                    rs("altlocation") = QuoteSanitise(NewLocation)
                                    If UCase(Left(NewLocation, 10)) = "AUTODELETE" Then
                                        rs("AutoDeleteDate") = Now
                                    End If
                                    rs("libraryID") = l_lngDestinationLibraryID
                                    rs("online") = l_lngOnline
                                    If l_lngDestinationLibraryID = 248095 Or l_lngDestinationLibraryID = 284349 Or l_lngDestinationLibraryID = 276356 Or l_lngDestinationLibraryID = 441212 Or l_lngDestinationLibraryID = 623206 Or l_lngDestinationLibraryID = 650950 Then
                                        rs("hidefromweb") = 0
                                        rs("MediaWindowDeleteDate") = Null
                                    End If
                                    If chkMoveThenForget.Value <> 0 Then
                                        rs("system_deleted") = 1
                                    End If
        
                                    rs.Update
                                    l_strSQL = "INSERT INTO eventusage (eventID, dateused) VALUES (" & rs("eventID") & ", getdate());"
                                    c.Execute l_strSQL
                                    'Record a History event
                                    l_strSQL = "INSERT INTO eventhistory ("
                                    l_strSQL = l_strSQL & "eventID, datesaved, username, description) VALUES ("
                                    l_strSQL = l_strSQL & "'" & rs("eventID") & "', "
                                    l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
                                    l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
                                    l_strSQL = l_strSQL & "'Moved' "
                                    l_strSQL = l_strSQL & ");"
                                    c.Execute l_strSQL
                                    rs.MoveNext
                                Loop
                            End If
                            rs.Close
                            Set rs = Nothing
                        
                            'Then check and move any individual folder records...
                            
                            l_lngOldEventID = LookupEventID(1, li, l_lngDestinationLibraryID)
                            If l_lngOldEventID <> 0 Then
                                l_strSQL = "DELETE FROM portalpermission WHERE eventID = " & l_lngOldEventID & ";"
                                c.Execute l_strSQL
                                l_strSQL = "DELETE FROM distributionpermission WHERE eventID = " & l_lngOldEventID & ";"
                                c.Execute l_strSQL
    '                            l_strSQL = "DELETE FROM eventsegment WHERE eventID = " & l_lngOldEventID & ";"
    '                            c.Execute l_strSQL
    '                            l_strSQL = "DELETE FROM iTunes_Clip_Advisory WHERE eventID = " & l_lngOldEventID & ";"
    '                            c.Execute l_strSQL
    '                            l_strSQL = "DELETE FROM iTunes_product WHERE eventID = " & l_lngOldEventID & ";"
    '                            c.Execute l_strSQL
                                l_strSQL = "UPDATE events SET system_deleted = 1 WHERE eventID = " & l_lngOldEventID & ";"
                                c.Execute l_strSQL
                                'Record a History event
                                l_strSQL = "INSERT INTO eventhistory ("
                                l_strSQL = l_strSQL & "eventID, datesaved, username, description) VALUES ("
                                l_strSQL = l_strSQL & "'" & l_lngOldEventID & "', "
                                l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
                                l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
                                l_strSQL = l_strSQL & "'Clip Deleted' "
                                l_strSQL = l_strSQL & ");"
                                c.Execute l_strSQL
                            End If
    
                            'check to see if the same drive is being used
                            'If there was a record for this source item...
                            If l_lngEventID <> 0 Then
                                If UCase(GetData("library", "subtitle", "libraryID", l_lngSourceLibraryID)) <> UCase(GetData("library", "subtitle", "libraryID", l_lngDestinationLibraryID)) Then
                                    
                                    'fire off the SQL
                                    If GetData("library", "format", "libraryID", l_lngDestinationLibraryID) = "DISCSTORE" Then
                                        l_lngOnline = 1
                                    Else
                                        l_lngOnline = 0
                                    End If
                                    If l_lngDestinationLibraryID = 248095 Or l_lngDestinationLibraryID = 284349 Or l_lngDestinationLibraryID = 276356 Or l_lngDestinationLibraryID = 441212 Or l_lngDestinationLibraryID = 623206 Or l_lngDestinationLibraryID = 650950 Then
                                        l_blnForceUnhide = True
                                    Else
                                        l_blnForceUnhide = False
                                    End If
                                    l_strSQL = "UPDATE events SET libraryID = '" & l_lngDestinationLibraryID & "', online = '" & l_lngOnline & "'"
                                    If l_blnForceUnhide = True Then
                                        l_strSQL = l_strSQL & ", hidefromweb = 0, MediaWindowDeleteDate = Null"
                                    End If
                                    l_strSQL = l_strSQL & " WHERE eventID = '" & l_lngEventID & "';"
                                    c.Execute l_strSQL
                                    'Record a History event
                                    l_strSQL = "INSERT INTO eventhistory ("
                                    l_strSQL = l_strSQL & "eventID, datesaved, username, description) VALUES ("
                                    l_strSQL = l_strSQL & "'" & l_lngEventID & "', "
                                    l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
                                    l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
                                    l_strSQL = l_strSQL & "'Moved' "
                                    l_strSQL = l_strSQL & ");"
                                    c.Execute l_strSQL
                                
                                End If
                        
                                'update the event to use the new path
                                If Left(l_strDestFile, 2) <> "\\" Then
                                    l_strSQL = "UPDATE events SET altlocation = '" & QuoteSanitise(Right(l_strDestFile, Len(l_strDestFile) - 3)) & "' WHERE eventID = '" & l_lngEventID & "';"
                                Else
                                    l_strSQL = "UPDATE events SET altlocation = '" & QuoteSanitise(Mid(l_strDestFile, Len(GetData("library", "subtitle", "libraryID", l_lngDestinationLibraryID)) + 2)) & "' WHERE eventID = '" & l_lngEventID & "';"
                                End If
                                c.Execute l_strSQL
                                l_strSQL = "INSERT INTO eventusage (eventID, dateused) VALUES (" & l_lngEventID & ", getdate());"
                                c.Execute l_strSQL
                            End If
                            
                        Case 2 'Folder Delete
                        
                            Set rs = New ADODB.Recordset
                            If Left(l_strSourceFile, 2) <> "\\" Then
                                l_strSrcLocation = Right(l_strSourceFile, Len(l_strSourceFile) - 3)
                            Else
                                l_strSrcLocation = Mid(l_strSourceFile, Len(GetData("library", "subtitle", "libraryID", l_lngSourceLibraryID)) + 2)
                            End If
    
                            l_strSQL = "SELECT eventID, libraryID, hidefromweb, online, system_deleted, mdate FROM events WHERE (altlocation = '" & QuoteSanitise(l_strSrcLocation) & "' OR altlocation LIKE '" & QuoteSanitise(l_strSrcLocation) & "\%') and libraryID = " & l_lngSourceLibraryID & ";"
                            rs.Open l_strSQL, c, adOpenDynamic, adLockOptimistic
                            If Not rs.EOF Then
                                rs.MoveFirst
                                Do While Not rs.EOF
                                    If rs("eventID") <> 0 Then
                                        l_strSQL = "DELETE FROM portalpermission WHERE eventID = '" & rs("eventID") & "';"
                                        c.Execute l_strSQL
                                        l_strSQL = "DELETE FROM distributionpermission WHERE eventID = " & rs("eventID") & ";"
                                        c.Execute l_strSQL
                                    End If
                                    rs("hidefromweb") = 1
                                    rs("system_deleted") = 1
                                    rs("mdate") = Now
                                    rs.Update
                                            
                                    'Record a History event
                                    l_strSQL = "INSERT INTO eventhistory ("
                                    l_strSQL = l_strSQL & "eventID, datesaved, username, description) VALUES ("
                                    l_strSQL = l_strSQL & "'" & rs("eventID") & "', "
                                    l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
                                    l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
                                    l_strSQL = l_strSQL & "'Clip Deleted' "
                                    l_strSQL = l_strSQL & ");"
                                    c.Execute l_strSQL
                                                                    
                                    rs.MoveNext
                                Loop
                            End If
                            rs.Close
                            Set rs = Nothing
                                            
                            'delete any individual foldr records
                            
                            'delete the record from the DB
                            If l_lngEventID <> 0 Then
                                l_strSQL = "DELETE FROM portalpermission WHERE eventID = '" & l_lngEventID & "';"
                                c.Execute l_strSQL
                                l_strSQL = "DELETE FROM distributionpermission WHERE eventID = '" & l_lngEventID & "';"
                                c.Execute l_strSQL
                                l_strSQL = "UPDATE events SET system_deleted = 1, hidefromweb = 1 WHERE eventID = '" & l_lngEventID & "';"
                                c.Execute l_strSQL
                            
                                'Record a History event
                                l_strSQL = "INSERT INTO eventhistory ("
                                l_strSQL = l_strSQL & "eventID, datesaved, username, description) VALUES ("
                                l_strSQL = l_strSQL & "'" & l_lngEventID & "', "
                                l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
                                l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
                                l_strSQL = l_strSQL & "'Clip Deleted' "
                                l_strSQL = l_strSQL & ");"
                                c.Execute l_strSQL
                                
                            End If
                    End Select
                    
                Else
                
                    'get the event ID of the item being moved
                    l_lngEventID = LookupEventID(0, li, l_lngSourceLibraryID)
                    
                    If l_lngEventID = 0 Then l_blnSkipRequest = True
                    
                    If l_blnSkipRequest = False Then
                    
                        'get the library ID of that item
                        l_lngSourceLibraryID = GetData("events", "libraryID", "eventID", l_lngEventID)
                                     
                        'work out the UNC drive (from the subtitle of the library item)
                        l_strSourceUNCDrive = GetData("library", "subtitle", "libraryID", l_lngSourceLibraryID)
                        
                        'CSBmk <Update the database>
                            
                        Select Case Index
                
                            Case 0 'copy the file
                                
                                l_lngOldEventID = LookupEventID(1, li, l_lngDestinationLibraryID)
                                l_lngNewEventID = CopyEventToLibraryID(l_lngEventID, l_lngDestinationLibraryID, chkDontBlankMasterfile.Value)
                                If l_lngOldEventID <> 0 Then
                                    l_strSQL = "DELETE FROM portalpermission WHERE eventID = " & l_lngOldEventID & ";"
                                    c.Execute l_strSQL
                                    l_strSQL = "DELETE FROM distributionpermission WHERE eventID = " & l_lngOldEventID & ";"
                                    c.Execute l_strSQL
                                    l_strSQL = "UPDATE events set system_deleted = 1 WHERE eventID = " & l_lngOldEventID & ";"
                                    c.Execute l_strSQL
                                    'Record a History event
                                    l_strSQL = "INSERT INTO eventhistory ("
                                    l_strSQL = l_strSQL & "eventID, datesaved, username, description) VALUES ("
                                    l_strSQL = l_strSQL & "'" & l_lngOldEventID & "', "
                                    l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
                                    l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
                                    l_strSQL = l_strSQL & "'Clip Deleted' "
                                    l_strSQL = l_strSQL & ");"
                                    c.Execute l_strSQL
                                End If
                                'check to see if the same drive is being used
                                If UCase(GetData("library", "subtitle", "libraryID", l_lngSourceLibraryID)) <> UCase(GetData("library", "subtitle", "libraryID", l_lngDestinationLibraryID)) Then
                                    
                                    'fire off the SQL
                                    If GetData("library", "format", "libraryID", l_lngDestinationLibraryID) = "DISCSTORE" Then
                                        l_lngOnline = 1
                                    Else
                                        l_lngOnline = 0
                                    End If
                                    l_strSQL = "UPDATE events SET libraryID = '" & l_lngDestinationLibraryID & "', online = '" & l_lngOnline & _
                                            "' WHERE eventID = '" & l_lngNewEventID & "';"
                                    c.Execute l_strSQL
                                
                                End If
                            
                                'update the event to use the new path
                                If Left(l_strDestFile, 2) <> "\\" Then
                                    l_strSQL = "UPDATE events SET altlocation = '" & QuoteSanitise(Right(l_strDestFile, Len(l_strDestFile) - 3)) & "' WHERE eventID = '" & l_lngNewEventID & "';"
                                    If UCase(Left(Right(l_strDestFile, Len(l_strDestFile) - 3), 10)) = "AUTODELETE" Then
                                        l_strSQL = l_strSQL & "UPDATE events SET AutoDeleteDate = '" & Format(Now, "YYYY-MM-DD HH:NN:SS") & "' WHERE eventID = '" & l_lngNewEventID & "';"
                                    End If
                                Else
                                    l_strSQL = "UPDATE events SET altlocation = '" & QuoteSanitise(Mid(l_strDestFile, Len(GetData("library", "subtitle", "libraryID", l_lngDestinationLibraryID)) + 2)) & "' WHERE eventID = '" & l_lngNewEventID & "';"
                                    If UCase(Left(Mid(l_strDestFile, Len(GetData("library", "subtitle", "libraryID", l_lngDestinationLibraryID)) + 2), 10)) = "AUTODELETE" Then
                                        l_strSQL = l_strSQL & "UPDATE events SET AutoDeleteDate = '" & Format(Now, "YYYY-MM-DD HH:NN:SS") & "' WHERE eventID = '" & l_lngNewEventID & "';"
                                    End If
                                End If
                                c.Execute l_strSQL
                                l_strSQL = "INSERT INTO eventusage (eventID, dateused) VALUES (" & l_lngEventID & ", getdate());"
                                c.Execute l_strSQL
                                l_strSQL = "INSERT INTO eventusage (eventID, dateused) VALUES (" & l_lngNewEventID & ", getdate());"
                                c.Execute l_strSQL
                                'Record a History event
                                l_strSQL = "INSERT INTO eventhistory ("
                                l_strSQL = l_strSQL & "eventID, datesaved, username, description) VALUES ("
                                l_strSQL = l_strSQL & "'" & l_lngNewEventID & "', "
                                l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
                                l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
                                l_strSQL = l_strSQL & "'Created' "
                                l_strSQL = l_strSQL & ");"
                                c.Execute l_strSQL
                
                            Case 1 'move the file
                                
                                l_lngOldEventID = LookupEventID(1, li, l_lngDestinationLibraryID)
                                If l_lngOldEventID <> 0 Then
                                    l_strSQL = "DELETE FROM portalpermission WHERE eventID = " & l_lngOldEventID & ";"
                                    c.Execute l_strSQL
                                    l_strSQL = "DELETE FROM distributionpermission WHERE eventID = " & l_lngOldEventID & ";"
                                    c.Execute l_strSQL
                                    l_strSQL = "UPDATE events SET system_deleted = 1 WHERE eventID = " & l_lngOldEventID & ";"
                                    c.Execute l_strSQL
                                    'Record a History event
                                    l_strSQL = "INSERT INTO eventhistory ("
                                    l_strSQL = l_strSQL & "eventID, datesaved, username, description) VALUES ("
                                    l_strSQL = l_strSQL & "'" & l_lngOldEventID & "', "
                                    l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
                                    l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
                                    l_strSQL = l_strSQL & "'Clip Deleted' "
                                    l_strSQL = l_strSQL & ");"
                                    c.Execute l_strSQL
                                End If
        
                                'check to see if the same drive is being used
                                If UCase(GetData("library", "subtitle", "libraryID", l_lngSourceLibraryID)) <> UCase(GetData("library", "subtitle", "libraryID", l_lngDestinationLibraryID)) Then
                                    
                                    'fire off the SQL
                                    If GetData("library", "format", "libraryID", l_lngDestinationLibraryID) = "DISCSTORE" Then
                                        l_lngOnline = 1
                                    Else
                                        l_lngOnline = 0
                                    End If
                                    If l_lngDestinationLibraryID = 248095 Or l_lngDestinationLibraryID = 284349 Or l_lngDestinationLibraryID = 276356 Or l_lngDestinationLibraryID = 441212 Or l_lngDestinationLibraryID = 623206 Or l_lngDestinationLibraryID = 650950 Then
                                        l_blnForceUnhide = True
                                    Else
                                        l_blnForceUnhide = False
                                    End If
                                    l_strSQL = "UPDATE events SET libraryID = '" & l_lngDestinationLibraryID & "', online = '" & l_lngOnline & "'"
                                    If l_blnForceUnhide = True Then
                                        l_strSQL = l_strSQL & ", hidefromweb = 0"
                                    End If
                                    l_strSQL = l_strSQL & " WHERE eventID = '" & l_lngEventID & "';"
                                    Debug.Print l_strSQL
                                    On Error GoTo REPEATFILEMOVE
                                    
REPEATFILEMOVE:
                                    Err.Clear
                                    c.Execute l_strSQL
                                    On Error GoTo cmdFileOp_Click_Err
                                    If chkMoveThenForget.Value <> 0 Then
                                        l_strSQL = "UPDATE events SET system_deleted = 1"
                                        l_strSQL = l_strSQL & " WHERE eventID = '" & l_lngEventID & "';"
                                        Debug.Print l_strSQL
                                        On Error Resume Next
                                        c.Execute l_strSQL
                                        On Error GoTo cmdFileOp_Click_Err
                                    End If
                                    
                                    'Record a History event
                                    l_strSQL = "INSERT INTO eventhistory ("
                                    l_strSQL = l_strSQL & "eventID, datesaved, username, description) VALUES ("
                                    l_strSQL = l_strSQL & "'" & l_lngEventID & "', "
                                    l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
                                    l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
                                    l_strSQL = l_strSQL & "'Moved' "
                                    l_strSQL = l_strSQL & ");"
                                    c.Execute l_strSQL
                                
                                End If
                            
                                'update the event to use the new path
                                If Left(l_strDestFile, 2) <> "\\" Then
                                    l_strSQL = "UPDATE events SET altlocation = '" & QuoteSanitise(Right(l_strDestFile, Len(l_strDestFile) - 3)) & "' WHERE eventID = '" & l_lngEventID & "';"
                                    If UCase(Left(Right(l_strDestFile, Len(l_strDestFile) - 3), 10)) = "AUTODELETE" Then
                                        l_strSQL = l_strSQL & "UPDATE events SET AutoDeleteDate = '" & Format(Now, "YYYY-MM-DD HH:NN:SS") & "' WHERE eventID = '" & l_lngEventID & "';"
                                    End If
                                Else
                                    l_strSQL = "UPDATE events SET altlocation = '" & QuoteSanitise(Mid(l_strDestFile, Len(GetData("library", "subtitle", "libraryID", l_lngDestinationLibraryID)) + 2)) & "' WHERE eventID = '" & l_lngEventID & "';"
                                    If UCase(Left(Mid(l_strDestFile, Len(GetData("library", "subtitle", "libraryID", l_lngDestinationLibraryID)) + 2), 10)) = "AUTODELETE" Then
                                        l_strSQL = l_strSQL & "UPDATE events SET AutoDeleteDate = '" & Format(Now, "YYYY-MM-DD HH:NN:SS") & "' WHERE eventID = '" & l_lngEventID & "';"
                                    End If
                                End If
                                Debug.Print l_strSQL
                                c.Execute l_strSQL
                                l_strSQL = "INSERT INTO eventusage (eventID, dateused) VALUES (" & l_lngEventID & ", getdate());"
                                c.Execute l_strSQL
                                
                            Case 2 'delete the file
                                
                                'delete the record from the DB
                                If l_lngEventID <> 0 Then
                                    l_strSQL = "DELETE FROM portalpermission WHERE eventID = '" & l_lngEventID & "';"
                                    c.Execute l_strSQL
                                    l_strSQL = "DELETE FROM distributionpermission WHERE eventID = '" & l_lngEventID & "';"
                                    c.Execute l_strSQL
                                    l_strSQL = "UPDATE events SET system_deleted = 1, hidefromweb = 1, mdate = getdate() WHERE eventID = '" & l_lngEventID & "';"
                                    Debug.Print
                                    On Error GoTo REPEATFILEDELETE
REPEATFILEDELETE:
                                    Err.Clear
                                    c.Execute l_strSQL
                                    On Error GoTo cmdFileOp_Click_Err
                                    'Record a History event
                                    l_strSQL = "INSERT INTO eventhistory ("
                                    l_strSQL = l_strSQL & "eventID, datesaved, username, description) VALUES ("
                                    l_strSQL = l_strSQL & "'" & l_lngEventID & "', "
                                    l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
                                    l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
                                    l_strSQL = l_strSQL & "'Clip Deleted' "
                                    l_strSQL = l_strSQL & ");"
                                    c.Execute l_strSQL
                                    
                                End If
                            
                        End Select
            
                    End If
                End If
                    
                'close the database connection
                c.Close
                Set c = Nothing
        
            Else
                
                l_blnMakeFileRequest = True
                On Error GoTo Database_Unavailable
                Set c = New ADODB.Connection
                c.Open g_strConnection
                On Error GoTo cmdFileOp_Click_Err
                
                'Do an offline request for the activity
                l_lngEventID = LookupEventID(0, li, l_lngSourceLibraryID)
                
                If l_lngEventID = 0 And (lFileOp = FO_COPY Or lFileOp = FO_MOVE Or lFileOp = FO_DELETE) Then
                    If li.IsFolder Then
                        MsgBox "There is no CETA record for " & li.DisplayName & vbCrLf & "Use the appropriate Folder operation buttons instead."
                    Else
                        MsgBox "There is no CETA record for " & li.DisplayName & vbCrLf & "Cannot Process this item."
                    End If
                    l_blnMakeFileRequest = False
                ElseIf l_lngEventID = 0 Then
                    If li.IsFolder Then
                        MsgBox "Individual file requests will be made for all items within the folder that have CETA records. All other items within the folder (including the folder itself) will be left unchanged"
                    Else
                        MsgBox "Item is not a folder, so a Folder Operations cannot be performed on it."
                        l_blnMakeFileRequest = False
                    End If
                End If
                
                If l_blnMakeFileRequest = True Then
                    If Left(l_strDestFile, 2) <> "\\" Then
                        l_strNewFolder = Right(l_strDestFile, Len(l_strDestFile) - 3)
                    Else
                        l_strNewFolder = Mid(l_strDestFile, Len(l_strDestinationUNCDrive) + 2)
                    End If
                    
                    Select Case lFileOp
                        Case FO_COPY
                            l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, NewAltLocation, PreserveMasterfileStatus, bigfilesize, RequestName, RequesterEmail) VALUES ("
                            l_strSQL = l_strSQL & l_lngEventID & ", "
                            l_strSQL = l_strSQL & l_lngSourceLibraryID & ", "
                            l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Copy") & ", "
                            l_strSQL = l_strSQL & l_lngDestinationLibraryID & ", "
                            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strNewFolder) & "', "
                            l_strSQL = l_strSQL & "1, "
                            l_strSQL = l_strSQL & GetData("events", "bigfilesize", "eventID", l_lngEventID) & ", "
                            l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                            Debug.Print l_strSQL
                            c.Execute l_strSQL
                        Case FO_MOVE
                            l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, NewAltLocation, bigfilesize, RequestName, RequesterEmail) VALUES ("
                            l_strSQL = l_strSQL & l_lngEventID & ", "
                            l_strSQL = l_strSQL & l_lngSourceLibraryID & ", "
                            l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move") & ", "
                            l_strSQL = l_strSQL & l_lngDestinationLibraryID & ", "
                            l_strSQL = l_strSQL & "'" & QuoteSanitise(l_strNewFolder) & "', "
                            l_strSQL = l_strSQL & GetData("events", "bigfilesize", "eventID", l_lngEventID) & ", "
                            l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                            Debug.Print l_strSQL
                            c.Execute l_strSQL
                        Case FO_DELETE
                            l_strSQL = "INSERT INTO event_file_request (eventID, sourcelibraryID, event_file_Request_typeID, NewLibraryID, RequestName, RequesterEmail) VALUES ("
                            l_strSQL = l_strSQL & l_lngEventID & ", "
                            l_strSQL = l_strSQL & l_lngSourceLibraryID & ", "
                            l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Delete") & ", "
                            l_strSQL = l_strSQL & l_lngSourceLibraryID & ", "
                            l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                            Debug.Print l_strSQL
                            c.Execute l_strSQL
                            
                        Case FO_FOLDERCOPY
                            If Left(l_strSourceFile, 2) <> "\\" Then
                                l_strSrcLocation = Right(l_strSourceFile, Len(l_strSourceFile) - 3)
                            Else
                                l_strSrcLocation = Mid(l_strSourceFile, Len(l_strSourceUNCDrive) + 2)
                            End If
                            l_strSQL = "SELECT eventID, altlocation, bigfilesize FROM events WHERE libraryID = " & l_lngSourceLibraryID & " AND (altlocation = '" & l_strSrcLocation & "\" & li.DisplayName & "' OR altlocation LIKE '" & l_strSrcLocation & "\" & li.DisplayName & "\%') "
                            l_strSQL = l_strSQL & "AND bigfilesize IS NOT Null AND system_deleted = 0;"
                            Set rs = New ADODB.Recordset
                            rs.Open l_strSQL, c, 3, 3
                            If rs.RecordCount > 0 Then
                                Do While Not rs.EOF
                                    l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, NewAltLocation, PreserveMasterfileStatus, bigfilesize, RequestName, RequesterEmail) VALUES ("
                                    l_strSQL = l_strSQL & rs("eventID") & ", "
                                    l_strSQL = l_strSQL & l_lngSourceLibraryID & ", "
                                    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Copy") & ", "
                                    l_strSQL = l_strSQL & l_lngDestinationLibraryID & ", "
                                    l_strSQL = l_strSQL & "'" & QuoteSanitise(Replace(rs("altlocation"), l_strSrcLocation, l_strNewFolder)) & "', "
                                    l_strSQL = l_strSQL & "1, "
                                    l_strSQL = l_strSQL & GetData("events", "bigfilesize", "eventID", rs("eventID")) & ", "
                                    l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                                    Debug.Print l_strSQL
                                    c.Execute l_strSQL
                                    rs.MoveNext
                                Loop
                            End If
                            rs.Close
                            Set rs = Nothing
                        
                        Case FO_FOLDERMOVE
                            If Left(l_strSourceFile, 2) <> "\\" Then
                                l_strSrcLocation = Right(l_strSourceFile, Len(l_strSourceFile) - 3)
                            Else
                                l_strSrcLocation = Mid(l_strSourceFile, Len(l_strSourceUNCDrive) + 2)
                            End If
                            l_strSQL = "SELECT eventID, altlocation, bigfilesize FROM events WHERE libraryID = " & l_lngSourceLibraryID & " AND (altlocation = '" & l_strSrcLocation & "\" & li.DisplayName & "' OR altlocation LIKE '" & l_strSrcLocation & "\" & li.DisplayName & "\%')  "
                            l_strSQL = l_strSQL & "AND bigfilesize IS NOT Null AND system_deleted = 0;"
                            Debug.Print l_strSQL
                            Set rs = New ADODB.Recordset
                            rs.Open l_strSQL, c, 3, 3
                            If rs.RecordCount > 0 Then
                                Do While Not rs.EOF
                                    l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, NewLibraryID, NewAltLocation, PreserveMasterfileStatus, bigfilesize, RequestName, RequesterEmail) VALUES ("
                                    l_strSQL = l_strSQL & rs("eventID") & ", "
                                    l_strSQL = l_strSQL & l_lngSourceLibraryID & ", "
                                    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Move") & ", "
                                    l_strSQL = l_strSQL & l_lngDestinationLibraryID & ", "
                                    l_strSQL = l_strSQL & "'" & QuoteSanitise(Replace(rs("altlocation"), l_strSrcLocation, l_strNewFolder)) & "', "
                                    l_strSQL = l_strSQL & "1, "
                                    l_strSQL = l_strSQL & GetData("events", "bigfilesize", "eventID", rs("eventID")) & ", "
                                    l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                                    Debug.Print l_strSQL
                                    c.Execute l_strSQL
                                    rs.MoveNext
                                Loop
                            End If
                            rs.Close
                            Set rs = Nothing
                        
                        Case FO_FOLDERDELETE
                            If Left(l_strSourceFile, 2) <> "\\" Then
                                l_strSrcLocation = Right(l_strSourceFile, Len(l_strSourceFile) - 3)
                            Else
                                l_strSrcLocation = Mid(l_strSourceFile, Len(l_strSourceUNCDrive) + 2)
                            End If
                            l_strSQL = "SELECT eventID, altlocation, bigfilesize FROM events WHERE libraryID = " & l_lngSourceLibraryID & " AND (altlocation = '" & l_strSrcLocation & "\" & li.DisplayName & "' OR altlocation LIKE '" & l_strSrcLocation & "\" & li.DisplayName & "\%') "
                            l_strSQL = l_strSQL & "AND bigfilesize IS NOT Null AND system_deleted = 0;"
                            Debug.Print l_strSQL
                            Set rs = New ADODB.Recordset
                            rs.Open l_strSQL, c, 3, 3
                            If rs.RecordCount > 0 Then
                                Do While Not rs.EOF
                                    l_strSQL = "INSERT INTO event_file_request (eventID, sourcelibraryID, event_file_Request_typeID, NewLibraryID, RequestName, RequesterEmail) VALUES ("
                                    l_strSQL = l_strSQL & rs("eventID") & ", "
                                    l_strSQL = l_strSQL & l_lngSourceLibraryID & ", "
                                    l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "Delete") & ", "
                                    l_strSQL = l_strSQL & l_lngSourceLibraryID & ", "
                                    l_strSQL = l_strSQL & "'" & g_strUserName & "', '" & g_strUserEmailAddress & "');"
                                    Debug.Print l_strSQL
                                    c.Execute l_strSQL
                                    rs.MoveNext
                                Loop
                            End If
                            rs.Close
                            Set rs = Nothing
                        
                    End Select
                End If
                c.Close
                Set c = Nothing
            End If
        
            Set li = shFiles(0).NextSelectedItem

        Loop
        lblCatalogprogress.Caption = ""
        DoEvents
        Screen.MousePointer = vbDefault
        ShellFileOperation.Flags = ShellFileOperation.Flags And Not SFONoConfirmation

        shFiles(0).RefreshView
        shFiles(1).RefreshView

        g_FileOpInProgress = False
        ShFolders(0).Enabled = True
        ShDrives(0).Enabled = True
        shFiles(0).Enabled = True
        ShFolders(1).Enabled = True
        ShDrives(1).Enabled = True
        shFiles(1).Enabled = True
        Exit Sub

Database_Unavailable:
        MsgBox "Database is unavailable to log the last operation undertaken on: " & l_strSourceFile & vbCrLf & "You will need to manually adjust this record in CETA.", vbCritical, "Error - Operation aborted"
        ShFolders(0).Enabled = True
        ShDrives(0).Enabled = True
        shFiles(0).Enabled = True
        ShFolders(1).Enabled = True
        ShDrives(1).Enabled = True
        shFiles(1).Enabled = True
        Exit Sub

cmdFileOp_Click_Err:
        MsgBox Err.Description & vbCrLf & _
               "in prjJCAFileManager.frmCETAFileManager.cmdFileOp_Click " & _
               "at line " & Erl, _
               vbExclamation + vbOKOnly, "Application Error"
        Resume Next

End Sub

Private Sub cmdGoTo_Click(Index As Integer)
        
        On Error GoTo cmdGoTo_Click_Err

        Dim l_strUNCPath As String, l_intControl As Integer

        'moves the file browsing controls to a default folder

        If Index < 24 Then
            l_intControl = 0
        Else
            l_intControl = 1
        End If
        
        If cmdGoTo(Index).Caption = "Desktop" Then
            shFiles(l_intControl).ResetToDesktop
        Else
            If CheckLocal10Gig = True Then
                l_strUNCPath = GetData("library", "foreignref", "barcode", cmdGoTo(Index).Caption)
                If l_strUNCPath = "" Then
                    l_strUNCPath = GetData("library", "subtitle", "barcode", cmdGoTo(Index).Caption)
                End If
            Else
                l_strUNCPath = GetData("library", "subtitle", "barcode", cmdGoTo(Index).Caption)
            End If
            
            shFiles(l_intControl).CurrentFolder = l_strUNCPath
        End If
        
        '<EhFooter>
        Exit Sub

cmdGoTo_Click_Err:
        MsgBox Err.Description & vbCrLf & _
               "in prjJCAFileManager.frmCETAFileManager.cmdGoTo_Click " & _
               "at line " & Erl, _
               vbExclamation + vbOKOnly, "Application Error"
        Resume Next
        '</EhFooter>
End Sub

Private Sub cmdOptionsForIndependents_Click()

chkMediaInfo.Value = 1
chkMD5.Value = 0
optSetAsMasterfile(0).Value = True
chkExcludeSubfolders.Value = 0
chkRebuildReferences.Value = 1
chkSkipVerify.Value = 0
cmbPurpose.Text = "Original Masterfile"
chkYesToAll.Value = 0
chkRename.Value = 0
chkLogAndXML.Value = 0
chkImages.Value = 0
chkAllowNoPurpose.Value = 0
chkAllowNoJobID.Value = 1
chkDontBlankMasterfile.Value = 0
chkMediaInfoTimecodes.Value = 1
chkSupressFilenameValidationWarnings.Value = 1

End Sub

Private Sub cmdOptionsNormal_Click()

chkMediaInfo.Value = 0
chkMD5.Value = 0
optSetAsMasterfile(2).Value = True
chkExcludeSubfolders.Value = 0
chkRebuildReferences.Value = 1
chkSkipVerify.Value = 0
cmbPurpose.Text = ""
chkYesToAll.Value = 0
chkRename.Value = 0
chkLogAndXML.Value = 0
chkImages.Value = 0
chkAllowNoPurpose.Value = 0
chkAllowNoJobID.Value = 0
chkDontBlankMasterfile.Value = 0
chkMediaInfoTimecodes.Value = 0
chkSupressFilenameValidationWarnings.Value = 0
chkNoExtenders.Value = 0

End Sub

Private Sub cmdParentFolder_Click(Index As Integer)

shFiles(Index).CurrentFolder = Left(shFiles(Index).CurrentFolder, Len(shFiles(Index).CurrentFolder) - Len(shFiles(Index).CurrentFolderDisplayName) - 1)

End Sub

Private Sub cmdRefresh_Click(Index As Integer)
        '<EhHeader>
        On Error GoTo cmdRefresh_Click_Err
        '</EhHeader>

'        ShDrives(Index).RefreshComboBox
        shFiles(Index).RefreshView
        ShFolders(Index).RefreshView

        '<EhFooter>
        Exit Sub

cmdRefresh_Click_Err:
        MsgBox Err.Description & vbCrLf & _
               "in prjJCAFileManager.frmCETAFileManager.cmdRefresh_Click " & _
               "at line " & Erl, _
               vbExclamation + vbOKOnly, "Application Error"
        Resume Next
        '</EhFooter>
End Sub

Private Sub cmdStopCatalog_Click()

chkStopCatalog.Value = 1

End Sub

Private Sub cmdSwapPaths_Click()
        '<EhHeader>
        On Error GoTo cmdSwapPaths_Click_Err
        '</EhHeader>

        Dim l_strPath0 As String

        Dim l_strPath1 As String

        l_strPath0 = shFiles(0).CurrentFolder
        l_strPath1 = shFiles(1).CurrentFolder

        shFiles(0).CurrentFolder = l_strPath1
        shFiles(1).CurrentFolder = l_strPath0

        '<EhFooter>
        Exit Sub

cmdSwapPaths_Click_Err:
        MsgBox Err.Description & vbCrLf & _
               "in prjJCAFileManager.frmCETAFileManager.cmdSwapPaths_Click " & _
               "at line " & Erl, _
               vbExclamation + vbOKOnly, "Application Error"
        Resume Next
        '</EhFooter>
End Sub

Private Sub Form_Load()
        
        Dim temp As Variant, l_strWorkstationprops As String
        '<EhHeader>
        On Error GoTo Form_Load_Err
        '</EhHeader>

        If WindowState <> vbMinimized Then
            Me.height = Screen.height - 540
            Me.Top = 60
        End If

        ShDrives(0).FolderView = ShFolders(0)
        ShFolders(0).FileView = shFiles(0)
        shFiles(0).DragDropSettings = FIVNoDragDrop

        ShDrives(1).FolderView = ShFolders(1)
        ShFolders(1).FileView = shFiles(1)
        shFiles(1).DragDropSettings = FIVNoDragDrop

    'column adjustments
        
        
        shFiles(0).SetColumnWidth "", 0, 200
        shFiles(1).SetColumnWidth "", 0, 200
        shFiles(0).SetColumnWidth "", 3, 100
        shFiles(1).SetColumnWidth "", 3, 100
        
        g_FileOpInProgress = False
        g_intMinutesToCloseDown = 2

        Dim l_strCommandLine As String
        l_strCommandLine = Command()
        
        If InStr(1, l_strCommandLine, "/testTim", vbTextCompare) <> 0 Then
            g_strConnection = "DRIVER={ODBC Driver 13 for SQL Server};SERVER=lon-a820-tim.office.refine-group.com;DATABASE=cetasoft;UID=Web_User;PWD=Web_User_Password1903"
            frmCetaFileManager.Caption = frmCetaFileManager.Caption & " - using Test-Tim Database"
        ElseIf InStr(1, l_strCommandLine, "/bsdev", vbTextCompare) <> 0 Then
            g_strConnection = "DRIVER={ODBC Driver 13 for SQL Server};SERVER=lon-bsdevsql-01.office.refine-group.com;DATABASE=cetasoft;UID=Web_User;PWD=Web_User_Password1903"
            frmCetaFileManager.Caption = frmCetaFileManager.Caption & " - using Test-Tim Database"
        Else
            g_strConnection = "DRIVER={ODBC Driver 13 for SQL Server};SERVER=lon-bssql-01.office.refine-group.com;DATABASE=cetasoft;UID=Web_User;PWD=Web_User_Password1903"
'            g_strConnection = "DRIVER=SQL Server;SERVER=vie-sql-1.jcatv.co.uk;DATABASE=cetasoft;UID=Web_User;PWD=Web_User_Password1903"
        End If
        
        'Check is on a red network machine and quite if not.
        l_strWorkstationprops = GetData("xref", "descriptionalias", "description", CurrentMachineName)
        If InStr(l_strWorkstationprops, "/rednetwork") > 0 Then
            'Do nothing
        Else
            MsgBox "Ceta File Manager cannot run unless you are on a Red Network computer.", vbCritical, "Cannot Run"
            End
        End If
        
        Dim l_strSQL As String
        l_strSQL = "SELECT name, accountcode, companyID FROM company WHERE (iscustomer = 1 OR isprospective = 1) ORDER BY name;"
        
        Dim l_conSearch As ADODB.Connection
        Dim l_rstSearch1 As ADODB.Recordset
        
        Set l_conSearch = New ADODB.Connection
        Set l_rstSearch1 = New ADODB.Recordset
        
        l_conSearch.ConnectionString = g_strConnection
        l_conSearch.Open
        
        With l_rstSearch1
             .CursorLocation = adUseClient
             .LockType = adLockBatchOptimistic
             .CursorType = adOpenDynamic
             .Open l_strSQL, l_conSearch, adOpenDynamic
        End With
        
        l_rstSearch1.ActiveConnection = Nothing
        
        Set cmbCompany.DataSourceList = l_rstSearch1
        
        l_conSearch.Close
        Set l_conSearch = Nothing

        '<EhFooter>
        GetLoginDetails
        
        Dim ButtonCounter As Integer
        For ButtonCounter = 0 To 47
            If GetFlag(GetData("setting", "value", "name", "CFMButtonVisible" & ButtonCounter)) <> 0 Then
                cmdGoTo(ButtonCounter).Visible = True
                cmdGoTo(ButtonCounter).Caption = GetData("setting", "value", "name", "CFMButtonText" & ButtonCounter)
                If GetFlag(GetData("setting", "value", "name", "CFMButtonIT" & ButtonCounter)) <> 0 And Not CheckAccess("/SHOWITRAID", True) Then
                    cmdGoTo(ButtonCounter).Visible = False
                End If
            Else
                cmdGoTo(ButtonCounter).Visible = False
            End If
        Next
        Exit Sub

Form_Load_Err:
        MsgBox Err.Description & vbCrLf & _
               "in prjJCAFileManager.frmCETAFileManager.Form_Load " & _
               "at line " & Erl, _
               vbExclamation + vbOKOnly, "Application Error"
        Resume Next
        '</EhFooter>
End Sub

Private Sub Form_Resize()

Dim l_lngHalfway As Long, l_lngBottomLine As Long, l_lngWidth As Long

On Error Resume Next

If Me.ScaleHeight >= 11520 Then
    l_lngHalfway = (Me.ScaleHeight - fraOptions.height - 600) / 2
Else
    l_lngHalfway = (11520 - fraOptions.height - 600) / 2
End If

If l_lngHalfway > 0 Then
    ShFolders(0).height = l_lngHalfway - ShFolders(0).Top - 120
    shFiles(0).height = l_lngHalfway - shFiles(0).Top - 120
    cmdSwapPaths.Top = l_lngHalfway - 150
    ShDrives(1).Top = l_lngHalfway - 60
    cmdParentFolder(1).Top = l_lngHalfway - 60
    cmdRefresh(1).Top = ShDrives(1).Top
    shFiles(1).Top = ShDrives(1).Top + ShDrives(1).height + 120
    ShFolders(1).Top = shFiles(1).Top
    shFiles(1).height = shFiles(0).height
    ShFolders(1).height = ShFolders(0).height
    fraOptions.Top = shFiles(1).Top + shFiles(1).height + 60
    cmdEnable.Top = fraOptions.Top + 90
    cmdStopCatalog.Top = fraOptions.Top + 90
    lblCatalogprogress.Top = fraOptions.Top + fraOptions.height + 60
'    l_lngBottomLine = shFiles(1).Top + shFiles(1).height
    l_lngBottomLine = shFiles(1).Top
    cmdGoTo(24).Top = l_lngBottomLine
    cmdGoTo(25).Top = l_lngBottomLine
    cmdGoTo(26).Top = l_lngBottomLine + 360
    cmdGoTo(27).Top = l_lngBottomLine + 360
    cmdGoTo(28).Top = l_lngBottomLine + 780
    cmdGoTo(29).Top = l_lngBottomLine + 780
    cmdGoTo(30).Top = l_lngBottomLine + 1200
    cmdGoTo(31).Top = l_lngBottomLine + 1200
    cmdGoTo(32).Top = l_lngBottomLine + 1620
    cmdGoTo(33).Top = l_lngBottomLine + 1620
    cmdGoTo(34).Top = l_lngBottomLine + 2040
    cmdGoTo(35).Top = l_lngBottomLine + 2040
    cmdGoTo(36).Top = l_lngBottomLine + 2460
    cmdGoTo(37).Top = l_lngBottomLine + 2460
    cmdGoTo(38).Top = l_lngBottomLine + 2880
    cmdGoTo(39).Top = l_lngBottomLine + 2880
    cmdGoTo(40).Top = l_lngBottomLine + 3300
    cmdGoTo(41).Top = l_lngBottomLine + 3300
    cmdGoTo(42).Top = l_lngBottomLine + 3720
    cmdGoTo(43).Top = l_lngBottomLine + 3720
    cmdGoTo(44).Top = l_lngBottomLine + 4140
    cmdGoTo(45).Top = l_lngBottomLine + 4140
    cmdGoTo(46).Top = l_lngBottomLine + 4560
    cmdGoTo(47).Top = l_lngBottomLine + 4560
End If


l_lngWidth = Me.ScaleWidth - ShFolders(0).Left - ShFolders(0).width - 60 - fraButtons.width
shFiles(0).width = l_lngWidth
shFiles(1).width = l_lngWidth
fraButtons.Left = shFiles(0).Left + shFiles(0).width + 60
lblCatalogprogress.width = fraOptions.width + cmdEnable.width

l_lngWidth = ShFolders(0).width + shFiles(0).width
ShDrives(0).width = l_lngWidth - cmdRefresh(0).width - cmdParentFolder(0).width - 120
cmdParentFolder(0).Left = ShDrives(0).Left + ShDrives(0).width + 60
cmdParentFolder(1).Left = cmdParentFolder(0).Left
cmdRefresh(0).Left = ShDrives(0).Left + ShDrives(0).width + cmdParentFolder(0).width + 120
ShDrives(1).width = ShDrives(0).width
cmdRefresh(1).Left = cmdRefresh(0).Left

On Error GoTo 0

End Sub

Private Sub shFiles_OnBeginItemRename(Index As Integer, ByVal Item As FileViewControl.IListItem, Cancel As Boolean)

If IsCETALocked = True Then
    MsgBox "Ceta is locked - cannot rename file at this time.", vbCritical, "CETA Locked"
    Cancel = True
    Exit Sub
End If

'If InStr(Item.Path, ShDrives(Index).EditBoxText) <= 0 Then
'    MsgBox "Please endure the drive dropdown is displaying the current folder location before proceeding.", vbCritical, "Cannot Proceed"
'    Cancel = True
'    Exit Sub
'End If

If g_LoginCancelled = True Then
    MsgBox "You must provide CETA login credentials to undertake this file operation.", vbCritical, "Error"
    Cancel = True
    Exit Sub
End If

g_FileOpInProgress = True

End Sub

Private Sub shFiles_OnEndItemRename(Index As Integer, ByVal Item As FileViewControl.IListItem, NewName As String, Cancel As Boolean)

    '<EhHeader>
    On Error GoTo shFiles_OnEndItemRename_Err
    '</EhHeader>
    
    If Cancel = True Then
        g_FileOpInProgress = False
        Exit Sub
    End If
    Dim l_lngEventID As Long
    Dim c As ADODB.Connection, rs As ADODB.Recordset
    Dim l_strSQL As String
    Dim UNCPath As String, sID As String, l_strFolderPath As String, l_strFileToOpen As String, l_strDrivePath As String, OldName As String, i As Long, EditText As String
    Dim temp As String, temp2 As String, l_strRootFolderPath As String, NewReference As String, Count As Long
    Dim FSO As Scripting.FileSystemObject
    
    Set FSO = New Scripting.FileSystemObject

    If Item.IsFolder Then
        'If the item is a folder then some hairy algorithms are needed to update all files which fall on that folder tree
        Set rs = New ADODB.Recordset
        OldName = Item.DisplayName
        If Left(Item.Path, 2) <> "\\" Then
            l_strFolderPath = Right(Item.Path, Len(Item.Path) - 3)
            l_strDrivePath = Left(Item.Path, 2)
        Else
            shFiles(2).CurrentFolder = Item.Path
            l_strFileToOpen = Item.Path
            
            Do While Not FSO.FileExists(l_strFileToOpen & "\libraryID.sys") And Not FSO.FileExists(l_strFileToOpen & "\.libraryID.sys")
                l_strFileToOpen = Left(shFiles(2).CurrentFolder, Len(shFiles(2).CurrentFolder) - Len(shFiles(2).CurrentFolderDisplayName) - 1)
                shFiles(2).CurrentFolder = l_strFileToOpen
            Loop
            
            l_strDrivePath = l_strFileToOpen
            l_strFolderPath = Mid(Item.Path, Len(l_strFileToOpen) + 2)
        End If
        
        If Left(l_strDrivePath, 1) <> "\" Then
            l_strFileToOpen = Left$(shFiles(Index).CurrentFolder, 2)
        Else
            l_strFileToOpen = l_strDrivePath
        End If
        
        If FSO.FileExists(l_strFileToOpen & "\libraryID.sys") Then
            Open l_strFileToOpen & "\libraryID.sys" For Input As 1
            Line Input #1, sID
            Close 1
        ElseIf FSO.FileExists(l_strFileToOpen & "\.libraryID.sys") Then
            Open l_strFileToOpen & "\.libraryID.sys" For Input As 1
            Line Input #1, sID
            Close 1
        End If
        
        If sID = 0 Then
            g_FileOpInProgress = False
            Exit Sub
        End If
        
        Set c = New ADODB.Connection
        c.Open g_strConnection
        'libraryID = " & sID & " AND
        
        'get the root part of the location - i.e. where the current folder is.
        If Left(ShDrives(Index).EditBoxText, 2) <> "\\" Then
            l_strRootFolderPath = Right(ShDrives(Index).EditBoxText, Len(ShDrives(Index).EditBoxText) - 3)
        Else
            shFiles(2).CurrentFolder = Item.Path
            l_strFileToOpen = Item.Path
            
            Do While Not FSO.FileExists(l_strFileToOpen & "\libraryID.sys") And Not FSO.FileExists(l_strFileToOpen & "\.libraryID.sys")
                l_strFileToOpen = Left(shFiles(2).CurrentFolder, Len(shFiles(2).CurrentFolder) - Len(shFiles(2).CurrentFolderDisplayName) - 1)
                shFiles(2).CurrentFolder = l_strFileToOpen
            Loop
            
            l_strDrivePath = l_strFileToOpen
            l_strRootFolderPath = Mid(Item.Path, Len(l_strFileToOpen) + 2)
            
        End If
        
        'Then check for items that are in that path
        l_strSQL = "SELECT eventID, altlocation FROM events WHERE (altlocation LIKE '" & QuoteSanitise(l_strFolderPath) & "\" & "%') AND libraryID = " & sID & " and system_deleted = 0;"
        EditText = ""
        rs.Open l_strSQL, c, 3, 3
        If Not rs.EOF Then
            Debug.Print rs.RecordCount
            rs.MoveFirst
            Do While Not rs.EOF
                temp = Mid(rs("altlocation"), Len(l_strRootFolderPath) + 1)
                If InStr(rs("altlocation"), OldName) > 1 Then
                    EditText = Left(rs("altlocation"), Len(rs("altlocation")) - Len(temp) - Len(OldName))
                End If
                EditText = EditText & NewName & temp
                If EditText <> rs("altlocation") Then
                    rs("altlocation") = EditText
                    rs.Update
                    
                    'Record a History event
                    l_strSQL = "INSERT INTO eventhistory ("
                    l_strSQL = l_strSQL & "eventID, datesaved, username, description) VALUES ("
                    l_strSQL = l_strSQL & "'" & rs("eventID") & "', "
                    l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
                    l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
                    l_strSQL = l_strSQL & "'Folder Renamed' "
                    l_strSQL = l_strSQL & ");"
                    
                    c.Execute l_strSQL
                End If
                rs.MoveNext
            Loop
        End If
        rs.Close
        l_strSQL = "SELECT eventID, altlocation FROM events WHERE (altlocation LIKE '" & QuoteSanitise(l_strFolderPath) & "') AND libraryID = " & sID & " and system_deleted = 0;"
        rs.Open l_strSQL, c, 3, 3
        If Not rs.EOF Then
            Debug.Print rs.RecordCount
            rs.MoveFirst
            Do While Not rs.EOF
                EditText = Left(rs("altlocation"), Len(rs("altlocation")) - Len(OldName)) & NewName
                If EditText <> rs("altlocation") Then
                    rs("altlocation") = EditText
                    rs.Update
                    
                    'Record a History event
                    l_strSQL = "INSERT INTO eventhistory ("
                    l_strSQL = l_strSQL & "eventID, datesaved, username, description) VALUES ("
                    l_strSQL = l_strSQL & "'" & rs("eventID") & "', "
                    l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
                    l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
                    l_strSQL = l_strSQL & "'Folder Renamed' "
                    l_strSQL = l_strSQL & ");"
                    
                    c.Execute l_strSQL
                End If
                rs.MoveNext
            Loop
        End If
        rs.Close
        c.Close
        Set rs = Nothing
        Set c = Nothing
    Else
        'If item isn't a folder then the individual record can just be updated.
        l_lngEventID = LookupEventID(Index, Item)
    
        If l_lngEventID = 0 Then
            g_FileOpInProgress = False
            Exit Sub
        End If
        Count = 0
        Do While InStr(Mid(NewName, Count + 1), ".") > 0
            Count = Count + InStr(Mid(NewName, Count + 1), ".")
        Loop
        If Count > 0 Then
            NewReference = Left(NewName, Count - 1)
        Else
            NewReference = NewName
        End If
    
        l_strSQL = "UPDATE events SET clipfilename = '" & QuoteSanitise(NewName) & "'"
        If chkRebuildReferences.Value <> 0 Then
            l_strSQL = l_strSQL & ", clipreference = '" & QuoteSanitise(NewReference) & "'"
        End If
        l_strSQL = l_strSQL & " WHERE eventID = '" & l_lngEventID & "';"
    
        'open the connection
        Set c = New ADODB.Connection
        c.Open g_strConnection
        c.Execute l_strSQL
        
        'Record a History event
        l_strSQL = "INSERT INTO eventhistory ("
        l_strSQL = l_strSQL & "eventID, datesaved, username, description) VALUES ("
        l_strSQL = l_strSQL & "'" & l_lngEventID & "', "
        l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
        l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
        l_strSQL = l_strSQL & "'Renamed' "
        l_strSQL = l_strSQL & ");"
        
        c.Execute l_strSQL
        c.Close
        Set c = Nothing
    End If
    
    '<EhFooter>
    g_FileOpInProgress = False
    Exit Sub

shFiles_OnEndItemRename_Err:
    MsgBox Err.Description & vbCrLf & _
           "in prjJCAFileManager.frmCETAFileManager.shFiles_OnEndItemRename " & _
           "at line " & Erl, _
           vbExclamation + vbOKOnly, "Application Error"
    Resume Next
    '</EhFooter>

End Sub

Private Sub shFiles_OnItemDblClick(Index As Integer, ByVal Item As FileViewControl.IListItem, ByVal x As Long, ByVal y As Long, Cancel As Boolean)

Dim l_strFileName As String, l_strDrivePath As String, l_strPath As String, l_strFolderPath As String, l_lngEventID As Long, UNCPath As String, temp As String, i As Long
Dim FSO As Scripting.FileSystemObject
Dim sID As String, ThisFolder As String, l_strFileToOpen As String


Set FSO = New Scripting.FileSystemObject

If Not Item.IsFolder And Not ((Item.Attributes(Link) And Link) <> 0) Then

    'get the Event ID if it exists
    l_lngEventID = LookupEventID(Index, Item)
    
    If l_lngEventID = 0 Then
    
        'pick up the actual file name
        l_strFileName = Item.DisplayName
    
        'get the full file name and path
        l_strPath = shFiles(Index).CurrentFolder
    
        If Left(l_strPath, 2) <> "\\" Then
            l_strFolderPath = Right(l_strPath, Len(l_strPath) - 3)
            l_strDrivePath = Left(l_strPath, 2)
        Else
            shFiles(2).CurrentFolder = shFiles(Index).CurrentFolder
            l_strFileToOpen = shFiles(Index).CurrentFolder
            
            Do While Not FSO.FileExists(l_strFileToOpen & "\libraryID.sys") And Not FSO.FileExists(l_strFileToOpen & "\.libraryID.sys")
                l_strFileToOpen = Left(shFiles(2).CurrentFolder, Len(shFiles(2).CurrentFolder) - Len(shFiles(2).CurrentFolderDisplayName) - 1)
                shFiles(2).CurrentFolder = l_strFileToOpen
            Loop
            
            l_strDrivePath = l_strFileToOpen
            l_strFolderPath = Mid(l_strPath, Len(l_strDrivePath) + 2)
        End If
                            
        If Left(l_strDrivePath, 1) <> "\" Then
            l_strFileToOpen = Left$(shFiles(Index).CurrentFolder, 2)
        Else
            l_strFileToOpen = l_strDrivePath
        End If
        
        If FSO.FileExists(l_strFileToOpen & "\libraryID.sys") Then
            Open l_strFileToOpen & "\libraryID.sys" For Input As 1
            Line Input #1, sID
            Close 1
        ElseIf FSO.FileExists(l_strFileToOpen & "\.libraryID.sys") Then
            Open l_strFileToOpen & "\.libraryID.sys" For Input As 1
            Line Input #1, sID
            Close 1
        End If

        If MsgBox("Record didn't exist" & vbCrLf & vbCrLf & "Do you wish to atempt to run CETA and enter details.", vbYesNo + vbDefaultButton2, "CETA Record Check") = vbYes Then
            Dim l_strAppPath As String
            l_strAppPath = Chr(34) & App.Path & "\CETA Facilities Manager 2.exe" & Chr(34)
            If InStr(g_strConnection, "DATABASE=MiniCETA") > 0 Then
                l_strAppPath = l_strAppPath & " /MiniCETA"
            End If
            l_strAppPath = l_strAppPath & " /username=" & g_strUserName & " /password=" & g_strUserPassword & " /media /drive:" & sID & " /path:" & l_strFolderPath & " /filename:" & l_strFileName & ""
            Shell l_strAppPath, vbMaximizedFocus
        End If
    Else
        MsgBox "Record Exists", vbInformation, "CETA Record check"
    End If

    Cancel = True

ElseIf ((Item.Attributes(Link) And Link) <> 0) Then
    'It is a link item - try and set the path
    
    If Right(Item.Path, 4) <> ".lnk" Then shFiles(Index).CurrentFolder = Item.Path
    Cancel = True
    
End If

End Sub

Sub Catalog_Folder(l_strFolder As String, l_lngJobID As Long)

Dim l_strFileName As String, l_strDrivePath As String, l_strPath As String, l_lngEventID As Long, Item As ListItem, Count As Long, Count2 As Long, l_strEpisode As String, l_lngOnline As Long
Dim cnn As ADODB.Connection, l_strSQL As String, l_lngIntReference As Long, l_strReference As String, l_lngFileHandle As Long, l_curFileSize As Currency
Dim l_strFolderPath As String, UNCPath As String, temp As String, i As Long, l_strLastFolder As String, MediaData As MediaInfoData
Dim l_ImageDimensions As ImgDimType, FSO As FileSystemObject, Catalog_File As File, FileCreatedDate As Date, l_blnFailed As Boolean, l_blnSupressMessage As Boolean, l_lngCommaCount As Long
Dim l_rst As ADODB.Recordset
Dim sID As String, l_strFileToOpen As String

If IsCETALocked = True Then
    MsgBox "Ceta is Locked - cannot Catalogue at this time.", vbCritical, "CETA Locked"
    Exit Sub
End If

Set cnn = New ADODB.Connection
cnn.Open g_strConnection

Set FSO = New Scripting.FileSystemObject

shFiles(0).CurrentFolder = l_strFolder
Debug.Print "Cataloguing '" & l_strFolder & "'"

If shFiles(0).ItemCount > 0 Then
    For Count = 1 To shFiles(0).ItemCount
        
        If chkStopCatalog.Value <> 0 Then
            MsgBox "Catalog Cancelled"
            lblCatalogprogress.Caption = ""
            Exit Sub
        End If
        
        Set Item = shFiles(0).ListItem(Count)
        
        If Not Item.IsFolder And Item.DisplayName <> ".DS_Store" And ValidateFilename(Item.DisplayName) = False And chkSupressFilenameValidationWarnings.Value = 0 Then
            MsgBox "File " & Item.DisplayName & " has illegal characters in the filename.", vbInformation
        End If
    
        'get the full file name and path
        l_strPath = shFiles(0).CurrentFolder
    
            If Left(l_strPath, 2) <> "\\" Then
                l_strFolderPath = Right(l_strPath, Len(l_strPath) - 3)
                l_strDrivePath = Left(l_strPath, 2)
            Else
                shFiles(2).CurrentFolder = l_strPath
                l_strFileToOpen = l_strPath
                
                Do While Not FSO.FileExists(l_strFileToOpen & "\libraryID.sys") And Not FSO.FileExists(l_strFileToOpen & "\.libraryID.sys")
                    l_strFileToOpen = Left(shFiles(2).CurrentFolder, Len(shFiles(2).CurrentFolder) - Len(shFiles(2).CurrentFolderDisplayName) - 1)
                    shFiles(2).CurrentFolder = l_strFileToOpen
                Loop
                
                l_strDrivePath = l_strFileToOpen
                l_strFolderPath = Mid(l_strPath, Len(l_strFileToOpen) + 2)
            End If
        
        If ValidateFoldername(l_strFolderPath) = False And l_blnSupressMessage = False And chkSupressFilenameValidationWarnings.Value = 0 Then
            If MsgBox("This folder: " & l_strFolderPath & " has illegal characters in the foldername. Supress further messages within this folder?", vbYesNo) = vbYes Then
                l_blnSupressMessage = True
            End If
        End If
            
        If Not Item.IsFolder And Item.DisplayName <> "libraryID.sys" And Item.DisplayName <> ".libraryID.sys" And Item.DisplayName <> "Thumbs.db" And Item.DisplayName <> ".DS_Store" And Left(Item.DisplayName, 2) <> "._" And Not ((Item.Attributes(Link) And Link) <> 0) And (InStr(Item.DisplayName, "@") = 0) Then
        
            If ((chkImages.Value <> vbChecked And _
            (LCase(Right(Item.DisplayName, 4)) <> ".jpg" And LCase(Right(Item.DisplayName, 4)) <> ".gif" And LCase(Right(Item.DisplayName, 4)) <> ".bmp" _
            And LCase(Right(Item.DisplayName, 4)) <> ".png" And LCase(Right(Item.DisplayName, 4)) <> ".tif" And LCase(Right(Item.DisplayName, 4)) <> ".dpx")) _
            Or chkImages.Value = vbChecked) _
            And ((chkLogAndXML.Value <> vbChecked And _
            (LCase(Right(Item.DisplayName, 4)) <> ".xml" And LCase(Right(Item.DisplayName, 4)) <> ".xmp" And LCase(Right(Item.DisplayName, 4)) <> ".log" _
            And LCase(Right(Item.DisplayName, 4)) <> ".fcp" And LCase(Right(Item.DisplayName, 4)) <> ".txt" And LCase(Right(Item.DisplayName, 17)) <> ".pipelineschedule" _
            And LCase(Right(Item.DisplayName, 17)) <> ".pipelineprint" And LCase(Right(Item.DisplayName, 4)) <> ".csv" And LCase(Right(Item.DisplayName, 5)) <> ".xlsx" _
            And LCase(Right(Item.DisplayName, 5)) <> ".docx" And LCase(Right(Item.DisplayName, 4)) <> ".doc" And LCase(Right(Item.DisplayName, 4)) <> ".xls" _
            And LCase(Right(Item.DisplayName, 5)) <> ".vamc")) _
            Or chkLogAndXML.Value = vbChecked) _
            Then
                'get the Event ID if it exists
                l_lngEventID = LookupEventID(0, Item)
                
                If l_lngEventID = 0 Then
                
                    'pick up the actual file name
                    l_strFileName = Item.DisplayName
                
                    lblCatalogprogress.Caption = Count & " of " & shFiles(0).ItemCount & ": " & shFiles(0).CurrentFolder & "\" & l_strFileName
                    DoEvents
                
                    If Left(l_strDrivePath, 1) <> "\" Then
                        l_strFileToOpen = Left$(shFiles(0).CurrentFolder, 2)
                    Else
                        l_strFileToOpen = l_strDrivePath
                    End If
                    
                    If FSO.FileExists(l_strFileToOpen & "\libraryID.sys") Then
                        Open l_strFileToOpen & "\libraryID.sys" For Input As 1
                        Line Input #1, sID
                        Close 1
                    ElseIf FSO.FileExists(l_strFileToOpen & "\.libraryID.sys") Then
                        Open l_strFileToOpen & "\.libraryID.sys" For Input As 1
                        Line Input #1, sID
                        Close 1
                    End If
            
                    'Drive LibraryID = sID (0 if no libraryID.sys on root of drive)
                    'Path = l_strPath
                    'Filename = l_strFilename
                    If sID <> "" Then
                        If GetData("library", "format", "libraryID", sID) = "DISCSTORE" Then
                            l_lngOnline = 1
                        Else
                            l_lngOnline = 0
                        End If
                        l_lngIntReference = GetNextSequence("internalreference")
                        l_curFileSize = 0
                        l_blnFailed = False
                        FileCreatedDate = 0
                        On Error GoTo FILE_OPEN_ERROR
                        If FSO.FileExists(Replace(shFiles(0).CurrentFolder, "\\", "\\?\UNC\") & "\" & Item.DisplayName) Then
                            API_OpenFile Replace(shFiles(0).CurrentFolder, "\\", "\\?\UNC\") & "\" & Item.DisplayName, l_lngFileHandle, l_curFileSize
                            API_CloseFile l_lngFileHandle
                            Set Catalog_File = FSO.GetFile(Replace(shFiles(0).CurrentFolder, "\\", "\\?\UNC\") & "\" & Item.DisplayName)
                            FileCreatedDate = Catalog_File.DateLastModified
                            Set Catalog_File = Nothing
                        End If
CONTINUE:
                        On Error GoTo 0
                        For Count2 = 1 To Len(l_strFileName)
                            If Mid(l_strFileName, Count2, 1) = "." Then
                                l_lngCommaCount = Count2
                            End If
                        Next
                            
                        If InStr(l_strFileName, ".") > 0 And chkNoExtenders.Value = 0 Then
                            l_strReference = Mid(l_strFileName, 1, l_lngCommaCount - 1)
                        Else
                            l_strReference = l_strFileName
                        End If
                                            
                        l_strSQL = "INSERT INTO EVENTS (cuser, FileModifiedDate, libraryID, altlocation, clipfilename, online, eventmediatype, internalreference, clipreference, soundlay, eventtitle, eventsubtitle, seriesID, " _
                            & "eventseries, companyID, companyname, eventtype, jobID, svenskprojectnumber, bigfilesize) VALUES ('" & g_strUserInitials & "', '" & FormatSQLDate(FileCreatedDate) & "', " _
                            & sID & ", '" & QuoteSanitise(l_strFolderPath) & "', '" & QuoteSanitise(l_strFileName) & "', '" & l_lngOnline & "', 'FILE', '" & l_lngIntReference & "', '" _
                            & QuoteSanitise(l_strReference) & "', '" & FormatSQLDate(Now) & "', '" & QuoteSanitise(txtTitle.Text) & "', "
                            
                        If txtSubTitle.Text <> "" Then
                            l_strSQL = l_strSQL & "'" & QuoteSanitise(txtSubTitle.Text) & "', "
                        Else
                            l_strSQL = l_strSQL & "NULL, "
                        End If
                        
                        If Val(txtSeriesID.Text) <> 0 Then
                            l_strSQL = l_strSQL & Val(txtSeriesID.Text) & ", "
                        Else
                            l_strSQL = l_strSQL & "NULL, "
                        End If
                    
                        l_strSQL = l_strSQL & "'" & QuoteSanitise(cmbSeries.Text) & "', "
                        If Val(lblCompanyID.Caption) <> 0 Then
                            l_strSQL = l_strSQL & lblCompanyID.Caption & ", '" & QuoteSanitise(cmbCompany.Text) & "', "
                        Else
                            l_strSQL = l_strSQL & "1, 'Unassigned Company', "
                        End If
                        l_strSQL = l_strSQL & "'" & cmbPurpose.Text & "', "
                        l_strSQL = l_strSQL & l_lngJobID & ", "
                                            
                        If txtSvenskProjectNumber.Text <> "" Then
                            l_strSQL = l_strSQL & "'" & txtSvenskProjectNumber.Text & "', "
                        Else
                            l_strSQL = l_strSQL & "NULL, "
                        End If
                        
                        If l_blnFailed = True Then
                            l_strSQL = l_strSQL & "NULL);"
                        Else
                            l_strSQL = l_strSQL & l_curFileSize & ");"
                        End If
                        
                        Debug.Print l_strSQL
                        cnn.Execute l_strSQL
                        l_lngEventID = LookupEventID(0, Item)
                        
                        If optSetAsMasterfile(0).Value <> 0 Then
                            SetData "events", "fileversion", "eventID", l_lngEventID, "Original Masterfile"
                        ElseIf optSetAsMasterfile(1).Value <> 0 Then
                            SetData "events", "fileversion", "eventID", l_lngEventID, "Transcode"
                        End If
                        
'                        'Update the DIVA Category
'                        Set l_rst = cnn.Execute("exec cet_Get_DIVA_Category @EventID=" & l_lngEventID)
'                        SetData "events", "clipsoundformat", "eventID", l_lngEventID, l_rst(0)
'                        l_rst.Close
                        
                        If chkMediaInfo.Value <> 0 Then
                            MediaData = GetMediaInfoOnFile(Replace(shFiles(0).CurrentFolder, "\\", "\\?\UNC\") & "\" & Item.DisplayName)
                            
                            If MediaData.txtFormat = "TTML" Then
                                SetData "events", "clipformat", "eventID", l_lngEventID, "ITT Subtitles"
    '                        ElseIf MediaData.txtFormat = "JPEG Image" Or MediaData.txtFormat = "PNG Image" Or MediaData.txtFormat = "BMP Image" Or MediaData.txtFormat = "GIF Image" Then
    '                            SetData "events", "clipformat", "eventID", l_lngEventID, MediaData.txtFormat
    '                            If getImgDim(ShFiles(0).CurrentFolder & "\" & Item.DisplayName, l_ImageDimensions, temp) = True Then
    '                                SetData "events", "clipverticalpixels", "eventID", l_lngEventID, l_ImageDimensions.height
    '                                SetData "events", "cliphorizontalpixels", "eventID", l_lngEventID, l_ImageDimensions.width
    '                            End If
                            ElseIf MediaData.txtFormat <> "" Then
                                SetData "events", "clipformat", "eventID", l_lngEventID, MediaData.txtFormat
                                SetData "events", "clipaudiocodec", "eventID", l_lngEventID, MediaData.txtAudioCodec
                                SetData "events", "clipframerate", "eventID", l_lngEventID, MediaData.txtFrameRate
                                SetData "events", "cbrvbr", "eventID", l_lngEventID, MediaData.txtCBRVBR
                                SetData "events", "cliphorizontalpixels", "eventID", l_lngEventID, MediaData.txtWidth
                                SetData "events", "clipverticalpixels", "eventID", l_lngEventID, MediaData.txtHeight
                                SetData "events", "videobitrate", "eventID", l_lngEventID, MediaData.txtVideoBitrate
                                SetData "events", "interlace", "eventID", l_lngEventID, MediaData.txtInterlace
                                If chkMediaInfoTimecodes.Value <> 0 Then
                                    SetData "events", "timecodestart", "eventID", l_lngEventID, MediaData.txtTimecodestart
                                    SetData "events", "timecodestop", "eventID", l_lngEventID, MediaData.txtTimecodestop
                                    SetData "events", "fd_length", "eventID", l_lngEventID, MediaData.txtDuration
                                End If
                                SetData "events", "audiobitrate", "eventID", l_lngEventID, MediaData.txtAudioBitrate
                                SetData "events", "trackcount", "eventID", l_lngEventID, MediaData.txtTrackCount
                                SetData "events", "channelcount", "eventID", l_lngEventID, MediaData.txtChannelCount
                                SetData "events", "clipbitrate", "eventID", l_lngEventID, Val(MediaData.txtAudioBitrate) + Val(MediaData.txtVideoBitrate)
                                SetData "events", "aspectratio", "eventID", l_lngEventID, MediaData.txtAspect
                                If MediaData.txtAspect = "4:3" Then
                                    SetData "events", "fourbythreeflag", "eventID", l_lngEventID, 1
                                Else
                                    SetData "events", "fourbythreeflag", "eventID", l_lngEventID, 0
                                End If
                                SetData "events", "geometriclinearity", "eventID", l_lngEventID, MediaData.txtGeometry
                                SetData "events", "clipcodec", "eventID", l_lngEventID, MediaData.txtVideoCodec
                                SetData "events", "colorspace", "eventID", l_lngEventID, MediaData.txtColorSpace
                                SetData "events", "chromasubsampling", "eventID", l_lngEventID, MediaData.txtChromaSubsmapling
'                                If Trim(" " & GetData("events", "eventtype", "eventID", l_lngEventID)) = "" Then SetData "events", "eventtype", "eventID", l_lngEventID, GetData("xref", "format", "description", MediaData.txtVideoCodec)
                                SetData "events", "mediastreamtype", "eventID", l_lngEventID, MediaData.txtStreamType
                                If MediaData.txtSeriesTitle <> "" Then
                                    SetData "events", "eventtitle", "eventID", l_lngEventID, MediaData.txtSeriesTitle
                                    SetData "events", "eventseries", "eventID", l_lngEventID, MediaData.txtSeriesNumber
                                    SetData "events", "eventsubtitle", "eventID", l_lngEventID, MediaData.txtProgrammeTitle
                                    SetData "events", "eventepisode", "eventID", l_lngEventID, MediaData.txtEpisodeNumber
                                    SetData "events", "notes", "eventID", l_lngEventID, MediaData.txtSynopsis
                                End If
                                SetData "events", "lastmediainfoquery", "eventID", l_lngEventID, FormatSQLDate(Now)
                            Else
                                l_strFileName = GetData("events", "clipfilename", "eventID", l_lngEventID)
                                Select Case UCase(Right(l_strFileName, 3))
                                    Case "ISO"
                                        SetData "events", "clipformat", "eventID", l_lngEventID, "DVD ISO Image"
                                    Case "PDF"
                                        SetData "events", "clipformat", "eventID", l_lngEventID, "PDF File"
                                    Case "PSD"
                                        SetData "events", "clipformat", "eventID", l_lngEventID, "PSD Still Image"
                                    Case "SCC"
                                        SetData "events", "clipformat", "eventID", l_lngEventID, "SCC File (EIA 608)"
                                    Case "STL"
                                        SetData "events", "clipformat", "eventID", l_lngEventID, "STL Subtitles"
                                    Case "TIF"
                                        SetData "events", "clipformat", "eventID", l_lngEventID, "TIF Still Image"
                                    Case "DOC", "DOCX"
                                        SetData "events", "clipformat", "eventID", l_lngEventID, "Word File"
                                    Case "XLS", "XLSX"
                                        SetData "events", "clipformat", "eventID", l_lngEventID, "XLS File"
                                    Case "XML"
                                        SetData "events", "clipformat", "eventID", l_lngEventID, "XML File"
                                    Case "RAR"
                                        SetData "events", "clipformat", "eventID", l_lngEventID, "RAR Archive"
                                    Case "ZIP"
                                        SetData "events", "clipformat", "eventID", l_lngEventID, "ZIP Archive"
                                    Case "TAR"
                                        SetData "events", "clipformat", "eventID", l_lngEventID, "TAR Archive"
                                    Case Else
                                        SetData "events", "clipformat", "eventID", l_lngEventID, "Other"
                                End Select
                            End If
                        End If
                        
                        'record a usage entry for the new clip
                        l_strSQL = "INSERT INTO eventusage ("
                        l_strSQL = l_strSQL & "eventID, dateused) VALUES ("
                        l_strSQL = l_strSQL & "'" & l_lngEventID & "', "
                        l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "'"
                        l_strSQL = l_strSQL & ");"
                        cnn.Execute l_strSQL
        
                        'Record a History event
                        l_strSQL = "INSERT INTO eventhistory ("
                        l_strSQL = l_strSQL & "eventID, datesaved, username, description) VALUES ("
                        l_strSQL = l_strSQL & "'" & l_lngEventID & "', "
                        l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "', "
                        l_strSQL = l_strSQL & "'" & g_strUserInitials & "', "
                        l_strSQL = l_strSQL & "'Created' "
                        l_strSQL = l_strSQL & ");"
                        
                        cnn.Execute l_strSQL
                    
                    Else
                        MsgBox "Disc not initialised for CETA logging.", vbCritical, "Cannot Catalogue..."
                        GoTo EXITSUB
                    End If
                Else
                    'pick up the actual file name
                    l_strFileName = Item.DisplayName
                    lblCatalogprogress.Caption = Count & " of " & shFiles(0).ItemCount & ": Already Logged - " & shFiles(0).CurrentFolder & "\" & l_strFileName
                    DoEvents
                    If chkSkipVerify.Value = 0 Then
                        API_OpenFile shFiles(0).CurrentFolder & "\" & Item.DisplayName, l_lngFileHandle, l_curFileSize
                        API_CloseFile l_lngFileHandle
                        l_strSQL = "UPDATE events SET "
                        l_strSQL = l_strSQL & "soundlay = getdate(), bigfilesize = " & l_curFileSize & ", clipfilename = '" & QuoteSanitise(Item.DisplayName) & "' WHERE eventID = " & l_lngEventID & ";"
                        cnn.Execute l_strSQL
                    End If
                End If
                If chkMD5.Value <> 0 And GetData("library", "format", "libraryID", GetData("events", "libraryID", "eventID", l_lngEventID)) = "DISCSTORE" Then
                    If Trim(" " & GetData("events", "md5checksum", "eventID", l_lngEventID) <> "") Then
                        l_strSQL = "INSERT INTO event_file_request (eventID, SourceLibraryID, event_file_Request_typeID, RequestName, RequesterEmail) VALUES ("
                        l_strSQL = l_strSQL & l_lngEventID & ", "
                        l_strSQL = l_strSQL & GetData("events", "libraryID", "eventID", l_lngEventID) & ", "
                        l_strSQL = l_strSQL & GetData("event_file_request_type", "event_file_request_typeID", "description", "MD5 Checksum") & ", "
                        l_strSQL = l_strSQL & "'" & g_strUserInitials & "', '" & g_strUserEmailAddress & "');"
                        cnn.Execute l_strSQL
                    End If
                End If
            ElseIf (chkImages.Value <> vbChecked And _
            (LCase(Right(Item.DisplayName, 4)) = ".jpg" Or LCase(Right(Item.DisplayName, 4)) = ".gif" Or LCase(Right(Item.DisplayName, 4)) = ".bmp" _
            Or LCase(Right(Item.DisplayName, 4)) = ".png" Or LCase(Right(Item.DisplayName, 4)) = ".tif" Or LCase(Right(Item.DisplayName, 4)) = ".dpx")) _
            Or (chkLogAndXML.Value <> vbChecked And _
            (LCase(Right(Item.DisplayName, 4)) = ".xml" Or LCase(Right(Item.DisplayName, 4)) = ".xmp" Or LCase(Right(Item.DisplayName, 4)) = ".log" _
            Or LCase(Right(Item.DisplayName, 4)) = ".txt" Or LCase(Right(Item.DisplayName, 17)) = ".pipelineschedule" Or LCase(Right(Item.DisplayName, 17)) = ".pipelineprint" _
            Or LCase(Right(Item.DisplayName, 4)) = ".fcp" Or LCase(Right(Item.DisplayName, 4)) = ".csv" Or LCase(Right(Item.DisplayName, 5)) = ".xlsx" _
             Or LCase(Right(Item.DisplayName, 4)) = ".xls" Or LCase(Right(Item.DisplayName, 4)) = ".doc" Or LCase(Right(Item.DisplayName, 5)) = ".docx")) _
            Then
                l_strFileName = Item.DisplayName
                lblCatalogprogress.Caption = Count & " of " & shFiles(0).ItemCount & ": Skipping " & l_strFileName
                DoEvents
            End If
            
        
        ElseIf Item.IsFolder And (InStr(Item.DisplayName, "@") = 0) Then
        
            If FSO.FolderExists(Replace(shFiles(0).CurrentFolder, "\\", "\\?\UNC\") & "\" & Item.DisplayName) Then
                On Error Resume Next
                l_strLastFolder = shFiles(0).CurrentFolder
                Catalog_Folder shFiles(0).CurrentFolder & "\" & Item.DisplayName, l_lngJobID
                shFiles(0).CurrentFolder = l_strLastFolder
                On Error GoTo 0
            End If
            
        End If
        
    Next
End If
EXITSUB:

cnn.Close
Set cnn = Nothing
lblCatalogprogress.Caption = ""
Exit Sub

FILE_OPEN_ERROR:
l_blnFailed = True
GoTo CONTINUE

End Sub

Private Sub timShutDown_Timer()

Dim l_strConnectionDisplay As String, Count As Long

Count = InStr(g_strConnection, "SERVER")
l_strConnectionDisplay = Mid(g_strConnection, Count)
Count = InStr(l_strConnectionDisplay, "DATABASE")
l_strConnectionDisplay = Left(l_strConnectionDisplay, Count - 1)

If g_FileOpInProgress = True Then Exit Sub

g_optCloseSystemDown = GetFlag(GetData("setting", "value", "name", "CloseSystemDown"))

If g_optCloseSystemDown <> False And IsCETALocked = True Then
    Beep
    If frmLogin.Visible = True Then
        frmLogin.Hide
        'ExitCETA True
    End If
    frmShutdownWarning.lblShutdownWarning.Caption = "The System Will Shutdown In " & g_intMinutesToCloseDown & " MINUTES"
    frmShutdownWarning.Show
    frmShutdownWarning.ZOrder 0
    If g_intMinutesToCloseDown <= 0 Then
        Unload frmShutdownWarning
        End
    End If
    g_intMinutesToCloseDown = g_intMinutesToCloseDown - 1
Else
    g_intMinutesToCloseDown = 5
    frmCetaFileManager.Caption = "CETA File Manager - logged on as " & g_strFullUserName & " using database " & l_strConnectionDisplay
End If

End Sub
Private Sub tmrPeriod_Timer()
    'this timer continuesly monitors the
    'value of IsIdle to see if the system ha
    '     s been
    'idle for INTERVAL


    If IsIdle Then
        'if the difference between now (timer) a
        '     nd the
        'time the idle started, is => INTERVAL,
        '     then
        'the 'idle state' has been reached

        If Timer - startOfIdle >= INTERVAL Then
            'call the sub that will handle any code
            '     at this stage
            'this is merely to seperate the idle che
            '     ck code
            'from your own code
            'NOTE: I advise you to perform some sort
            '     of
            'check here to see if the idle state has
            '     been
            'reached for the first time, or if the s
            '     ystem
            'has just been idling ever since the idl
            '     e state
            'was reached
            Call IdleStateEngaged(Timer)
            'important: set the values
            startOfIdle = Timer
            IsIdle = True
        End If
    Else ' not idling, or the idlestate has been left
        'call the sub
        'NOTE: I advise you to perform some sort
        '     of
        'check here to see if the system was in
        '     the
        'idle state, or if the system
        'has not been idling anyway
        Call IdleStateDisengaged(Timer)
    End If
End Sub

Public Sub IdleStateEngaged(ByVal IdleStartTime As Long)
    
'System has been idle for 300 second (5 minutes)
'Check whether this is a machine that should autologout, and do it if it is.

If g_FileOpInProgress = True Then Exit Sub

If InStr(GetData("xref", "descriptionalias", "description", CurrentMachineName()), "/autologout") > 0 Then
    Logout
End If

End Sub

Public Sub IdleStateDisengaged(ByVal IdleStopTime As Long)

'Nothing significant needs to happen here

End Sub

Private Sub tmrStateMonitor_Timer()
    'holds the state of the key that is bein
    '     g monitored
    Dim state As Integer
    'holds the CURRENT mouse position.
    'It's to compare the current position wi
    '     th the previous
    'position
    Dim tmpPos As POINTAPI
    Dim Ret As Long 'simply holds the return value of the API
    'this checks if a key/button is pressed,
    '     or
    'if the mouse has moved.
    Dim IdleFound As Boolean
    Dim i As Integer 'the counter uses by the For loop
    IdleFound = False
    'Here I'm not sure about myself:
    'I don't know to what to set the value
    '256 to. It works as is, though!
    'And, what it does, is retrieve the stat
    '     e of each
    'individual key.


    For i = 1 To 256
        'call the API
        state = GetAsyncKeyState(i)
        'state will = -32767 if the 'i' key/butt
        '     on is
        'currently being pressed:


        If state = -32767 Then
            'if it is pressed, then this is the end
            '     of any idles
            IdleFound = True 'means that something is withholding the computer of idling
            IsIdle = False 'thus, it is not idling, so set the value
        End If
    Next
    'get the position of the mouse cursor
    Ret = GetCursorPos(tmpPos)
    'if the coordinates of the mouse are dif
    '     ferent than
    'last time or when the idle started, the
    '     n the system
    'is not idling:


    If tmpPos.x <> MousePos.x Or tmpPos.y <> MousePos.y Then
        IsIdle = False 'set the...
        IdleFound = True 'values
        'store the current coordinates so that w
        '     e
        'can compare next time round
        MousePos.x = tmpPos.x
        MousePos.y = tmpPos.y
    End If
    'if something did not withhold the idle
    '     then...


    If Not IdleFound Then
        'if isIdle not equals false, then don't
        '     reset the
        'startOfIdle!!


        If Not IsIdle Then
            'if it is false, then the idle is beginn
            '     ing
            IsIdle = True
            startOfIdle = Timer
        End If
    End If
End Sub

Private Sub txtTitle_Click()
txtSeriesID.Text = txtTitle.Columns("SeriesID").Text
End Sub

