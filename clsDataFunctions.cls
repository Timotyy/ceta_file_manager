VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsDataFunctions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Function clsExecuteSQL(ByVal SQL As String, MsgString As String, ByVal lp_strConnection As String) As ADODB.Recordset

        '<EhHeader>
        On Error GoTo ExecuteSQL_Err

        '</EhHeader>

100     g_lngLastID = 0

        'Debug.Print SQL

        'executes SQL and returns Recordset
        Dim cnn As ADODB.Connection, rst As ADODB.Recordset, l_strSQL As String

        'Dim sTokens() As String

        On Error GoTo ExecuteSQL_Error

        'split the SQL up in to individual SQL statements, seperated by ; (semicolon)
        'sTokens = Split(SQL, ";")
102     Set cnn = New ADODB.Connection
104     cnn.Open lp_strConnection
    
        'if this is an action query, and there will be no results returned
106     If Left(UCase(SQL), 6) = "INSERT" Or Left(UCase(SQL), 6) = "DELETE" Or Left(UCase(SQL), 6) = "UPDATE" Or Left(UCase(SQL), 5) = "ALTER" Then
        
            'remove a preleading semicolon if there
108         If Left(SQL, 1) = ";" Then SQL = Right(SQL, Len(SQL) - 1)
        
110         cnn.Execute SQL
        
112         MsgString = SQL & " query successful"
        
            'if insert query we need to get the ID
        
114         If Left(UCase(SQL), 6) = "INSERT" Then
        
                'different code for MySQL and MS SQL - Duh!
116             Select Case g_dbtype

                    Case "mssql"
118                     l_strSQL = "SELECT @@identity;"

120                 Case "mysql"
122                     l_strSQL = "SELECT LAST_INSERT_ID();"
                End Select
        
                'get the insert ID
                Dim l_rstID As New ADODB.Recordset

124             l_rstID.Open l_strSQL, cnn, adOpenKeyset, adLockReadOnly

126             If Not l_rstID.EOF Then
128                 g_lngLastID = l_rstID(0)
                Else
130                 g_lngLastID = 0
                End If
        
132             l_rstID.Close
134             Set l_rstID = Nothing
136             cnn.Close
            End If

        Else
            'this was a select query, so just get back the results
    
138         Set rst = New ADODB.Recordset
        
140         rst.Open Trim$(SQL), cnn, adOpenKeyset, adLockOptimistic
            'If rst.EOF = False Then
            'rst.MoveLast 'get approx RecordCount
            'rst.MoveFirst
            'End If
        
142         Set clsExecuteSQL = rst
144         MsgString = rst.RecordCount & " records found from SQL"
        
        End If

ExecuteSQL_Exit:

146     Set rst = Nothing
148     Set cnn = Nothing

        Exit Function

ExecuteSQL_Error:
150     MsgString = "clsExecuteSQL *ERROR* " & Err.Description
152     g_strDebugSQLString = SQL

        'Resume ExecuteSQL_Exit
        '<EhFooter>
        Exit Function

ExecuteSQL_Err:
        Err.Raise vbObjectError + 100, "Project1.clsDataFunctions.clsExecuteSQL", "clsDataFunctions component failure"

        '</EhFooter>

End Function

Function clsGetData(lp_strTableName As String, _
                 lp_strFieldToReturn As String, _
                 lp_strFieldToSearch As String, _
                 lp_varCriterea As Variant, _
                 ByVal lp_strConnection As String) As Variant

        '<EhHeader>
        On Error GoTo GetData_Err

        '</EhHeader>

        On Error GoTo Proc_GetData_Error

        Dim l_strSQL As String

100     l_strSQL = "SELECT " & lp_strFieldToReturn & " FROM " & lp_strTableName & " WHERE " & lp_strFieldToSearch & " = '" & QuoteSanitise(lp_varCriterea) & "'"

        Dim l_rstGetData As New ADODB.Recordset

        Dim l_con        As New ADODB.Connection

102     l_con.Open lp_strConnection
104     l_rstGetData.Open l_strSQL, l_con, adOpenStatic, adLockReadOnly

106     If Not l_rstGetData.EOF Then
    
108         If Not IsNull(l_rstGetData(lp_strFieldToReturn)) Then
110             clsGetData = l_rstGetData(lp_strFieldToReturn)
112             GoTo Proc_CloseDB
            End If

        Else
114         GoTo Proc_CloseDB

        End If

116     Select Case l_rstGetData.Fields(lp_strFieldToReturn).Type

            Case adChar, adVarChar, adVarWChar, 201, 203
118             clsGetData = ""

120         Case adBigInt, adBinary, adBoolean, adCurrency, adDecimal, adDouble, adInteger
122             clsGetData = 0

124         Case adDate, adDBDate, adDBTime, adDBTimeStamp
126             clsGetData = 0

128         Case Else
130             clsGetData = False
        End Select

Proc_CloseDB:

        On Error Resume Next

132     l_rstGetData.Close
134     Set l_rstGetData = Nothing

136     l_con.Close
138     Set l_con = Nothing

        Exit Function

Proc_GetData_Error:

140     MsgBox prompt_error$ & " - clsGetData: " & Err.Description

142     clsGetData = ""

        'GoTo Proc_CloseDB

        '<EhFooter>
        Exit Function

GetData_Err:
        Err.Raise vbObjectError + 100, "Project1.clsDataFunctions.clsGetData", "clsDataFunctions component failure"

        '</EhFooter>

End Function

Function clsGetDataSQL(lp_strSQL As String, ByVal lp_strConnection As String)

        '<EhHeader>
        On Error GoTo GetDataSQL_Err

        '</EhHeader>

        On Error GoTo Proc_GetData_Error

        Dim l_rstGetData As New ADODB.Recordset

        Dim l_conGetData As New ADODB.Connection

100     l_conGetData.Open lp_strConnection
102     l_rstGetData.Open lp_strSQL, l_conGetData, adOpenStatic, adLockReadOnly

104     CheckForSQLError

106     If Not l_rstGetData.EOF Then
    
108         If Not IsNull(l_rstGetData(0)) Then
110             clsGetDataSQL = l_rstGetData(0)
112             GoTo Proc_CloseDB
            End If

        End If

114     Select Case l_rstGetData.Fields(0).Type

            Case adChar, adVarChar, adVarWChar, 201
116             clsGetDataSQL = ""

118         Case adBigInt, adBinary, adBoolean, adCurrency, adDecimal, adDouble, adInteger
120             clsGetDataSQL = 0

122         Case adDate, adDBDate, adDBTime, adDBTimeStamp
124             clsGetDataSQL = 0

126         Case Else
128             clsGetDataSQL = False
        End Select

Proc_CloseDB:

130     l_rstGetData.Close
132     Set l_rstGetData = Nothing

134     l_conGetData.Close
136     Set l_conGetData = Nothing

        Exit Function

Proc_GetData_Error:

        'Err.Raise Err.Number
138     g_strExecuteError = "*ERROR* - " & Err.Description

140     clsGetDataSQL = ""

        '<EhFooter>
        Exit Function

GetDataSQL_Err:
        Err.Raise vbObjectError + 100, "Project1.clsDataFunctions.clsGetDataSQL", "clsDataFunctions component failure"

        '</EhFooter>

End Function

Function clsGetCount(lp_strSQL As String, ByVal lp_strConnection As String) As Double

        '<EhHeader>
        On Error GoTo GetCount_Err

        '</EhHeader>

100     Screen.MousePointer = vbHourglass

        Dim l_conSearch As ADODB.Connection

        Dim l_rstSearch As ADODB.Recordset

102     Set l_conSearch = New ADODB.Connection
104     Set l_rstSearch = New ADODB.Recordset

106     l_conSearch.ConnectionString = lp_strConnection
108     l_conSearch.Open

110     With l_rstSearch
112         .CursorLocation = adUseClient
114         .LockType = adLockBatchOptimistic
116         .CursorType = adOpenDynamic
118         .Open lp_strSQL, l_conSearch, adOpenStatic, adLockReadOnly
        End With

120     l_rstSearch.ActiveConnection = Nothing

122     If l_rstSearch.EOF Then
124         clsGetCount = 0
        Else

126         If Not IsNull(l_rstSearch(0)) Then
128             clsGetCount = Val(l_rstSearch(0))
            Else
130             clsGetCount = 0
            End If
        End If

132     l_rstSearch.Close
134     Set l_rstSearch = Nothing

136     l_conSearch.Close
138     Set l_conSearch = Nothing

140     Screen.MousePointer = vbDefault

        '<EhFooter>
        Exit Function

GetCount_Err:
        Err.Raise vbObjectError + 100, "Project1.clsDataFunctions.clsGetCount", "clsDataFunctions component failure"

        '</EhFooter>

End Function

Function clsRecordExists(lp_strTable As String, _
                      lp_strFieldToSearch As String, _
                      lp_varCriteria As Variant, _
                      ByVal lp_strConnection) As Boolean

        '<EhHeader>
        On Error GoTo RecordExists_Err

        '</EhHeader>

        Dim l_strSQL As String

100     l_strSQL = "SELECT count(" & lp_strFieldToSearch & ") FROM " & lp_strTable & " WHERE " & lp_strFieldToSearch & " = '" & QuoteSanitise(lp_varCriteria) & "'"

        Dim l_rstExists As New ADODB.Recordset

102     Set l_rstExists = ExecuteSQL(l_strSQL, g_strExecuteError, lp_strConnection)

104     If l_rstExists(0) = 0 Then
106         clsRecordExists = False
        Else
108         clsRecordExists = True
        End If

110     l_rstExists.Close
112     Set l_rstExists = Nothing

        '<EhFooter>
        Exit Function

RecordExists_Err:
        Err.Raise vbObjectError + 100, "Project1.clsDataFunctions.clsRecordExists", "clsDataFunctions component failure"

        '</EhFooter>

End Function

Function clsSetData(lp_strTableName As String, _
                 lp_strFieldToEdit As String, _
                 lp_strFieldToSearch As String, _
                 lp_varCriterea As Variant, _
                 lp_varValue As Variant, _
                 ByVal lp_strConnection) As Variant

        '<EhHeader>
        On Error GoTo SetData_Err

        '</EhHeader>

        Dim l_strSQL As String, l_UpdateValue As String

100     If UCase(lp_varValue) = "NULL" Or lp_varValue = "" Then
102         l_UpdateValue = "NULL"
        Else
104         l_UpdateValue = "'" & QuoteSanitise(lp_varValue) & "'"
        End If

106     l_strSQL = "UPDATE " & lp_strTableName & " SET " & lp_strFieldToEdit & " = " & l_UpdateValue & " WHERE " & lp_strFieldToSearch & " = '" & QuoteSanitise(lp_varCriterea) & "'"

108     ExecuteSQL l_strSQL, g_strExecuteError, lp_strConnection
110     CheckForSQLError

        '<EhFooter>
        Exit Function

SetData_Err:
        Err.Raise vbObjectError + 100, "Project1.clsDataFunctions.clsSetData", "clsDataFunctions component failure"

        '</EhFooter>

End Function

