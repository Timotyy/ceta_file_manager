Attribute VB_Name = "modLogin"
Option Explicit

Public g_strUserName As String
Public g_strUserPassword As String
Public g_strUserAccessCode As String
Public g_strUserInitials As String
Public g_strFullUserName As String
Public g_strUserEmailAddress As String
Public g_lngUserID As Integer
Public g_intPasswordNeverExpires As Integer
Public g_datLastChangedPasswordDate As Date
Public g_strWorkstation As String
Public g_intPasswordChangeEvery As Integer
Public g_LoginCancelled As Boolean
Private Const MAX_COMPUTERNAME_LENGTH As Long = 15&

'API functions
Public Declare Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" (ByVal lpBuffer As String, nSize As Long) As Long
Private Declare Function GetComputerName Lib "kernel32" _
   Alias "GetComputerNameA" (ByVal lpBuffer As String, _
   nSize As Long) As Long


Public Function GetLoginDetails()

Dim l_strUserName As String, l_strPassword As String, Count As Long
Dim l_strCommandLine As String

l_strCommandLine = Command()

l_strUserName = ""
l_strPassword = ""

If InStr(l_strCommandLine, "/username=") > 0 Then
    Count = InStr(l_strCommandLine, "/username=")
    l_strUserName = Mid(l_strCommandLine, Count + 10)
    If InStr(l_strUserName, " ") > 0 Then
        l_strUserName = Left(l_strUserName, InStr(l_strUserName, " ") - 1)
    ElseIf InStr(l_strUserName, "/") > 0 Then
        l_strUserName = Left(l_strUserName, InStr(l_strUserName, "/") - 1)
    End If
End If

If InStr(l_strCommandLine, "/password=") > 0 Then
    Count = InStr(l_strCommandLine, "/password=")
    l_strPassword = Mid(l_strCommandLine, Count + 10)
    If InStr(l_strPassword, " ") > 0 Then
        l_strPassword = Left(l_strPassword, InStr(l_strPassword, " ") - 1)
    ElseIf InStr(l_strPassword, "/") > 0 Then
        l_strPassword = Left(l_strPassword, InStr(l_strPassword, "/") - 1)
    End If
End If

g_LoginCancelled = False

AutomaticLogin l_strUserName, l_strPassword

End Function

Function AutomaticLogin(Optional lp_strUserName As String, Optional lp_strPassword As String)
    
'    On Error GoTo AutomaticLogin_Error
    
    Dim sBuffer As String
    Dim lSize As Long
    
    Dim l_strUserName As String
    
    sBuffer = Space$(255)
    lSize = Len(sBuffer)
    
    'call API function to get windows user name
    Call GetUserName(sBuffer, lSize)
    
    'make sure there is a user name
    If lSize > 0 Then
        l_strUserName = Left$(sBuffer, lSize)
    Else
        l_strUserName = vbNullString
    End If
    
    Dim l_blnResult As Boolean
    
    'for some reason this picks up a funny charactes as well as the user name
    'chop it off the end
    l_strUserName = Left(l_strUserName, Len(l_strUserName) - 1)
    
    'check user name (if there is one) and try to login
    If l_strUserName <> "" Then
        l_blnResult = CheckLogon(l_strUserName, "")
    End If
    
    'If unsuccessful check the command line for login credentials and try those
    If l_blnResult = False Then
        l_blnResult = CheckLogon(lp_strUserName, lp_strPassword)
    End If
    
    'if unsucessful, then log in manually
    If l_blnResult = False Then
        Logout
    End If
    
    Exit Function
    
AutomaticLogin_Error:
    
    MsgBox Err.Description, vbExclamation
    Logout
    
    Exit Function
    
End Function

Function CheckLogon(lp_strUserName As String, lp_strPassword As String) As Boolean
    
    Dim l_strPassword As String
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim l_strConnectionDisplay As String, Count As Long
    
    Count = InStr(g_strConnection, "SERVER")
    l_strConnectionDisplay = Mid(g_strConnection, Count)
    Count = InStr(l_strConnectionDisplay, "DATABASE")
    l_strConnectionDisplay = Left(l_strConnectionDisplay, Count - 1)
    
    If InStr(lp_strUserName, Chr(39)) <> 0 Or InStr(lp_strPassword, Chr(39)) <> 0 Then
        CheckLogon = False
        Exit Function
    End If
    
'    If g_optUseMD5Passwords = 1 Then
        If lp_strPassword <> "" Then l_strPassword = MD5Encrypt(lp_strPassword)
'    End If
    
    Dim cnn As ADODB.Connection
    Set cnn = New ADODB.Connection
    cnn.Open g_strConnection
    
    Dim l_strSQL As String
    
    l_strSQL = "SELECT * FROM cetauser WHERE username = '" & lp_strUserName & "' "
    
    If lp_strPassword = "" Then
        l_strSQL = l_strSQL & "AND fd_password = 'nopassword'"
    Else
        l_strSQL = l_strSQL & "AND fd_password = '" & l_strPassword & "'"
    End If
    
    Dim l_rstUser As ADODB.Recordset
    Set l_rstUser = New ADODB.Recordset
    l_rstUser.Open l_strSQL, cnn, 3, 3
    
    If l_rstUser.RecordCount < 0 Then
        GoTo CheckLogon_Error
    End If
    
    If l_rstUser.EOF Then
        g_strUserName = ""
        g_strUserPassword = ""
        g_strUserAccessCode = ""
        g_strUserInitials = ""
        g_strFullUserName = ""
        g_strUserEmailAddress = ""
        g_lngUserID = 0
        g_datLastChangedPasswordDate = vbEmpty
        g_intPasswordNeverExpires = 0
        CheckLogon = False
    Else
        g_strUserName = lp_strUserName
        g_strUserPassword = lp_strPassword
        g_strUserAccessCode = Trim(" " & l_rstUser("accesscode"))
        g_strUserInitials = Format(l_rstUser("initials"))
        g_strFullUserName = Format(l_rstUser("fullname"))
        g_strUserEmailAddress = Format(l_rstUser("email"))
        g_lngUserID = l_rstUser("cetauserID")
        g_intPasswordNeverExpires = GetFlag(l_rstUser("passwordneverexpires"))
        If IsDate(l_rstUser("lastchangedpassworddate")) Then
            g_datLastChangedPasswordDate = l_rstUser("lastchangedpassworddate")
        Else
            g_datLastChangedPasswordDate = vbEmpty
        End If
        CheckLogon = True
    End If
    
    l_rstUser.Close
    Set l_rstUser = Nothing
    
    If CheckLogon = True Then
        If Not CheckAccess("/allowlogon") Then
            CheckLogon = False
        End If
    End If
        
    If CheckLogon = True Then
        l_strSQL = "INSERT INTO session (userID, username, workstation, logindate) VALUES ("
        l_strSQL = l_strSQL & "'" & g_lngUserID & "', "
        l_strSQL = l_strSQL & "'" & g_strUserName & "', "
        l_strSQL = l_strSQL & "'" & g_strWorkstation & "', "
        l_strSQL = l_strSQL & "'" & FormatSQLDate(Now) & "') "
        cnn.Execute l_strSQL
    End If
    
    frmCetaFileManager.Caption = "CETA File Manager - logged on as " & g_strFullUserName & " using database " & l_strConnectionDisplay
    
    Exit Function
CheckLogon_Error:
    MsgBox "An error occured while trying to log on."
    Set l_rstUser = Nothing
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume 'PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function MD5Encrypt(ByVal lp_strStringToEncrypt As String) As String

Dim oMD5 As New CMD5
Set oMD5 = New CMD5

Dim l_strEncryptedString As String
l_strEncryptedString = oMD5.MD5(lp_strStringToEncrypt)

Set oMD5 = Nothing

MD5Encrypt = l_strEncryptedString

End Function

Public Function CheckAccess(lp_strAccesscode As String, Optional lp_intDontShowMessage As Integer) As Boolean
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If (InStr(UCase(g_strUserAccessCode), UCase(lp_strAccesscode)) <> 0) Or (InStr(UCase(g_strUserAccessCode), "/SUPERUSER") <> 0) Then
        If InStr(UCase(g_strUserAccessCode), UCase("--" & lp_strAccesscode)) = 0 Then
            CheckAccess = True
        Else
        
            'check the security template for the correct
            
            GoTo PROC_CheckSecurityTemplate
        
            GoTo PROC_BlockedAccess
        End If
    Else

    GoTo PROC_CheckSecurityTemplate

PROC_BlockedAccess:

        If Val(lp_intDontShowMessage) = 0 Then
            MsgBox "You do not have the required access rights to use this function." & vbCrLf & vbCrLf & "If you need to use this feature, the access code you need to give to your CETA Administrator is: " & lp_strAccesscode, vbExclamation, "No Access - " & lp_strAccesscode
        End If
    End If
    
    Exit Function

PROC_CheckSecurityTemplate:

    Dim l_lngSecurityTemplateUserID  As Long
    l_lngSecurityTemplateUserID = GetData("cetauser", "securitytemplate", "cetauserID", g_lngUserID)
    
    If l_lngSecurityTemplateUserID > 0 Then
        Dim l_strSecurityAccessCode As String
        l_strSecurityAccessCode = GetData("cetauser", "accesscode", "cetauserID", l_lngSecurityTemplateUserID)
        
        CheckAccess = False
    
        If (InStr(UCase(l_strSecurityAccessCode), UCase(lp_strAccesscode)) <> 0) Or (InStr(UCase(l_strSecurityAccessCode), "/SUPERUSER") <> 0) Then
            If InStr(UCase(l_strSecurityAccessCode), UCase("--" & lp_strAccesscode)) = 0 Then
                CheckAccess = True
                Exit Function
            End If
        End If
    End If
    
    GoTo PROC_BlockedAccess
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Sub Logout()
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    frmLogin.Show vbModal
    
    Unload frmLogin
    Set frmLogin = Nothing
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Public Function CurrentMachineName() As String
    
        
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim lSize As Long
    Dim sBuffer As String
    sBuffer = Space$(MAX_COMPUTERNAME_LENGTH + 1)
    lSize = Len(sBuffer)
    
    If GetComputerName(sBuffer, lSize) Then
        CurrentMachineName = Left$(sBuffer, lSize)
    End If
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function


