Attribute VB_Name = "modFunctions"
Option Explicit

Public g_strConnection As String

Global Const g_strCompanyFields = "name AS 'Company Name', address AS 'Address', postcode AS 'Post Code', telephone AS 'Telephone', fax AS 'Fax', accountcode AS 'A/C Code', accountstatus AS 'A/C Status', insuranceoutdate AS 'Insurance', companyID AS 'Company ID'"

Public Const FO_COPY = &H2

Public Const FO_DELETE = &H3

Public Const FO_MOVE = &H1

Public Const FO_RENAME = &H4

Public Const FO_FOLDERMOVE = &H5

Public Const FO_FOLDERDELETE = &H6

Public Const FO_FOLDERCOPY = &H7

Public Const g_dbtype = "mssql"

Public g_optCloseSystemDown As Integer

Public g_intMinutesToCloseDown As Integer

Private Const RESOURCETYPE_ANY = &H0

Private Const RESOURCE_CONNECTED = &H1

Private Type NETRESOURCE

    dwScope As Long
    dwType As Long
    dwDisplayType As Long
    dwUsage As Long
    lpLocalName As Long
    lpRemoteName As Long
    lpComment As Long
    lpProvider As Long

End Type

Public g_lngLastID As Long

Public g_strDebugSQLString As String

Public g_FileOpInProgress As Boolean

Private Declare Function WNetOpenEnum _
                Lib "mpr.dll" _
                Alias "WNetOpenEnumA" (ByVal dwScope As Long, _
                                       ByVal dwType As Long, _
                                       ByVal dwUsage As Long, _
                                       lpNetResource As Any, _
                                       lphEnum As Long) As Long

Private Declare Function WNetEnumResource _
                Lib "mpr.dll" _
                Alias "WNetEnumResourceA" (ByVal hEnum As Long, _
                                           lpcCount As Long, _
                                           lpBuffer As Any, _
                                           lpBufferSize As Long) As Long

Private Declare Function WNetCloseEnum Lib "mpr.dll" (ByVal hEnum As Long) As Long

Private Declare Function lstrlen _
                Lib "kernel32" _
                Alias "lstrlenA" (ByVal lpString As Any) As Long

Private Declare Function lstrcpy _
                Lib "kernel32" _
                Alias "lstrcpyA" (ByVal lpString1 As Any, _
                                  ByVal lpString2 As Any) As Long

Declare Function LockWindowUpdate Lib "user32" (ByVal hwndLock As Long) As Long

Function QuoteSanitise(lp_strText) As String
        '<EhHeader>
        On Error GoTo QuoteSanitise_Err
        '</EhHeader>


        Dim l_strNewText As String

        Dim l_strOldText As String

        Dim l_intLoop    As Integer

        l_strNewText = ""
    
        If IsNull(lp_strText) Then
            l_strOldText = ""
            l_strNewText = l_strOldText
            lp_strText = ""
        Else
            l_strOldText = lp_strText
        End If
    
        If InStr(lp_strText, "'") <> 0 Then
        
            For l_intLoop = 1 To Len(l_strOldText)
            
                If Mid(l_strOldText, l_intLoop, 1) = "'" Then
                    l_strNewText = l_strNewText & "''"
                Else
                    l_strNewText = l_strNewText & Mid(l_strOldText, l_intLoop, 1)
                End If
            
            Next

        Else
            l_strNewText = lp_strText
        End If
    
        If Right(l_strNewText, 1) = "/" Or Right(l_strNewText, 1) = "\" Then
            l_strNewText = Left(l_strNewText, Len(l_strNewText) - 1)
        End If
    
        QuoteSanitise = l_strNewText
    

        '<EhFooter>
        Exit Function

QuoteSanitise_Err:
        MsgBox Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.QuoteSanitise " & _
               "at line " & Erl, _
               vbExclamation + vbOKOnly, "Application Error"
        Resume Next
        '</EhFooter>
End Function

Function GetData(lp_strTableName As String, lp_strFieldToReturn As String, lp_strFieldToSearch As String, lp_varCriterea As Variant) As Variant
        '<EhHeader>
        On Error GoTo GetData_Err
        '</EhHeader>


    Dim l_strSQL As String
    l_strSQL = "SELECT TOP 1 " & lp_strFieldToReturn & " FROM " & lp_strTableName & " WHERE " & lp_strFieldToSearch & " = '" & (lp_varCriterea) & "'"

    Dim l_rstGetData As New ADODB.Recordset
    Dim l_con As New ADODB.Connection

    l_con.Open g_strConnection
    l_rstGetData.Open l_strSQL, l_con, adOpenStatic, adLockReadOnly

    If Not l_rstGetData.EOF Then
    
        If Not IsNull(l_rstGetData(lp_strFieldToReturn)) Then
            GetData = l_rstGetData(lp_strFieldToReturn)
            GoTo Proc_CloseDB
        End If

    Else
        GoTo Proc_CloseDB

    End If

    Select Case l_rstGetData.Fields(lp_strFieldToReturn).Type
    Case adChar, adVarChar, adVarWChar, 201, 203
        GetData = ""
    Case adBigInt, adBinary, adBoolean, adCurrency, adDecimal, adDouble, adInteger
        GetData = 0
    Case adDate, adDBDate, adDBTime, adDBTimeStamp
        GetData = 0
    Case Else
        GetData = False
    End Select

Proc_CloseDB:

    On Error Resume Next

    l_rstGetData.Close
    Set l_rstGetData = Nothing

    l_con.Close
    Set l_con = Nothing



        '<EhFooter>
        Exit Function

GetData_Err:
        MsgBox Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.GetData " & _
               "at line " & Erl, _
               vbExclamation + vbOKOnly, "Application Error"
        Resume Next
        '</EhFooter>
End Function
Public Function CopyEventToLibraryID(lp_lngEventID As Long, lp_lngLibraryID As Long, lp_lngBlankMasterfile As Long) As Long

'copy the info from one job to another
Dim l_rstOriginalEvent As ADODB.Recordset
Dim l_rstNewEvent As ADODB.Recordset, l_lngNewEventID As Long
Dim c As ADODB.Connection
Dim l_rstTemp As ADODB.Recordset, l_lngTechrevID As Long

'get the original job's details
Dim l_strSQL As String

Set c = New ADODB.Connection
c.Open g_strConnection

l_strSQL = "INSERT INTO events ("
l_strSQL = l_strSQL & "eventmediatype, libraryID, companyID) VALUES ("
l_strSQL = l_strSQL & "'FILE', " & lp_lngLibraryID & ", 1);SELECT SCOPE_IDENTITY();"

Debug.Print l_strSQL
Set l_rstOriginalEvent = New ADODB.Recordset
Set l_rstOriginalEvent = c.Execute(l_strSQL)

l_lngNewEventID = l_rstOriginalEvent.NextRecordset().Fields(0).Value

l_strSQL = "SELECT * FROM events WHERE eventID = '" & lp_lngEventID & "';"
l_rstOriginalEvent.Open l_strSQL, c, adOpenDynamic, adLockBatchOptimistic

'allow adding of a new job
l_strSQL = "SELECT * FROM events WHERE eventID = '" & l_lngNewEventID & "';"
Set l_rstNewEvent = New ADODB.Recordset
l_rstNewEvent.Open l_strSQL, c, adOpenDynamic, adLockOptimistic

Dim l_intLoop As Integer
Dim l_lngConflict As Long
    
If l_rstOriginalEvent.EOF Then Exit Function
    
l_rstNewEvent("libraryID") = lp_lngLibraryID

For l_intLoop = 0 To l_rstOriginalEvent.Fields.Count - 1
    If l_rstOriginalEvent.Fields(l_intLoop).Name <> "libraryID" And l_rstOriginalEvent.Fields(l_intLoop).Name <> "eventID" _
    And l_rstOriginalEvent.Fields(l_intLoop).Name <> "MediaWindowDeleteDate" And l_rstOriginalEvent.Fields(l_intLoop).Name <> "sound_stereo_russian" Then
    
        If Not IsNull(l_rstOriginalEvent(l_intLoop)) Then
            l_rstNewEvent(l_intLoop) = l_rstOriginalEvent(l_intLoop)
        End If
    
    End If
Next

'Set the 'Hide from Web' to 0 if it is the StreamStore or the AsperaStore, or FlashStore or Aspera-2 or Aspera-3 or landing pad

If lp_lngLibraryID = 248095 Or lp_lngLibraryID = 276356 Or lp_lngLibraryID = 284349 Or lp_lngLibraryID = 441212 Or lp_lngLibraryID = 623206 Or lp_lngLibraryID = 650950 Then
    l_rstNewEvent("hidefromweb") = 0
End If
    
'blank out fields which are specific to the ORIGINAL event only
l_rstNewEvent("cdate") = Now
l_rstNewEvent("cuser") = g_strUserInitials
If lp_lngBlankMasterfile = 0 Then
    If InStr(l_rstNewEvent("fileversion"), "Masterfile") > 0 Then
        l_rstNewEvent("fileversion") = Null
    End If
End If

l_rstNewEvent("mdate") = Null
l_rstNewEvent("muser") = Null

l_rstNewEvent.Update

'MsgBox g_lngLastID, vbExclamation

'close the original record
l_rstOriginalEvent.Close

'close the new record
l_rstNewEvent.Close

'Copy the Segment Information re-using the l_rstOriginalEvent to read the records
l_rstOriginalEvent.Open "SELECT * FROM eventsegment WHERE eventID = " & lp_lngEventID, c, 3, 3
If l_rstOriginalEvent.RecordCount > 0 Then
    l_rstOriginalEvent.MoveFirst
    Do While Not l_rstOriginalEvent.EOF
        l_strSQL = "INSERT INTO eventsegment (eventID, segmentreference, timecodestart, timecodestop, customfield1) VALUES ("
        l_strSQL = l_strSQL & l_lngNewEventID & ", "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstOriginalEvent("segmentreference")) & "', "
        l_strSQL = l_strSQL & "'" & l_rstOriginalEvent("timecodestart") & "', "
        l_strSQL = l_strSQL & "'" & l_rstOriginalEvent("timecodestop") & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstOriginalEvent("customfield1")) & "');"
        c.Execute l_strSQL
        l_rstOriginalEvent.MoveNext
    Loop
End If
l_rstOriginalEvent.Close

'Copy the Logging Information re-using the l_rstOriginalEvent to read the records
l_rstOriginalEvent.Open "SELECT * FROM eventlogging WHERE eventID = " & lp_lngEventID, c, 3, 3
If l_rstOriginalEvent.RecordCount > 0 Then
    l_rstOriginalEvent.MoveFirst
    Do While Not l_rstOriginalEvent.EOF
        l_strSQL = "INSERT INTO eventlogging (eventID, segmentreference, timecodestart, timecodestop, note) VALUES ("
        l_strSQL = l_strSQL & l_lngNewEventID & ", "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstOriginalEvent("segmentreference")) & "', "
        l_strSQL = l_strSQL & "'" & l_rstOriginalEvent("timecodestart") & "', "
        l_strSQL = l_strSQL & "'" & l_rstOriginalEvent("timecodestop") & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstOriginalEvent("note")) & "');"
        c.Execute l_strSQL
        l_rstOriginalEvent.MoveNext
    Loop
End If
l_rstOriginalEvent.Close

'Copy the iTunes Advisory Information
l_strSQL = "SELECT * FROM iTunes_Clip_Advisory WHERE eventID = " & lp_lngEventID & ";"
l_rstOriginalEvent.Open l_strSQL, c, 3, 3
If l_rstOriginalEvent.RecordCount > 0 Then
    l_rstOriginalEvent.MoveFirst
    Do While Not l_rstOriginalEvent.EOF
        l_strSQL = "INSERT INTO iTunes_Clip_Advisory (eventID, advisorysystem, advisory) VALUES ("
        l_strSQL = l_strSQL & l_lngNewEventID & ", "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstOriginalEvent("advisorysystem")) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstOriginalEvent("advisory")) & "');"
        c.Execute l_strSQL
        l_rstOriginalEvent.MoveNext
    Loop
End If
l_rstOriginalEvent.Close

'Copy the iTunes Product Details
l_strSQL = "SELECT * FROM iTunes_product WHERE eventID = " & lp_lngEventID & ";"
l_rstOriginalEvent.Open l_strSQL, c, 3, 3
If l_rstOriginalEvent.RecordCount > 0 Then
    l_rstOriginalEvent.MoveFirst
    Do While Not l_rstOriginalEvent.EOF
        l_strSQL = "INSERT INTO iTunes_product (eventID, territory, salesstartdate, clearedforsale) VALUES ("
        l_strSQL = l_strSQL & l_lngNewEventID & ", "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstOriginalEvent("territory")) & "', "
        l_strSQL = l_strSQL & "'" & FormatSQLDate(l_rstOriginalEvent("salesstartdate")) & "', "
        l_strSQL = l_strSQL & "'" & QuoteSanitise(l_rstOriginalEvent("clearedforsale")) & "');"
        Debug.Print l_strSQL
        c.Execute l_strSQL
        l_rstOriginalEvent.MoveNext
    Loop
End If
l_rstOriginalEvent.Close

Set l_rstOriginalEvent = Nothing
Set l_rstNewEvent = Nothing

CopyEventToLibraryID = l_lngNewEventID

End Function

Public Function FormatSQLDate(lDate As Variant)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If IsNull(lDate) Then
        FormatSQLDate = Null
        Exit Function
    End If
    If lDate = "" Then
        FormatSQLDate = Null
        Exit Function
    End If
    If Not IsDate(lDate) Then
        FormatSQLDate = Null
        Exit Function
    
    End If
    
    Select Case g_dbtype
        Case "mysql"
            FormatSQLDate = Year(lDate) & "/" & Month(lDate) & "/" & Day(lDate) & " " & Hour(lDate) & ":" & Minute(lDate) & ":" & Second(lDate)
        Case "mssql"
            FormatSQLDate = Month(lDate) & "/" & Day(lDate) & "/" & Year(lDate) & " " & Hour(lDate) & ":" & Minute(lDate) & ":" & Second(lDate)
        Case "access"
            FormatSQLDate = lDate
    End Select
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Public Function IsCETALocked() As Boolean

Dim optLockSystem As Currency

optLockSystem = Val(Trim(" " & GetData("setting", "value", "name", "LockSystem")))

If (optLockSystem And 2) = 2 Then
    IsCETALocked = True
Else
    IsCETALocked = False
End If

End Function

Public Function GetNextSequence(lp_strSequenceName As String) As Long
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Dim c As ADODB.Connection
    Dim l_strSQL As String
    Dim l_lngGetNextSequence  As Long
    Dim l_rsSequence As ADODB.Recordset
    
    Set c = New ADODB.Connection
    c.Open g_strConnection
    
    l_strSQL = "SELECT * FROM sequence WHERE sequencename = '" & lp_strSequenceName & "';"
    
    Set l_rsSequence = New ADODB.Recordset
    l_rsSequence.Open l_strSQL, c, adOpenDynamic, adLockBatchOptimistic
    
    If Not l_rsSequence.EOF Then
        l_rsSequence.MoveFirst
        l_lngGetNextSequence = l_rsSequence("sequencevalue")
        
        l_strSQL = "UPDATE sequence SET sequencevalue = '" & l_lngGetNextSequence + 1 & "', muser = '" & g_strUserInitials & "', mdate = '" & FormatSQLDate(Now()) & "' WHERE sequencename = '" & lp_strSequenceName & "';"
        
        c.Execute l_strSQL
    
    End If
    
    l_rsSequence.Close
    Set l_rsSequence = Nothing
    c.Close
    Set c = Nothing
    
    GetNextSequence = l_lngGetNextSequence
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function SetData(lp_strTableName As String, lp_strFieldToEdit As String, lp_strFieldToSearch As String, lp_varCriterea As Variant, lp_varValue As Variant) As Variant

Dim l_strSQL As String, l_UpdateValue As String, c As ADODB.Connection

If UCase(lp_varValue) = "NULL" Or lp_varValue = "" Then
    l_UpdateValue = "NULL"
Else
    l_UpdateValue = "'" & QuoteSanitise(lp_varValue) & "'"
End If

l_strSQL = "UPDATE " & lp_strTableName & " SET " & lp_strFieldToEdit & " = " & l_UpdateValue & " WHERE " & lp_strFieldToSearch & " = '" & QuoteSanitise(lp_varCriterea) & "'"

Set c = New ADODB.Connection
c.Open g_strConnection

c.Execute l_strSQL
c.Close
Set c = Nothing

End Function

Public Function GetEventIDForFile(ByVal lp_strUNCDrive As String, ByVal lp_strPath As String, ByVal lp_strFileName As String) As Long
    '<EhHeader>
    On Error GoTo GetEventIDForFile_Err
    '</EhHeader>


    Dim s As String
    Dim e As Long
    Dim c As New ADODB.Connection
    Dim r As New ADODB.Recordset

    'build the SQL statement
    s = "SELECT eventID FROM library INNER JOIN events ON library.libraryID = events.libraryID WHERE (events.system_deleted = 0)"
    s = s & " AND (library.subtitle = '" & lp_strUNCDrive & "') "
    s = s & " AND (events.altlocation = '" & QuoteSanitise(lp_strPath) & "') "
    s = s & " AND (events.clipfilename = '" & QuoteSanitise(lp_strFileName) & "');"

    'open the database and recordset
    c.Open g_strConnection
    r.Open s, c, adOpenStatic, adLockReadOnly

    'was there a result?
    If Not r.EOF Then
        e = r(0)
    End If

    'close the recordset
    r.Close
    Set r = Nothing

    'close the connection
    c.Close
    Set c = Nothing

    'return the event ID to the function
    GetEventIDForFile = e

    '<EhFooter>
    Exit Function

GetEventIDForFile_Err:
        MsgBox Err.Description & vbCrLf & _
               "in prjJCAFileManager.Module1.GetEventIDForFile " & _
               "at line " & Erl, _
               vbExclamation + vbOKOnly, "Application Error"
        Resume Next
        '</EhFooter>
End Function


Public Function GetEventIDForFileUsingLibraryID(ByVal lp_lngLibraryID As Long, ByVal lp_strPath As String, ByVal lp_strFileName As String) As Long
        '<EhHeader>
        On Error GoTo GetEventIDForFileUsingLibraryID_Err
        '</EhHeader>


    Dim s As String
    Dim e As Long
    Dim c As New ADODB.Connection
    Dim r As New ADODB.Recordset

    'build the SQL statement
    s = "SELECT eventID FROM events "
    s = s & " WHERE (libraryID = '" & lp_lngLibraryID & "') AND (events.system_deleted = 0) "
    s = s & " AND (altlocation = '" & QuoteSanitise(lp_strPath) & "') "
    s = s & " AND (clipfilename = '" & QuoteSanitise(lp_strFileName) & "');"

    'open the database and recordset
    c.Open g_strConnection
    r.Open s, c, adOpenStatic, adLockReadOnly

    'was there a result?
    If Not r.EOF Then
        e = r(0)
    End If

    'close the recordset
    r.Close
    Set r = Nothing

    'close the connection
    c.Close
    Set c = Nothing

    'return the event ID to the function
    GetEventIDForFileUsingLibraryID = e

        '<EhFooter>
        Exit Function

GetEventIDForFileUsingLibraryID_Err:
        MsgBox Err.Description & vbCrLf & _
               "in prjJCAFileManager.Module1.GetEventIDForFileUsingLibraryID " & _
               "at line " & Erl, _
               vbExclamation + vbOKOnly, "Application Error"
        Resume Next
        '</EhFooter>
End Function

Public Function LetterToUNC(DriveLetter As String) As String
        '<EhHeader>
        On Error GoTo LetterToUNC_Err
        '</EhHeader>

        Dim hEnum         As Long

        Dim NetInfo(1023) As NETRESOURCE

        Dim entries       As Long

        Dim nStatus       As Long

        Dim LocalName     As String

        Dim UNCName       As String

        Dim i             As Long

        Dim r             As Long

        ' Begin the enumeration
        nStatus = WNetOpenEnum(RESOURCE_CONNECTED, RESOURCETYPE_ANY, 0&, ByVal 0&, hEnum)

        LetterToUNC = "Drive Letter Not Found"

        'Check for success from open enum
        If ((nStatus = 0) And (hEnum <> 0)) Then
            ' Set number of entries
            entries = 1024

            ' Enumerate the resource
            nStatus = WNetEnumResource(hEnum, entries, NetInfo(0), CLng(Len(NetInfo(0))) * 1024)

            ' Check for success
            If nStatus = 0 Then

                For i = 0 To entries - 1
                    ' Get the local name
                    LocalName = ""

                    If NetInfo(i).lpLocalName <> 0 Then
                        LocalName = Space(lstrlen(NetInfo(i).lpLocalName) + 1)
                        r = lstrcpy(LocalName, NetInfo(i).lpLocalName)
                    End If

                    ' Strip null character from end
                    If Len(LocalName) <> 0 Then
                        LocalName = Left(LocalName, (Len(LocalName) - 1))
                    End If

                    If UCase$(LocalName) = UCase$(DriveLetter) Then
                        ' Get the remote name
                        UNCName = ""

                        If NetInfo(i).lpRemoteName <> 0 Then
                            UNCName = Space(lstrlen(NetInfo(i).lpRemoteName) + 1)
                            r = lstrcpy(UNCName, NetInfo(i).lpRemoteName)
                        End If

                        ' Strip null character from end
                        If Len(UNCName) <> 0 Then
                            UNCName = Left(UNCName, (Len(UNCName) - 1))
                        End If

                        ' Return the UNC path to drive
                        LetterToUNC = UNCName

                        ' Exit the loop
                        Exit For

                    End If

                Next i

            End If
        End If

        ' End enumeration
        nStatus = WNetCloseEnum(hEnum)
        '<EhFooter>
        Exit Function

LetterToUNC_Err:
        MsgBox Err.Description & vbCrLf & _
               "in prjJCAFileManager.Module1.LetterToUNC " & _
               "at line " & Erl, _
               vbExclamation + vbOKOnly, "Application Error"
        Resume Next
        '</EhFooter>
End Function

Function LookupEventID(ByVal Index As Long, ByVal lp_lstItem As ListItem, Optional lp_lngLibraryID As Long) As Long

    Dim l_strFileName As String, l_strPath As String, l_strDrivePath As String, l_lngEventID As Long, temp As String, i As Long, FSO As Scripting.FileSystemObject
    
    Set FSO = New Scripting.FileSystemObject
    
'    If Not lp_lstItem.IsFolder Then
   
        'pick up the actual file name
        l_strFileName = lp_lstItem.DisplayName
    
        'get the full file name and path
        l_strPath = frmCetaFileManager.shFiles(Index).CurrentFolder
    
        'get the UNC drive path from the drive letter or the full path
        If Left(l_strPath, 2) <> "\\" Then
            l_strDrivePath = LetterToUNC(Left(l_strPath, 2))
        Else
            temp = Right(l_strPath, Len(l_strPath) - 2)
            l_strDrivePath = "\\"
            i = InStr(temp, "\")
            If i <> 0 Then l_strDrivePath = l_strDrivePath & Left(temp, i)
            temp = Mid(temp, i + 1)
            i = InStr(temp, "\")
            If i <> 0 Then
                l_strDrivePath = l_strDrivePath & Left(temp, i - 1)
            Else
                l_strDrivePath = l_strDrivePath & temp
            End If
        End If
        
        'chop off the drive letter and the first slash
        If Left(l_strPath, 2) <> "\\" Then
            l_strPath = Right$(l_strPath, Len(l_strPath) - 3)
        Else
            If Len(l_strPath) > Len(l_strDrivePath) + 1 Then
                l_strPath = Mid(l_strPath, Len(l_strDrivePath) + 2)
            Else
                l_strPath = ""
            End If
        End If

'        If Left(l_strDrivePath, 1) <> "\" Then
        
            'check for the libraryID.sys file on the root of this drive
            
            On Error GoTo PROC_NoRootFile
            
            Dim sID As String
            Dim l_strFileToOpen As String
            
            If Left(l_strDrivePath, 1) <> "\" Then
                l_strFileToOpen = Left$(frmCetaFileManager.shFiles(Index).CurrentFolder, 2)
            Else
                l_strFileToOpen = frmCetaFileManager.shFiles(Index).CurrentFolder
            End If
                      
            frmCetaFileManager.shFiles(2).CurrentFolder = l_strFileToOpen
            
            Do While Not FSO.FileExists(l_strFileToOpen & "\libraryID.sys") And Not FSO.FileExists(l_strFileToOpen & "\.libraryID.sys")
                l_strFileToOpen = Left(frmCetaFileManager.shFiles(2).CurrentFolder, Len(frmCetaFileManager.shFiles(2).CurrentFolder) - Len(frmCetaFileManager.shFiles(2).CurrentFolderDisplayName) - 1)
                frmCetaFileManager.shFiles(2).CurrentFolder = l_strFileToOpen
                l_strPath = Mid(frmCetaFileManager.shFiles(Index).CurrentFolder, Len(l_strFileToOpen) + 2)
            Loop
            
            If FSO.FileExists(l_strFileToOpen & "\libraryID.sys") Then
                Open l_strFileToOpen & "\libraryID.sys" For Input As 1
                Line Input #1, sID
                Close 1
            ElseIf FSO.FileExists(l_strFileToOpen & "\.libraryID.sys") Then
                Open l_strFileToOpen & "\.libraryID.sys" For Input As 1
                Line Input #1, sID
                Close 1
            End If
            
PROC_NoRootFile:
            'clear the error if there was one
            Err.Clear

            'check to see if that file has a logged event ID
            If sID <> "" Then
                l_lngEventID = GetEventIDForFileUsingLibraryID(Val(sID), l_strPath, l_strFileName)
            Else
                l_lngEventID = GetEventIDForFileUsingLibraryID(lp_lngLibraryID, l_strPath, l_strFileName)
            End If
        
'        Else
        
            'check to see if that file has a logged event ID
'            l_lngEventID = GetEventIDForFile(l_strDrivePath, l_strPath, l_strFileName)
        
'        End If
        
        If l_lngEventID > 0 Then
            LookupEventID = l_lngEventID
        Else
            LookupEventID = 0
        End If
           
'    End If

End Function

Function GetFlag(lp_intFlagValue As Variant)
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    Select Case lp_intFlagValue
        Case 1, -1, True, "TRUE", "True", "true", "YES", "Yes", "yes"
            GetFlag = 1
        Case 0, "FALSE", "False", "false", "no", "NO", "No"
            GetFlag = 0
        Case Else
            GetFlag = 0
    End Select
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Public Function GetAlias(lp_strText As String) As String

Dim l_con As New ADODB.Connection

l_con.Open g_strConnection

'make sure we don't get balnks
If lp_strText = "" Then

    'return a blank
    GetAlias = ""
    
    'and exit
    Exit Function
    
End If

Dim i&
For i& = 1 To Len(lp_strText)
    If Mid$(lp_strText, i&, 1) = Chr$(34) Then
        Mid$(lp_strText, i&, 1) = Chr$(39)
    End If
Next

'get the alias of some text from the system, i.e. if there is some text
'in the spreadsheet, match it to an item in the alias table
Dim l_dynAlias As ADODB.Recordset
Dim l_sql$

l_sql$ = "SELECT * FROM bbcalias WHERE typedstring = '" & QuoteSanitise(lp_strText) & "'"
'create a recordset to get the value out
Set l_dynAlias = New ADODB.Recordset
l_dynAlias.Open l_sql$, l_con, 3, 3

'check if there is a match
If l_dynAlias.RecordCount > 0 Then

    'there is a match, so use the alias instead of the spreadsheet text
    GetAlias = l_dynAlias("bbcAlias")
    
End If

'close the recordset
l_dynAlias.Close: Set l_dynAlias = Nothing
l_con.Close: Set l_con = Nothing

End Function

Public Sub PopulateCombo(lp_strCategory As String, lp_conControl As Control, Optional lp_strCriterea As String, Optional lp_blnClearFirst As Boolean)

Dim Count As Long

On Error GoTo PROC_CheckListError
    
    If lp_conControl.ListCount > 0 Then
        If lp_blnClearFirst = False Then
            Exit Sub
        Else
            For Count = 0 To lp_conControl.ListCount - 1
                lp_conControl.RemoveItem (0)
            Next
        End If
    End If
    GoTo PROC_Continue
    
PROC_TryRowCount:
    If lp_conControl.Rows > 0 Then
        If lp_blnClearFirst = False Then
            Exit Sub
        Else
            For Count = 0 To lp_conControl.Rows - 1
                lp_conControl.RemoveItem (0)
            Next
        End If
    End If
    GoTo PROC_Continue

PROC_CheckListError:
    If Err.Number = 438 Then
        GoTo PROC_TryRowCount
    End If
    
PROC_Continue:
    
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    
    Dim lp_strCategory1 As String
    Dim l_rstXRef As ADODB.Recordset, cnn As ADODB.Connection
           
    LockControl lp_conControl, True
    
    Screen.MousePointer = vbHourglass
    
    Dim l_intLoop As Integer
    Dim l_strSQL As String
    Dim l_rstResource As New ADODB.Recordset
            
    l_strSQL = "SELECT DISTINCT description, forder FROM xref WHERE category = '" & QuoteSanitise(lp_strCategory) & "' " & lp_strCriterea & " ORDER BY forder, description"
    
    Set l_rstXRef = New ADODB.Recordset
    Set cnn = New ADODB.Connection
    cnn.Open g_strConnection
    l_rstXRef.Open l_strSQL, cnn, 3, 3
    
    If Not l_rstXRef.EOF Then
        l_rstXRef.MoveFirst
    End If
    
    Do While Not l_rstXRef.EOF
        lp_conControl.AddItem Trim(" " & l_rstXRef("description"))
        l_rstXRef.MoveNext
    Loop
    
    l_rstXRef.Close
    Set l_rstXRef = Nothing
    cnn.Close
    Set cnn = Nothing
    
    LockControl lp_conControl, False
    DoEvents
    
    'MDIForm1.Caption = l_strTempCaption
    Screen.MousePointer = vbDefault
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Sub HighLite(lp_conControl As Control)
    'selects the text in a text box
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    lp_conControl.SelStart = 0
    lp_conControl.SelLength = Len(lp_conControl.Text)
    
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Sub
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Sub

Public Function LockControl(objX As Object, cLock As Boolean)
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If cLock Then
        LockWindowUpdate objX.hWnd
    Else
        LockWindowUpdate 0
        objX.Refresh
    End If
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    MsgBox Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Public Function ValidateFilename(sFilename) As Boolean
    Dim myExpression As RegExp
    
    Set myExpression = New RegExp
    myExpression.Pattern = "^[a-zA-Z0-9\.\_][a-zA-Z0-9\_\-\.]*[a-zA-Z0-9]\.[a-zA-Z0-9]{0,16}$"

    If myExpression.Test(sFilename) = True Then
        ValidateFilename = True
    Else
        ValidateFilename = False
    End If
End Function

Public Function ValidateFoldername(sFilename) As Boolean
    Dim myExpression As RegExp
    
    Set myExpression = New RegExp
    myExpression.Pattern = "^[a-zA-Z0-9\.\_][a-zA-Z0-9\_\-\.\\ ]*[a-zA-Z0-9]$"

    If myExpression.Test(sFilename) = True Then
        ValidateFoldername = True
    Else
        ValidateFoldername = False
    End If
End Function

Public Function CheckLocal10Gig() As Boolean

Dim IPConfig As Variant
Dim IPConfigSet As Object

Set IPConfigSet = GetObject("winmgmts:{impersonationLevel=impersonate}").ExecQuery("SELECT IPAddress FROM Win32_NetworkAdapterConfiguration WHERE IPEnabled = TRUE")

CheckLocal10Gig = False

For Each IPConfig In IPConfigSet
    If Not IsNull(IPConfig.ipaddress) Then
        If InStr(Trim(" " & IPConfig.ipaddress(0)), "10.9.7.") > 0 Or InStr(Trim(" " & IPConfig.ipaddress(0)), "10.9.77.") > 0 Then
            CheckLocal10Gig = True
        End If
    End If
Next IPConfig

End Function

Public Function GetCETASetting(lp_strSettingName As String) As Variant

GetCETASetting = GetData("setting", "value", "name", lp_strSettingName)

End Function

